<?php

namespace App\Http\Controllers;

use App\Base;
use App\BoncPhoto;
use App\BoncSticker;
use App\BoncText;
use App\BoncTextDutch;
use App\ConceptImage;
use App\PostImage;
use App\PostMaster;
use App\Posts;
use App\UserImages;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ScheduleController extends Controller
{
    protected $base;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Base $base)
    {
         $this->middleware('auth');
        $this->base = $base;
//        $ip = file_get_contents("http://ipecho.net/plain");
            $ip = $_SERVER['REMOTE_ADDR'];
            $url = 'http://ip-api.com/json/'.$ip;
            $tz = file_get_contents($url);
            $data=\GuzzleHttp\json_decode($tz);
            if($data->status == 'fail'){
             $timezone='Europe/Amsterdam';   
            }else{
             $timezone = json_decode($tz,true)['timezone'];
            }
           
            date_default_timezone_set($timezone);
            $value = config(['app.timezone'=>$timezone]);


    }


    public function index(Request $request)
    {
        $u_uuid = Auth::user()->org_uuid;

        $start = \Carbon\Carbon::now()->startOfMonth();

        $posts_ = Posts::select( 'id','post_id','channel', 'local_user_id', 'text','image', 'media', 'date', 'time')
            ->where('u_uuid', $u_uuid)
            ->whereDate('date', '>=', Carbon::today())
            ->get();


        $posts = [];
        $postsUn = [];
        $ocDate = [];
        if (count($posts_) >= 1){
            $posts_ = $posts_->toArray();

            foreach ($posts_ as $value){

                $postImage = PostImage::where('post_id',$value['id'])->pluck('image');
                if ($postImage != null && count($postImage) > 0){
                    $value['image'] = asset('storage/images/post/'.$postImage[0]);
                }


                if (isset($posts[$value['date']])){
                    $value['channel'] = explode(',',$value['channel']);
                    array_push($postsUn[$value['date']][] , $value);
                }else{
                    $value['channel'] = explode(',',$value['channel']);
                    $postsUn[$value['date']][] = [$value];
                }
                if (!in_array($value['date'], $ocDate))
                    $ocDate[]=$value['date'];

            }
        }else{
            $ocDate[]=Carbon::today()->toDateString();
        }

        usort($ocDate, array($this, "compareByTimeStamp"));

        foreach ($ocDate as $k => $d) {
            if (isset($postsUn[$d])) {
                $posts[$d] = $postsUn[$d];
            }
        }

        $finalDates=$posts;

        $count = count($posts_);


        return view('schedule', compact('finalDates','posts', 'ocDate', 'count'));
    }

    public function previousPost(Request $request)
    {
        $u_uuid = Auth::user()->org_uuid;

        $posts_ = Posts::select( 'channel', 'local_user_id', 'text','image', 'media', 'date', 'time')
            ->where('u_uuid', $u_uuid)
            ->whereDate('date', '<', Carbon::today())
//            ->groupBy(['date','time','channel', 'local_user_id', 'text','image', 'media', ])
            ->get();

        $posts = [];
        $postsUn = [];
        $ocDate = [];
        if (count($posts_) >= 1){
            $posts_ = $posts_->toArray();

            foreach ($posts_ as $value){

                if (isset($posts[$value['date']])){
                    $value['channel'] = explode(',',$value['channel']);
                    array_push($postsUn[$value['date']][] , $value);
                }else{
                    $value['channel'] = explode(',',$value['channel']);
                    $postsUn[$value['date']][] = [$value];
                }
                if (!in_array($value['date'], $ocDate))
                    $ocDate[]=$value['date'];

            }
        }

        usort($ocDate, array($this, "compareByTimeStamp"));
        $ocDate = array_reverse($ocDate);
        foreach ($ocDate as $k => $d) {
            if (isset($postsUn[$d])) {
                $posts[$d] = $postsUn[$d];
            }
        }

        $finalDates=$posts;
        $count = count($posts_);
        return view('previousPost', compact('finalDates','posts', 'ocDate', 'count'));
    }

    public function compareByTimeStamp($time1, $time2)
    {
        if (strtotime($time1) > strtotime($time2))
            return 1;
        else if (strtotime($time1) < strtotime($time2))
            return -1;
        else
            return 0;
    }

    public function postData( Request $request)
    {


        $data = $request->all();
//dd($data);
        try {

            $postImages = [];
            $postImages['imagename'] = [];
            $mediaUuid=[];
            if ($request->has('your_photo')) {
                $postImages = $this->thumbnailImage($request, 'your_photo');


                $images = $request->file('your_photo');

                $this->client = new Client();
                $uri = env('API_URL') . '/media';
               
                foreach ($images as $k => $image) {
                    $multipart[0]=[];
                    $image_path = $image->getPathname();
                    $image_mime = $image->getmimeType();
                    $image_org = $image->getClientOriginalName();

                    $multipart[0] = [
                        'name' => 'data',
                        'contents' => fopen($image_path, 'r'),
                    ];
                    $media = $this->base->postRequest('media', $multipart);
                    $mediaUuid[] =$media->uuid;
                 
                }
               
//                $media = $this->base->postRequest('media', $multipart);
            }

            if (isset($data['channel'])) {
                foreach ($data['channel'] as $key => $value) {
                    $channel[] = $key;
                }
            }else{
//                Session::flash('error', 'Select Channel');
                return response()->json(['status' => 500, 'data' => []]);

            }

            $postMaster = new PostMaster;
            $postMaster->u_uuid = Auth::user()->org_uuid;
            $postMaster->image = !empty($postImages['imagename']) ? $postImages['imagename'][0] : '';
            $postMaster->content = $data['text'];
            $postMaster->channels = implode(',', $channel);
            $postMaster->is_scheduled = '1';
            $postMaster->is_posted = '0';
            $postMaster->save();

            $body = ['text' => $data['text'], 'media_uuid' => $mediaUuid];
            $publication = [];
            $i = 0;

            foreach ($data['channel'] as $key => $value) {
                $datePost = Carbon::createFromFormat('d/m/Y', $data['date'][$key])->toDateString();
                $timePost =Carbon::createFromFormat('H:i', $data['time'][$key])->toTimeString();


                $datetime = Carbon::createFromFormat('d/m/Y H:i', $data['date'][$key].' '.$data['time'][$key], 'Asia/Kolkata')->setTimezone('UTC')->toDateTimeString();
                if ($key == 'instagram') {
                    $notification = new \App\Notification();
                    $notification->content = $data['text'];
                    $notification->image = !empty($postImages['imagename']) ? $postImages['imagename'][0] : '';
                    $notification->status = 0;
                    $notification->date = $datePost;
                    $notification->time = $timePost;
//                    $notification->save();
                    $publications_id = time();
                } else{
                    $publication[$i] = (object)[
                        'channel_uuid' => $data['uuid'][$key],
                        'publishTime' => $datetime,
                        'body' => (object)$body
                    ];
                    $publications = $this->base->postRequest('publications', json_encode(['publications' => $publication]));
                    $publications_id = $publications['data'][0]['uuid'];

                }
                $post = new Posts;
                $post->master_id = $postMaster->id;
                $post->text = $data['text'];
                $post->u_uuid = Auth::user()->org_uuid;
                $post->c_uuid = $data['uuid'][$key];
                $post->channel = $key;
                $post->image = !empty($postImages['imagename']) ? $postImages['imagename'][0] : '';
                $post->media = isset($media->uuid)?$media->uuid:'';
                $post->date = $datePost;
                $post->time = $timePost;
                $post->post_id = $publications_id;
                $post->save();

                if ($key == 'instagram') {
                    $notification->post_id = $post->id;
                    $notification->save();
                }

                foreach ($postImages['imagename'] as $k => $image) {
                    $postImage = new PostImage();
                    $postImage->post_id = $post->id;
                    $postImage->image = $image;
                    $postImage->save();
                }


            }


            if ($request->from == 'submit')
                Session::flash('success', 'Post Scheduled');
            else
                Session::flash('success', 'Posted Successfully');
            return response()->json(['status' => 200, 'data' => ['redirect_url' => url('schedule')]]);
//            return redirect('schedule');
//            return redirect()->back();
        }
        catch (\Exception $e){
            dd('here', $e);
            Session::flash('erro', 'Something went wrong');
            return redirect()->back();

        }catch (GuzzleException $ge){
            dd('herea');
            Session::flash('erro', 'Something went wrong');
            return redirect()->back();

        }

    }
    public function shareNow($id,Request $request){
       
        $post = Posts::where('post_id',$id)->first();
        $postImage =PostImage::where('post_id',$post->id)->get();
        $mediaUuid=[];
         foreach ($postImage as $k => $image) {
                    $multipart[0]=[];
                    
                    $multipart[0] = [
                        'name' => 'data',
                        'contents' => fopen('storage/images/post/'.$image->image,'r'),  ];
                    
                    $media = $this->base->postRequest('media', $multipart);
                    $mediaUuid[] =$media->uuid;
                 
                }
               
        $body = ['text' => $post->text, 'media_uuid' => $mediaUuid];
            $publication = (object)[
                'channel_uuid' => $post->c_uuid,
                'publishTime' => \Carbon\Carbon::now(),
                'body' => (object)$body
            ];
             $publications = $this->base->putRequest('publications/'.$post->post_id, json_encode(['publications' => $publication]));
        
        return response()->json(['status'=>200]);
    }

    public function savePostDesign(Request $request)
    {
        $image = $this->makeDP($request->file('image')->path(),1);
        $loc = public_path('images/design/'). str_random(10) . ".png" ;

        file_put_contents($loc, $image);
//        $postImage = $this->base64Image($request, 'image','design');
        dd( 'done');
    }
    public function filterPostDesign(Request $request)
    {
        $uuid = (Auth::user() != null ) ? Auth::user()->org_uuid : time();
        $act = $request->act;
        $image = $request->img;
        $picture = $image;
        $dataPic = base64_decode($picture);
        $path = public_path('').'/tmp/'.$uuid.'/';
        if(substr($image, 0,10) == 'data:image'){
            $img_type = exif_imagetype($picture);
            if($img_type == "2"){
                $imgName = time().'.jpg';
                $image = str_replace('data:image/jpeg;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                \File::put($path.$imgName, base64_decode($image));

                $c_image = imagecreatefromjpeg($path.$imgName);
            }
            elseif($img_type == "1") {
                $imgName = time() . '.gif';
                $image = str_replace('data:image/gif;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                \File::put($path.$imgName, base64_decode($path.$image));

                $c_image = imagecreatefromgif($imgName);
            }
            elseif($img_type == "3") {
                $imgName = time() . '.png';
                $image = str_replace('data:image/png;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                \File::put($path.$imgName, base64_decode($image));

                $c_image = imagecreatefrompng($path.$imgName);
            }
            $lastPath = $path.$imgName;

            if(!\File::isDirectory($path)){
                \File::makeDirectory($path, 0777, true, true);
            }

            if($act != "undo" && $act != "redo" && $act != "rot" && $act != "rot")
            {
                $effects = array(
                    "neg" => IMG_FILTER_NEGATE,
                    "blr" => IMG_FILTER_GAUSSIAN_BLUR,
                    "brg" => IMG_FILTER_BRIGHTNESS,
                    "clr" => IMG_FILTER_COLORIZE,
                    "cntr" => IMG_FILTER_CONTRAST,
                    "edgd" => IMG_FILTER_EDGEDETECT,
                    "gray" => IMG_FILTER_GRAYSCALE,
                    "mean" => IMG_FILTER_MEAN_REMOVAL,
                    "seleb" => IMG_FILTER_SELECTIVE_BLUR,
                    "smth" => IMG_FILTER_SMOOTH,
                );

                if(file_exists($lastPath))
                {

                    if($act == "brg")
                        imagefilter($c_image, $effects[$act],-50);
                    elseif($act == "clr")
                        imagefilter($c_image, $effects[$act],0, -155, -255);
                    elseif($act == "cntr")
                        imagefilter($c_image, $effects[$act],60);
                    elseif($act == "smth")
                        imagefilter($c_image, $effects[$act],20);
                    else
                        imagefilter($c_image, $effects[$act]);

                    $filteredImg = $this->savePicture($c_image,$lastPath,$img_type);
                    //unlink("../active/".$picture);
                }
                else
                {
                    echo "Image not found...!";
                }
            }
        }
        else{
//            dd($path.$picture);
            $img_type = exif_imagetype($path.$picture);
//            $imgName =$picture;
            if($img_type == "2"){
                $imgName = time().'.jpg';
                $c_image = imagecreatefromjpeg($path.$picture);
            }
            elseif($img_type == "1") {
                $imgName = time() . '.gif';
                $c_image = imagecreatefromgif($path.$picture);
            }
            elseif($img_type == "3") {
                $imgName = time() . '.png';
                $c_image = imagecreatefrompng($path.$picture);
            }

            $lastPath = $path.$picture;

            if($act != "undo" && $act != "redo" && $act != "rot" && $act != "rot")
            {
                $effects = array(
                    "neg" => IMG_FILTER_NEGATE,
                    "blr" => IMG_FILTER_GAUSSIAN_BLUR,
                    "brg" => IMG_FILTER_BRIGHTNESS,
                    "clr" => IMG_FILTER_COLORIZE,
                    "cntr" => IMG_FILTER_CONTRAST,
                    "edgd" => IMG_FILTER_EDGEDETECT,
                    "gray" => IMG_FILTER_GRAYSCALE,
                    "mean" => IMG_FILTER_MEAN_REMOVAL,
                    "seleb" => IMG_FILTER_SELECTIVE_BLUR,
                    "smth" => IMG_FILTER_SMOOTH,
                );

                if(file_exists($lastPath))
                {

                    if($act == "brg")
                        imagefilter($c_image, $effects[$act],-50);
                    elseif($act == "clr")
                        imagefilter($c_image, $effects[$act],0, -155, -255);
                    elseif($act == "cntr")
                        imagefilter($c_image, $effects[$act],60);
                    elseif($act == "smth")
                        imagefilter($c_image, $effects[$act],20);
                    else
                        imagefilter($c_image, $effects[$act]);

                    $filteredImg = $this->savePicture($c_image,$path.$imgName,$img_type);
                    //unlink("../active/".$picture);
                }
                else
                {
                    echo "Image not found...!";
                }
            }
        }

        return response()->json(['status' => '', 'image' => $imgName, 'uid'=> $uuid]);

    }

    public function savePicture(&$image, $file,$img_type)
    {
        //write image to a file
        if($img_type == "2")
            imagejpeg($image, $file);
        elseif($img_type == "1")
            imagegif($image, $file);
        elseif($img_type == "3")
            imagepng($image, $file);

        //clean up memory
        imagedestroy($image);
        //print image tag
        return $file;
    }

    public function addLogo(&$image, $file,$img_type)
    {
        $png = imagecreatefrompng('./mark.png');
        $jpeg = imagecreatefromjpeg('./image.jpg');

        list($width, $height) = getimagesize('./image.jpg');
        list($newwidth, $newheight) = getimagesize('./mark.png');
        $out = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($out, $jpeg, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
        imagecopyresampled($out, $png, 0, 0, 0, 0, $newwidth, $newheight, $newwidth, $newheight);
        imagejpeg($out, 'out.jpg', 100);
    }

    public function makeDP($sourcePath, $design = 0){

        if(in_array($design, array(0, 1, 2)))
            $design = "frame$design.png";
        else
            exit;


        $src = imagecreatefromstring(file_get_contents($sourcePath));
        $fg = imagecreatefrompng('images/frame/'.$design);

        list($width, $height) = getimagesize($sourcePath);

        $croppedFG = imagecreatetruecolor($width, $height);

        $background = imagecolorallocate($croppedFG, 0, 0, 0);
//        dd($background, $croppedFG);
        // removing the black from the placeholder
        imagecolortransparent($croppedFG, $background);

        imagealphablending($croppedFG, false);
        imagesavealpha($croppedFG, true);

        imagecopyresized($croppedFG, $fg, 0, 0, 0, 0, $width, $height, 400, 400);

        // Start merging
        $out = imagecreatetruecolor($width, $height);
        imagecopyresampled($out, $src, 0, 0, 0, 0, $width, $height, $width, $height);
        imagecopyresampled($out, $croppedFG, 0, 0, 0, 0, $width, $height, $width, $height);

        ob_start();
        imagepng($out);
        $image = ob_get_clean();
        return $image;
    }

    public function editPost(Request $request, $id, $p_id){

        $editPost_=Posts::select('id', 'post_id','channel','c_uuid','text','media','image','date','time')->with('postImage')
            ->where('id',$id)->where('post_id', $p_id)->first()->toArray();
//        dd($editPost_, \Illuminate\Support\Facades\Auth::user()->getAuthPassword());
        $channels = $this->base->request('get', 'channels');
        if(app()->getLocale() =='en')
            $textCategory=BoncText::select(DB::raw('distinct category'))->get()->toArray();
        else
            $textCategory=BoncTextDutch::select(DB::raw('distinct category'))->get()->toArray();

        $companyIcon = [];
        $companyIcon_ = UserImages::where('u_uuid', Auth::user()->org_uuid)->get();
        if($companyIcon_ != null){
            $companyIcon = $companyIcon_->toArray();
        }
        $boncPhoto=BoncPhoto::select(DB::raw('distinct category'))->get()->toArray();
        $boncQuote=BoncPhoto::select(DB::raw('distinct category'))->get()->toArray();
        $boncSticker=BoncSticker::select(DB::raw('distinct category'))->get()->toArray();
        return view('editPost', compact('channels', 'editPost_','textCategory','companyIcon', 'boncPhoto', 'boncQuote', 'boncSticker'));

    }
    public function updatePost(Request $request){


        $data = $request->all();
        try {
        $media ='';
        if (isset($data['delete_image_id']) || isset($data['your_photo'])){
            if (isset($data['delete_image_id']) && $data['delete_image_id'] != null) {
                $deleteId = explode(',', $data['delete_image_id']);
                PostImage::whereIn('id', $deleteId)->delete();
            }
            if (isset($data['your_photo'])) {
                $postImage = $this->thumbnailImage($request, 'your_photo');
                foreach ($postImage['imagename'] as $k => $image) {
                    $postImage = new PostImage();
                    $postImage->post_id = $data['post_id'];
                    $postImage->image = $image;
                    $postImage->save();
                }
            }
        }
        $datePost = Carbon::createFromFormat('d/m/Y', $data['date'])->toDateString();
        $timePost = Carbon::createFromFormat('H:i', $data['time'])->toTimeString();
        $datetime = Carbon::createFromFormat('d/m/Y H:i', $data['date'].' '.$data['time'], 'Asia/Kolkata')->setTimezone('UTC')->toDateTimeString();
        $body = ['text' => $data['text'], 'media_uuid' => isset($media->uuid) ? $media->uuid:''];
        $post = Posts::find($data['post_id']);
            $publication = (object)[
                'publishTime' => $datePost.' '.$timePost,
                'body' => (object)$body
            ];
//dd($publication, $datePost);
        if ($post->channel == 'instagram'){
            $notification = \App\Notification::where('post_id', $post->id)->update(['content'=>$data['text'], 'date'=>$datePost, 'time' => $timePost]);
        }else{
            $publications = $this->base->putRequest('publications/'.$post->post_id, json_encode(['publications' => $publication]));
        }



        $post->text = $data['text'];
        $post->date = $datePost;
        $post->time = $timePost;
        $post->save();

            Session::flash('success', 'Update Post Successfully ');
            return response()->json(['status' => 200, 'data' => ['redirect_url' => url('schedule')]]);
        } catch (\Exception $e) {
            dd('here', $e);
            Session::flash('erro', 'Something went wrong');
            return redirect()->back();

        } catch (GuzzleException $ge) {
            dd('herea');
            Session::flash('erro', 'Something went wrong');
            return redirect()->back();

        }
    }

    public function deletePost(Request $request, $id, $p_id=0){

        $post = Posts::find($id);

        if ($post->channel == 'instagram' )
            $notification = \App\Notification::where('post_id', $post->id)->delete();
        elseif ($p_id != 0 )
            $publications = $this->base->request('delete','publications/'.$p_id, []);

        $post=Posts::where('id',$id)->delete();

        Session::flash('success', 'Delete Post Successfully ');
        return redirect()->back();
    }
}
