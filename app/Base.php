<?php
/**
 * Created by PhpStorm.
 * User: info
 * Date: 04/04/17
 * Time: 9:41 AM
 */

namespace App;


use GuzzleHttp\Client;
use App\Helpers\Helper;
use Session;
use Auth;

class Base extends Helper
{

    protected $client;
    protected $baseUrl;

    const SUCCESS_STATUS = 200;
    const ERROR_STATUS = 500;

    public function __construct()
    {
        $this->baseUrl = env('API_URL');
    }

    public function request($method, $uri = '', array $options = [])
    {
        $uri = $this->urlCorrector($this->baseUrl .'/'. $uri);
//dd($method, $uri, $options, \Illuminate\Support\Facades\Auth::user()->getAuthPassword());
        if (\Illuminate\Support\Facades\Auth::user() == null)
            return redirect('login');
        $this->client = new Client([
            'headers' => [
                "Content-Type" => "application/json",
                "Authorization" => "Bearer ".\Illuminate\Support\Facades\Auth::user()->getAuthPassword()
            ]]);
    
    // dd(Auth()->User());
            // $this->client = new Client(['base_uri' => $uri]);
//             dd($this->client, $method, $uri, $options);
            $response = $this->client->request($method, $uri, $options);
            $res_ = $response;

            $resData = json_decode($response->getBody()->getContents(), true);

//            if($resData['status'] == '403'){
//                dd('403');
//                return redirect()->route('internal.server.error');
//            }
            return $resData;

    }
    public function postRequest($uri = '',$json = '')
    {
        $this->client = new Client();
        if ($uri == 'media'){
            $uri = $this->urlCorrector($this->baseUrl .'/'. $uri);
            $header = ['Accept'                => 'application/json',
                // 'Content-Type'          => 'multipart/form-data',
                'Authorization'         => 'Bearer '. \Illuminate\Support\Facades\Auth::user()->getAuthPassword(),
            ];

            $options = ['headers' => $header, "multipart"=> $json ];
            // dd($options);
            $response = $this->client->post($uri, $options);
            $responseData = json_decode($response->getBody()->getContents())    ;

            return $responseData;

        }
        $uri = $this->urlCorrector($this->baseUrl .'/'. $uri);

            $header =[
                    "Content-Type" => "application/json",
                    "Authorization" => "Bearer ".\Illuminate\Support\Facades\Auth::user()->getAuthPassword()
                ];
            $options = ['headers' => $header, "body"=> $json ];
//        dd($options, $uri);
            $response = $this->client->post($uri, $options);

            $resData = json_decode($response->getBody()->getContents(), true);
            if ($response->getStatusCode() == 201){
                return ['status' => 201, 'message' => 'Post Created Successfully', 'data'=>$resData];
            }
////            $response = $this->client->request($method, $uri, $options);
//            dd($resData, $response->getContents(), $response);
            return $resData;

    }
    public function putRequest($uri = '',$json = '')
    {
        $this->client = new Client();

        $uri = $this->urlCorrector($this->baseUrl .'/'. $uri);

        $header =[
                "Content-Type" => "application/json",
                "Authorization" => "Bearer ".\Illuminate\Support\Facades\Auth::user()->getAuthPassword()
            ];
        $options = ['headers' => $header, "body"=> $json ];

        $response = $this->client->put($uri, $options);

        $resData = json_decode($response->getBody()->getContents(), true);


        if ($response->getStatusCode() == 201){
            return ['status' => 201, 'message' => 'success', 'data'=>$resData];
        }

        return $resData;

    }

}
