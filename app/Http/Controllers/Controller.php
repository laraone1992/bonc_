<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function fileUpload(Request $request)
    {
        $uploadedFile = $request->file('file');
        $filename = time().$uploadedFile->getClientOriginalName();

        Storage::disk('local')->putFileAs(
            'files/'.$filename,
            $uploadedFile,
            $filename
        );

        $upload = new Upload;
        $upload->filename = $filename;

        $upload->user()->associate(auth()->user());

        $upload->save();

        return response()->json([
            'id' => $upload->id
        ]);
    }

    protected function thumbnailImage($request, $imageVarName = 'photo', $destination = 'post', $height = 100, $width = 100 ) {
        $images=[];
        if ($imageVarName == 'user_image') {
             $images = $request;
        } else {
            $images = $request->file($imageVarName);
        }


        $input = [];
        foreach ($images as $k => $image) {

            $imageOriginalName = $image->getClientOriginalName();
            $imageOriginalexe = $image->getClientOriginalExtension();
            $input['imagename'][$k] = time().$k . "." . $imageOriginalexe;
            $destinationPath = storage_path('images/'. $destination);

            Storage::disk('local')->putFileAs(
                'public/images/'. $destination,
                $image,
                $input['imagename'][$k]
            );
//            Storage::disk('s3')->put(
//                $destination.'/'.$input['imagename'][$k],
//                file_get_contents($image),
//                'public'
//
//            );
        }

        return $input;

        /* Code for create new row in database */
    }

    protected function base64Image($request, $imageVarName = 'photo', $destination = 'post', $height = 100, $width = 100 ) {


        $image = $request->file($imageVarName);
//        $imageP = Image::make($image->getpath());
//        dd($image->getpath(), $imageP);
//        $image = str_replace('data:image/png;base64,', '', $image);
//        $image = str_replace(' ', '+', $image);
//        $imageName = str_random(10).'.'.'png';
//
//        \File::put(public_path('images/'. $destination.'/'.$imageName), base64_decode($image));

        $input = [];

        $imageOriginalName = $image->getClientOriginalName();
        $imageOriginalexe = $image->getClientOriginalExtension();
//        dd($imageOriginalexe, $image);
        $input['imagename'] = time() . ".png";
        $destinationPath = public_path('images/'. $destination);

        Storage::disk('local')->putFileAs(
            'app/images/'. $destination,
            $image,
            $input['imagename']
        );

//        Storage::disk('s3')->put(
//            $destination,
//            $image,
//            $input['imagename']
//        );

        return $input;

        /* Code for create new row in database */
    }

}
