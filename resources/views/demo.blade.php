<html>
    <head>
        <title>Laravel</title>
        <style>
            ul {}
            ul li{ list-style: none}
            ul li img{ height: 30px}
        </style>
        <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    </head>
    <body>
    <div class="container">
        <h1>CSS Mask Demos</h1>

        <ul>
            <li class="demo" data-src="{{asset('images/demo/v1.png')}}">
                <img src="{{asset('images/demo/v1.png')}}">
            </li>
            <li class="demo" data-src="{{asset('images/demo/v2.png')}}">
                <img src="{{asset('images/demo/v2.png')}}">
            </li>
        </ul>
        
        
        <img src="data:image/png;base64,{{base64_encode($imgData)}}">
{{--        <input type="hidden" value="{{route('imageProcess')}}" id="image-path">--}}

    </div>

        <script>
            $('.demo').on('click', function () {
               var src = $(this).data('src');
               var path = $('#image-path').val();

                $.ajax({
                    url: path,
                    dataType: 'text',
                    type: 'post',
                    contentType: 'application/x-www-form-urlencoded',
                    data: $(this).serialize(),
                    success: function( data, textStatus, jQxhr ){
                        $('#response pre').html( data );
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log( errorThrown );
                    }
                });

            });
        </script>
    </body>
</html>
