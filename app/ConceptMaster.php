<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConceptMaster extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'concept_master';
    protected $primaryKey = 'id';

    public function concept() {
        return $this->hasMany(Concept::class, 'master_id');
    }
    public function conceptImage() {
        return $this->hasMany(ConceptImage::class, 'master_id');
    }
}
