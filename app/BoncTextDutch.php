<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoncTextDutch extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'b_text_dutch';
    protected $primaryKey = 'id';
}
