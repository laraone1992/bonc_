@extends('layouts.layout')
@section('title','Post')

@section('content')

    <!-- Content Start -->
    <div id="content" class="d-flex">
        <!-- =========================================
        Left Section
        ========================================== -->
        <section class="left-section sidebar">

{{--            <div class="recent-post">--}}
{{--                <div class="recent-post-head d-flex align-items-center justify-content-between">--}}
{{--                    <h3 class="mb-0">{{__('message.Recent_Posts')}}</h3>--}}

{{--                </div>--}}
{{--            </div>--}}
            <div class="recent-post recent-post-cnt">
                <div class="recent-post-body scroll-cust">
                    <div class="recent-post-block">
                        <div class="thumb">
                            <img src="{{asset('')}}images/img1.png" alt="">
                        </div>
                        <div class="caption">
                            <div class="cap-heading">
                                <h4> {{__('message.Need help to get started')}}</h4>
                                <p>{{__('message.To get you started')}} {{__('message.Starters_package')}}{!! __('message.We will create Business')!!}
                                <div class="help-form">
                                    <form action="{{url('need-help')}}" method="get" class="form" >
                                        <div class="row mt-3">
                                            <div class="col-md-3">
                                                <label class="">Name</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="cntName" required>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-3">
                                                <label class="">Phone</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control " name="cntPhone" required>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-3">
                                                <label class="">Email</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="email" class="form-control" name="cntEmail" required>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-3">

                                            </div>
                                            <div class="col-md-9">
                                                <input type="submit" class="btn btn-secondary help-form-send" name="cnt-submit" id="cnt-submit" value="Send">
                                            </div>
                                        </div>
                                    </form>
                                </div>
{{--{{__('message.Click_here')}}</a> {{__('message.consultants')}}--}}
                                </p>
                            </div>
{{--                            <div class="post-likes-wrap overlays">--}}
{{--                                <div class="text">Coming Soon..!</div>--}}
{{--                                <div class="post-likes d-flex align-items-center justify-content-between">--}}
{{--                                    <div class="d-flex align-items-center">--}}
{{--                                        <img src="{{asset('')}}images/facebook.svg" alt="" class="analytics">--}}
{{--                                        <div class="like-counter d-flex align-items-center">--}}
{{--                                            <span><img src="{{asset('')}}images/ic/like-icon.png" alt="">171</span>--}}
{{--                                            <span><img src="{{asset('')}}images/ic/comment-icon.png" alt="">1</span>--}}
{{--                                            <span><img src="{{asset('')}}images/ic/share-icon.png" alt="">102</span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <span class="cursor-pointer">--}}
{{--											<img src="{{asset('')}}images/ic/upload-icon.png" alt="">--}}
{{--										</span>--}}
{{--                                </div>--}}
{{--                                <div class="post-likes d-flex align-items-center justify-content-between">--}}
{{--                                    <div class="d-flex align-items-center">--}}
{{--                                        <img src="{{asset('')}}images/twitter.svg" alt="" class="analytics">--}}
{{--                                        <div class="like-counter d-flex align-items-center">--}}
{{--                                            <span><img src="{{asset('')}}images/ic/t-comment-icon.png" alt="">1</span>--}}
{{--                                            <span><img src="{{asset('')}}images/ic/t-share-icon.png" alt="">30</span>--}}
{{--                                            <span><img src="{{asset('')}}images/ic/icon-heart.png" alt="">51</span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <span class="cursor-pointer">--}}
{{--											<img src="{{asset('')}}images/ic/upload-icon.png" alt="">--}}
{{--										</span>--}}
{{--                                </div>--}}
{{--                                <div class="post-likes d-flex align-items-center justify-content-between">--}}
{{--                                    <div class="d-flex align-items-center">--}}
{{--                                        <img src="{{asset('')}}images/linkedin.svg" alt="" class="analytics">--}}
{{--                                        <div class="like-counter d-flex align-items-center">--}}
{{--                                            <span><img src="{{asset('')}}images/ic/like-icon.png" alt="">171</span>--}}
{{--                                            <span><img src="{{asset('')}}images/ic/comment-icon.png" alt="">1</span>--}}
{{--                                            <span><img src="{{asset('')}}images/ic/share-icon.png" alt="">102</span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <span class="cursor-pointer">--}}
{{--											<img src="{{asset('')}}images/ic/upload-icon.png" alt="">--}}
{{--										</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                    <div class="recent-post-block">
                        <div class="thumb">
                            <img src="{{asset('')}}images/ic/image-2.jpg" alt="">
                        </div>
                        <div class="caption">
                            <div class="cap-heading">
                                <h4>{{__('message.Your analytics on one page')}}</h4>
                                <p>{{__('message.Recent_Post_Content')}}</p>
                            </div>
                            <div class="post-likes-wrap overlays">
                                <div class="coming-soon-text">{{__('message.Coming Soon..!')}}</div>
                                <div class="post-likes d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center">
                                        <img src="{{asset('')}}images/facebook.svg" alt="" class="analytics">
                                        <div class="like-counter d-flex align-items-center">
                                            <span><img src="{{asset('')}}images/ic/like-icon.png" alt="">171</span>
                                            <span><img src="{{asset('')}}images/ic/comment-icon.png" alt="">1</span>
                                            <span><img src="{{asset('')}}images/ic/share-icon.png" alt="">102</span>
                                        </div>
                                    </div>
                                    <span class="cursor-pointer">
											<img src="{{asset('')}}images/ic/upload-icon.png" alt="" style="opacity: 0.3">
										</span>
                                </div>
                                <div class="post-likes d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center">
                                        <img src="{{asset('')}}images/twitter.svg" alt="" class="analytics">
                                        <div class="like-counter d-flex align-items-center">
                                            <span><img src="{{asset('')}}images/ic/t-comment-icon.png" alt="">1</span>
                                            <span><img src="{{asset('')}}images/ic/t-share-icon.png" alt="">30</span>
                                            <span><img src="{{asset('')}}images/ic/icon-heart.png" alt="">51</span>
                                        </div>
                                    </div>
                                    <span class="cursor-pointer">
											<img src="{{asset('')}}images/ic/upload-icon.png" alt="" style="opacity: 0.3">
										</span>
                                </div>
                                <div class="post-likes d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center">
                                        <img src="{{asset('')}}images/linkedin.svg" alt="" class="analytics">
                                        <div class="like-counter d-flex align-items-center">
                                            <span><img src="{{asset('')}}images/ic/like-icon.png" alt="">171</span>
                                            <span><img src="{{asset('')}}images/ic/comment-icon.png" alt="">1</span>
                                            <span><img src="{{asset('')}}images/ic/share-icon.png" alt="">102</span>
                                        </div>
                                    </div>
                                    <span class="cursor-pointer">
											<img src="{{asset('')}}images/ic/upload-icon.png" alt="" style="opacity: 0.3">
										</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- =========================================
        Content Section
        ========================================== -->
        <section class="content-section flex-fill scroll-cust">
            {{--            <div class="d-flex align-items-center justify-content-between mb-4">--}}
            {{--                <button type="button" class="side-button left-sidebutton btn btn-secondary">Left Sidebar</button>--}}
            {{--                <button type="button" class="side-button right-sidebutton btn btn-secondary">Right Sidebar</button>--}}
            {{--            </div>--}}
            @if(Session::has('success'))
                <p class="alert alert-success">{{ Session::get('success') }}</p>
            @endif
            @if(Session::has('error'))
                <p class="alert alert-danger">{{ Session::get('error') }}</p>
            @endif
            <div class="page-header post-page-header">{{__('message.postPageHeader')}}</div>
            <input type="hidden" id="varified" value="{{$varified}}">
            <form action="{{url('post-data')}}" method="post" enctype="multipart/form-data" name="post-form" id="post-form">
                @csrf
                {{--            <div class="card">--}}
                {{--                <div class="card-body py-0">--}}
                <div class="tab-content">
                    <div class="post-wrap">
                        <div class="post-block position-relative">
                            {{--                                <span class="post-small-log"><img src="{{asset('')}}images/ic/post-logo.png" alt=""></span>--}}
                            <div class="panel">
                                {{--                                    <div class="panel-head">--}}
                                {{--                                        Great to see you! Lets post something online!--}}
                                {{--                                    </div>--}}
                                <div class="panel-body p-0">
{{--                                    <div class="add-pic d-md-flex align-items-center --}}{{--justify-content-between--}}{{--">--}}
{{--                                        {{__('message.Add_picture_video')}}--}}
{{--                                        <input type="file" name="your_photo[]" id="your-photo" multiple>--}}
{{--                                        <div class="d-md-flex d-block ">--}}
{{--                                            <div class="btn-group pl-5 img-sel-post">--}}

{{--                                                <button type="button" class="btn btn-home-grey" id="your-photo-btn"--}}
{{--                                                        type="images">--}}
{{--                                                    <img src="{{asset('')}}images/upload.svg" class="wh15 inverse-clr" alt="">--}}
{{--                                                    {{__('message.Your_photos')}}--}}
{{--                                                </button>--}}
{{--                                                <button type="button" class="btn btn-home-grey" id="bonc-photo"--}}
{{--                                                        data-toggle="modal" data-target="#BoncModalPhoto">--}}
{{--                                                    <img src="{{asset('')}}images/upload.svg" alt="" class="wh15">--}}
{{--                                                    {{__('message.Bonc_photos')}}--}}
{{--                                                </button>--}}
{{--                                                <button type="button" class="btn btn-home-grey" data-toggle="modal"--}}
{{--                                                        data-target="#BoncModalQuote">--}}
{{--                                                    <img src="{{asset('')}}images/upload.svg" alt="" class="wh15">--}}
{{--                                                    {{__('message.Bonc_quotes')}}--}}
{{--                                                </button>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

                                    <div class="write-here row ">
                                        <div class="col-12">
                                            <textarea class="form-control" rows="4" name="text" id="text"
                                                      placeholder="{{__('message.Write_your_post')}}"></textarea>
                                        </div>
                                    </div>
                                    <div class="file-input">
                                        <div class="file-preview scroll-cust">
                                            <div class="file-drop-disabled">
                                                <div class="file-preview-thumbnails" id="selectedFiles">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="media-btn-group">

                                        <div class="row ">

                                            <div class="col-media">
                                                <button type="button" class="btn btn-home-grey btn-block " id="your-photo-btn"
                                                        type="images">
                                                    <img src="{{asset('')}}images/upload_black.svg" class="wh15 inverse-clr" alt="">
                                                    {{__('message.Your_photos')}}
                                                </button>
                                            </div>
                                            <div class="col-media">
                                                <button type="button" class="btn btn-home-grey btn-block " id="bonc-photo"
                                                        data-toggle="modal" data-target="#BoncModalPhoto">
                                                    <img src="{{asset('')}}images/photos.svg" alt="" class="wh15">
                                                    {{__('message.Bonc_photos')}}
                                                </button>
                                            </div>
                                            <div class="col-media">
                                                <button type="button" id="BoncQuote" class="btn btn-home-grey btn-block " data-toggle="modal"
                                                        data-target="#BoncModalQuote">
                                                    <img src="{{asset('')}}images/quote.svg" alt="" class="wh15">
                                                    {{__('message.Bonc_quotes')}}
                                                </button>
                                            </div>
                                            <div class="col-media">
                                                <button type="button" id="text_select"
                                                        class="btn btn-home-grey btn-block ">
                                                    <img src="{{asset('')}}images/bonctext.svg" alt="" class="wh15">
                                                    {{__('message.Bonc_text')}}
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="post-on d-md-flex align-items-center ">
                                        {{__('message.Post_on')}}
                                        <div class="post-persons d-flex flex-wrap">
                                            @foreach($channels['data'] as $key => $channel)
                                                @if($channel['type'] != 'facebook_profile')
                                                    <div class="posters position-relative">
                                                        @if($channel['type'] == 'facebook_profile')
                                                        @elseif($channel['type'] == 'facebook_page')
                                                            <img src="{{asset('')}}images/facebook.svg"
                                                                 class="social-post" alt="">
                                                        @elseif($channel['type'] == 'linkedin_profile' || $channel['type'] == 'linkedin_page')
                                                            <img src="{{asset('')}}images/linkedin.svg"
                                                                 class="social-post" alt="">
                                                        @elseif($channel['type'] == 'twitter')
                                                            <img src="{{asset('')}}images/twitter.svg"
                                                                 class="social-post" alt="">
                                                        @endif
                                                        <span class="post-user">
                                                                <input type="checkbox" id="cb{{$key}}"
                                                                       data-id="{{$channel['uuid']}}"
                                                                       name="channel[{{$channel['type']}}]"
                                                                       class="channel-chk"/>
                                                                <label for="cb{{$key}}">
                                                                    <img src="{{$channel['profile_picture_url']}}"
                                                                         alt=""
                                                                         onerror="this.onerror=null;this.src='{{asset('images/noimage.png')}}';">
                                                                </label>
                                                            </span>
                                                        <input type="hidden" name="uuid[{{$channel['type']}}]"
                                                               value="{{$channel['uuid']}}"/>
                                                    </div>
                                                @endif

                                            @endforeach
                                            <div class="posters position-relative">

                                                <img src="{{asset('')}}images/instagram.svg" class="social-post" alt="">
                                                <span class="post-user">
                                                            <input type="checkbox" id="cb{{count($channels['data'])+1}}"
                                                                   data-id="123123" name="channel[instagram]"
                                                                   class="channel-chk"/>
                                                            <label for="cb{{count($channels['data'])+1}}">
                                                                <img src="" alt=""
                                                                     onerror="this.onerror=null;this.src='{{asset('images/noimage.png')}}';">
                                                            </label>
                                                        </span>
                                                <input type="hidden" name="uuid[instagram]" value="123123"/>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <input type="file" name="your_photo[]" id="your-photo" multiple>
                        </div>
                        <div>
                            <div class="btn-post-grp pull-right" id="btn-grp-1">
                                <button class="btn btn-secondary btn-cnfm btn-lg" id="postNow" type="submit">{{__('message.Post')}}</button>
                                <button class="btn btn-secondary btn-cnfm btn-lg" id="schedule" type="button">{{__('message.Schedule')}}</button>
                                <button class="btn btn-secondary btn-cnfm btn-lg" id="saveD" type="submit">{{__('message.Concept')}}</button>
                            </div>
                        </div>
                        <div class="post-date-block post-block position-relative d-none" >
                            {{--                                <span class="post-small-log"><img src="{{asset('')}}images/ic/post-logo.png" alt=""></span>--}}
                            <div class="panel">

                                <div class="panel-body">
                                    <div class="date-time-wrap">
                                        @foreach($channels['data'] as $key => $channel)
                                            @if($channel['type'] != 'facebook_profile')
                                                <div class="date-time-block position-relative disable-block"
                                                     id="date-time-{{$channel['uuid']}}">
                                                    @if($channel['type'] == 'facebook_profile')
                                                        <img src="{{asset('')}}images/facebook.svg" class="lg-social"
                                                             alt="">
                                                    @elseif($channel['type'] == 'facebook_page')
                                                        <img src="{{asset('')}}images/facebook.svg" class="lg-social"
                                                             alt="">
                                                    @elseif($channel['type'] == 'linkedin_profile' || $channel['type'] == 'linkedin_page')
                                                        <img src="{{asset('')}}images/linkedin.svg" class="lg-social"
                                                             alt="">
                                                    @elseif($channel['type'] == 'twitter')
                                                        <img src="{{asset('')}}images/twitter.svg" class="lg-social"
                                                             alt="">
                                                    @endif
                                                    <div class="row">
                                                        <div class="col-7">
                                                            <div class="d-flex align-items-center date-group">
                                                                <label>{{__('message.Date')}}</label>
                                                                <div class="datepick flex-fill form-group">
                                                                    <input type="text"
                                                                           class="form-control dateSelect "
                                                                           id="date{{$key}}"
                                                                           value="{{\Carbon\Carbon::parse($date)->format('d/m/Y')}}"
                                                                           name="date[{{$channel['type']}}]"
                                                                           readonly='true' placeholder="dd/mm/yyyy">
                                                                    <span class="cal-ico date-ico" for="date{{$key}}">
                                                                        <img src="{{asset('')}}images/calendar.svg">
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-5">
                                                            <div class="d-flex align-items-center time-group">
                                                                <label>{{__('message.Time')}}</label>
                                                                <div class="datepick flex-fill form-group">
                                                                    <input type="text" class="form-control timeSelect"
                                                                           id="time{{$key}}"
                                                                           value="{{\Carbon\Carbon::parse($date)/*->addHour()->startOfHour()*/->format('H:i')}}"
                                                                           name="time[{{$channel['type']}}]"
                                                                           placeholder="hh:mm">
                                                                    <span class="cal-ico time-ico" for="time{{$key}}">
                                                                        <img src="{{asset('')}}images/clock.svg">
                                                                </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach

                                        <div class="date-time-block position-relative disable-block"
                                             id="date-time-123123">
                                            <img src="{{asset('')}}images/instagram.svg" alt="" class="lg-social">
                                            <div class="row">
                                                <div class="col-7">
                                                    <div class="d-flex align-items-center date-group">
                                                        <label>{{__('message.Date')}}</label>
                                                        <div class="datepick flex-fill form-group">
                                                            <input type="text" class="form-control dateSelect"
                                                                   id="date40"
                                                                   value="{{\Carbon\Carbon::parse($date)->format('d/m/Y')}}"
                                                                   name="date[instagram]" readonly='true'
                                                                   placeholder="dd/mm/yyyy">
                                                            <span class="cal-ico date-ico" for="date40">
                                                                        <img src="{{asset('')}}images/calendar.svg">
                                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-5">
                                                    <div class="d-flex align-items-center time-group">
                                                        <label>{{__('message.Time')}}</label>
                                                        <div class="datepick flex-fill form-group">
                                                            <input type="text" class="form-control timeSelect"
                                                                   id="time40"
                                                                   value="{{\Carbon\Carbon::parse($date)/*->addHour()->startOfHour()*/->format('H:i')}}"
                                                                   name="time[instagram]" readonly='true'
                                                                   placeholder="hh:mm">
                                                        </div>
                                                        <span class="cal-ico time-ico" for="time40">
                                                                    <img src="{{asset('')}}images/clock.svg">
                                                                </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="btn-post-grp pull-right">
                                <button class="btn btn-secondary btn-cnfm btn-lg" id="submit"
                                        type="submit">{{__('message.Confirm')}}</button>
                                <button class="btn btn-secondary btn-cnfm btn-lg" id="save"
                                        type="submit">{{__('message.Concept')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
        <div class="modal " id="BoncModalText" role="dialog" aria-labelledby="exampleModalCenterTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header black-header">
                        <span>{{__('message.Bonc_text')}}</span>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="close-popup-btn">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body bonc-txt-body">
                         @if($subcriptionType == 'FREE')
                        <div class="text-center upgrade-text">Sorry {{$userDetails->name}}, only members with a paid account can use the Bonc texts.<br> Upgrade now and you can choose out of our library with thousands of royalty free texts.</div>
                        <div class="text-center upgrade-button">
                            <input type="button" class="btn btn-secondary upgrade_now" name="update" id="updateSubcription" value="Upgrade Now">
                        </div>
                        @else
                        <select class="form-control bonc-txt-select" name="category" id="category">
                            <option value="__">{{__('message.Select_category')}}</option>
                            @foreach($textCategory as $category)
                                <option value="{{$category['category']}}">{{$category['category']}}</option>
                            @endforeach
                        </select>
                        <input type="hidden" id="org_name" value="">
                        <ul class="bonc-txt-ul" id="list_text">

                        </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content welcome-modal-content">
      <div class="modal-header modal_header_title">
        <h4 class="js-title-step"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

      </div>
        <div class="modal-body welcome-body">
        <div class="d-none" data-step="1" data-title="Welcome">
            <div class="col-md-12">
                <img src="{{asset('images/ic/Welcome.png')}}" class="welcome-image">
                <p class="welcome-text">Welcome  at Bonc!  We are excited to start working together on boosting your sales! In 5 simple steps we will explain to you how Bonc works. Many things are optional, the only thing you need to do before you can start is connect your social media channels to your Bonc account. You can do this by clicking on this button.</p>
            </div>
        </div>
        <div class="d-none" data-step="2" data-title="The basics">
          <div class="col-md-12 text-center">
              <img src="{{asset('images/ic/basics.png')}}" class="basics-image">
              <p class="welcome-text">Posting messages on all your channels in one time with Bonc is super simple. You write your text and  select a photo. You can select photos from your own gallery or you can use royalty free Bonc photos or Quotes. If you do not know what to write, click on “Bonc text” and select a text that inspires you! When you are done, you click on the channels  you want use to share your thoughts and Bonc will post them  for you!  It is as simple as it sounds!</p>
          </div>
        </div>
        <div class="d-none" data-step="3" data-title="Branding photo’s">
          <div class="col-md-12 text-center"> <img src="{{asset('images/ic/schedulling.png')}}" class="branding-image"></div>
          <p class="branding-text">If you want to add your logo to photos you click on the ‘edit icon’ you see in the photo you selected and you will can add your logo in the photo. You can change the size and / or position of the logo. If you want, you can also add funny or informative stickers from the Bonc library.  Try it….it’s fun and looks professional!</p>
        </div>
         <div class="d-none" data-step="4" data-title="Scheduling">
          <div class="col-md-12 text-center"> <img src="{{asset('images/ic/schedulling.png')}}" class="branding-image"></div>
          <p class="branding-text">When your post is ready, you can click on “post” and your post will be posted right away. But if you want to schedule your post for later, you click on “schedule” and you can schedule your post in just a few clicks. For each channel separately, if you want!</p>
        </div>
         <div class="d-none" data-step="5" data-title="Concept">
          <div class="col-md-12 text-center"> <img src="{{asset('images/ic/concept.png')}}" class="concept-image"></div>
          <p class="branding-text">Are you not ready to post your partially written post? Or you have no inspiration what to write with the photo you made? No problem, you can click anytime you want on the “concept” button and Bonc will save your unfinished post on the concept page. When you are ready, you (or someone else!) can finish the post and post or schedule it! <br>
              These are the basic steps we wanted to introduce to you. If you need any assistance , do not hesitate to chat with one of our Bonc consultants.<br>
              Good luck with boosting your business with Bonc!
          </p>
        </div>
      </div>
      <div class="modal-footer welcome-footer">
          <button type="button" class="btn btn-success js-btn-step" style="margin-right: auto;" data-orientation="previous"></button>
          <a href="{{url('setting')}}" class="js-btn-channel add_channel" data-orientation="add_channel" ></a>
        <button type="button" class="btn btn-success js-btn-step" data-orientation="next"></button>
      </div>
    </div>
  </div>
</div>
<!--        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
  Launch wizard modal
</button>
        <div class="modal " id="BoncWelcomeText" role="dialog" aria-labelledby="exampleModalCenterTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header black-header">
                        <span>Thank you</span>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="close-popup-btn">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body bonc-txt-body">
                        <p>Welcome at Bonc!<br>
                            Thank you for choosing Bonc to manage your social media exposure.  We are looking forward to assist you by boosting your business online.<br><br>
                            If you want, you can upload your company logo(‘s) so that we can add them to your posts and, depending on your subscription you can add users who can represent your company using Bonc.<br><br>
                            The only thing you need to do before you can start is connecting one or more of your social media channels to your Bonc account. We made this very easy for you, so <a href="{{url('setting')}}" style="color: #000;">click here</a> to connect LinkedIn, Facebook, Twitter or Instagram to your account.<br><br>
                            We are expending and the coming months we will introduce many new functionalities.<br><br>
                            Enjoy your 100% FREE trial!  </p>
                    </div>
                </div>
            </div>
        </div>-->
        <div class="modal " id="BoncModalPhoto" role="dialog" aria-labelledby="BoncModalCenterTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header black-header">
                        <span>{{__('message.Bonc_photo_text')}}</span>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="close-popup-btn">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body bonc-photo-body" id="list_scroll">
{{--                        <select class="form-control bonc-txt-select" name="photo_category" id="photo_category">--}}
{{--                            <option value="__">{{__('message.Select_category')}}</option>--}}
{{--                            @foreach($boncPhoto as $category)--}}
{{--                                <option value="{{$category['category']}}">{{$category['category']}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
                        <input type="hidden" value="" name="rowCount" id="rowCount">
                        @if($subcriptionType == 'FREE')
                        <div class="text-center upgrade-text">Sorry {{$userDetails->name}}, only members with a paid account can use the Bonc photos.<br> Upgrade now and you can choose out of our library with thousands of royalty free photos.</div>
                        <div class="text-center upgrade-button">
                            <input type="button" class="btn btn-secondary upgrade_now" name="update" id="updateSubcription" value="Upgrade Now">
                        </div>
                        @else
                        <div class="bonc-photo-div" id="list_text">
                            <div class="bonc-media-list" id="list_photo">

                            </div>
                            <div id="greybag" class="bonc-media-list d-none">
                                <div class="example1 c-animated-background">
                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    @if($subcriptionType != 'FREE')
                        <div class="modal-footer icon-model-footer">
                            <button type="button" class="btn btn-secondary"  data-dismiss="modal">Select</button>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="modal " id="BoncModalQuote" role="dialog" aria-labelledby="BoncModalCenterTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header black-header">
                        <span >{{__('message.Bonc_quote_text')}}</span>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="close-popup-btn">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body bonc-photo-body" id="list_scroll_quote">
{{--                        <select class="form-control bonc-txt-select" name="quote_category" id="quote_category">--}}
{{--                            <option value="__">{{__('message.Select_category')}}</option>--}}
{{--                            @foreach($boncQuote as $category)--}}
{{--                                <option value="{{$category['category']}}">{{$category['category']}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
                        <input type="hidden" value="" name="rowCountQuote" id="rowCountQuote">
                         @if($subcriptionType == 'FREE')
                        <div class="text-center upgrade-text">Sorry {{$userDetails->name}}, only members with a paid account can use the Bonc quotes.<br> Upgrade now and you can choose out of our library with thousands of royalty free quotes.</div>
                        <div class="text-center upgrade-button">
                            <input type="button" class="btn btn-secondary upgrade_now" name="update" id="updateSubcription" value="Upgrade Now">
                        </div>
                        @else
                        <div class="bonc-photo-div" id="list_text">
                            <div class="bonc-media-list" id="list_quote">


                            </div>
                            <div id="greybagquote" class="bonc-media-list d-none">
                                <div class="example1 c-animated-background">
                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                                <div class="example1 c-animated-background">

                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    @if($subcriptionType != 'FREE')
                        <div class="modal-footer icon-model-footer">
                             <button type="button" class="btn btn-secondary"  data-dismiss="modal">Select</button>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <!-- =========================================
        Right Section
        ========================================== -->
        <section class="right-section sidebar scroll-cust">
            <div class="right-sec-top">
                <h4>{{__('message.Sidebar_right_question')}}</h4>
            </div>
            <div class="right-sec-header">
                {{__('message.Sidebar_right_content')}}
            </div>
            <div class="side-accord">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne"
                             aria-expanded="false" aria-controls="collapseOne">
                            {{__('message.How_to_create_LinkedIn_account')}}
                            <span class="acc-arrow">
									<em class="fa fa-angle-right"></em>
								</span>
                        </div>

                        <div id="collapseOne" class="collapse " aria-labelledby="headingOne"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                {{__('message.How_to_create_LinkedIn_account_content')}}
                                <a href="https://vimeo.com/310342097" target="_blank"> Click here </a> to watch the
                                video

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo"
                             aria-expanded="false" aria-controls="collapseTwo">
                            {{__('message.How_to_create_Twitter_business_account')}}
                            <span class="acc-arrow">
									<em class="fa fa-angle-right"></em>
								</span>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                {{__('message.How_to_create_Twitter_business_account_content')}}
                                <a href="https://vimeo.com/310342245"
                                   target="_blank"> {{__('message.Click_here')}} </a> {{__('message.to_watch_the_video')}}
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree"
                             aria-expanded="false" aria-controls="collapseThree">
                            {{__('message.How_to_create_Facebook_business_account')}}
                            <span class="acc-arrow">
									<em class="fa fa-angle-right"></em>
								</span>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                {{__('message.How_to_create_Facebook_business_account_content')}}

                                <a href="https://vimeo.com/310341968"
                                   target="_blank"> {{__('message.Click_here')}} </a> {{__('message.to_watch_the_video')}}


                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <img src="{{asset('')}}images/chart-image.png" height="100%" width="100%">
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour" data-toggle="collapse" data-target="#collapseFour"
                             aria-expanded="false" aria-controls="collapseFour">
                            {{__('message.How_to_create_Instagram_business_account')}}
                            <span class="acc-arrow">
									<em class="fa fa-angle-right"></em>
								</span>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                {{__('message.How_to_create_Instagram_business_account_content')}}

                                <a href="https://vimeo.com/310342044"
                                   target="_blank"> {{__('message.Click_here')}} </a> {{__('message.to_watch_the_video')}}


                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingFive" data-toggle="collapse" data-target="#collapseFive"
                             aria-expanded="false" aria-controls="collapseFive">
                            {{__('message.How_to_attract_the_attention_of_your_target_audience')}}
                            <span class="acc-arrow">
									<em class="fa fa-angle-right"></em>
								</span>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                {{__('message.How_to_attract_the_attention_of_your_target_audience_content')}}
                                <a href="https://bonconline.com/en/blog/online-marketingtrends.html"
                                   target="_blank"> {{__('message.Click_here')}} </a>
                            </div>
                        </div>
                    </div>
                </div>
                {{--                <div class="accordion" id="accordionExample">--}}
                {{--                    <div class="card">--}}
                {{--                        <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">--}}
                {{--                            {{__('message.Help_Me_Post')}}--}}
                {{--                            <span class="acc-arrow">--}}
                {{--									<em class="fa fa-angle-right"></em>--}}
                {{--								</span>--}}
                {{--                        </div>--}}

                {{--                        <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">--}}
                {{--                            <div class="card-body">--}}
                {{--                                {{__('message.Help_Me_Post_Content')}}--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                    <div class="card">--}}
                {{--                        <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">--}}
                {{--                            {{__('message.Do_not_know_schedule_post')}}--}}
                {{--                            <span class="acc-arrow">--}}
                {{--									<em class="fa fa-angle-right"></em>--}}
                {{--								</span>--}}
                {{--                        </div>--}}
                {{--                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">--}}
                {{--                            <div class="card-body">--}}
                {{--                                {{__('message.Do_not_know_schedule_post_content')}}--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                    <div class="card">--}}
                {{--                        <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">--}}
                {{--                            {{__('message.Need_Help_with_Settings')}}--}}
                {{--                            <span class="acc-arrow">--}}
                {{--									<em class="fa fa-angle-right"></em>--}}
                {{--								</span>--}}
                {{--                        </div>--}}
                {{--                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">--}}
                {{--                            <div class="card-body">--}}
                {{--                                {{__('message.Need_Help_with_Settings_content')}}--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>

        </section>
    </div>

    <div class="modal " id="postEditModal" tabindex="-1" role="dialog" aria-labelledby="postEditModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-post-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <span>Edit Photo</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body post-modal-body d-flex">
                    <div class="modal-sidebar d-flex">
                        <div class="post-menu-tab">
                            <button class="post-tablinks" onclick="openCnt(event, 'text_post')"><img
                                    src="{{asset('images/text.png')}}"></button>
                            <button class="post-tablinks" onclick="openCnt(event, 'filter')"><img
                                    src="{{asset('images/post_filter.png')}}"></button>
                            <button class="post-tablinks" onclick="openCnt(event, 'post-templates')"><img
                                    src="{{asset('images/template.png')}}"></button>
                            <button class="post-tablinks" onclick="openCnt(event, 'post-upload')"><img
                                    src="{{asset('images/post_upload.png')}}"></button>
                        </div>

                        <div id="text_post" class="post-tabcontent active">

                            <div class="text-section-post">
                                <button class="btn btn-secondary btn-add-text">
                                    <i class="mdi mdi-plus"></i> Add Text
                                </button>

                                <select name="post-font-text" class="form-control post-font-text">
                                    <option value="Arial">Arial</option>
                                    <option value="Titillium Web">Titillium Web</option>
                                    <option value="sans-serif">Sans Serif</option>
                                </select>

                                <div class="color-align-post row">
                                    <div class="col-6">
                                        <button class="jscolor {valueElement:null,value:'000'}"
                                                style="width:30px; height:30px;  border: none"></button>
                                    </div>
                                    <div class="col-6">
                                        <ul class="align-group-post">
                                            <li>
                                                <i class="mdi mdi-format-align-left"></i>
                                            </li>
                                            <li>
                                                <i class="mdi mdi-format-align-center"></i>
                                            </li>
                                            <li>
                                                <i class="mdi mdi-format-align-right"></i>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div id="filter" class="post-tabcontent">
                            <div class="filters">
                                <ul>
                                    <li>
                                        <input type="button" class="img_action" id="neg" value="Negative">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="blr" value="Blur">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="brg" value="Brighten">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="clr" value="Colorize">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="cntr" value="Contrast">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="edgd" value="Edge Detect">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="gray" value="Grayscale">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="mean" value="Mean">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="seleb" value="Selective Blur">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="smth" value="Smoothen">
                                    </li>
                                </ul>

                            </div>

                        </div>

                        <div id="post-templates" class="post-tabcontent">
                            <div class="templates-div" id="designs">
                                <input type="text" class="search-inp" placeholder="Search Template">
                                <div class="templates-list">
                                    <div class="cat-title d-flex">
                                        <div class="w-50 d-flex">
                                            <span>Category 1</span>
                                        </div>
                                        <div class="w-50 d-flex">
                                            <a class="see_all" href="#">See all</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="template-img">
                                        <a href="#">
                                            <img src="{{asset('images/frame/frame1.png')}}" class="design ">
                                        </a>
                                        <a href="#">
                                            <img src="{{asset('images/frame/frame1.png')}}" class="design ">
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="templates-list">
                                    <div class="cat-title d-flex">
                                        <div class="w-50 d-flex">
                                            <span>Category 2</span>
                                        </div>
                                        <div class="w-50 d-flex">
                                            <a class="see_all" href="#">See all</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="template-img">
                                        <a href="#">
                                            <img src="{{asset('images/frame/frame1.png')}}" class="design ">
                                        </a>
                                        <a href="#">
                                            <img src="{{asset('images/frame/frame1.png')}}" class="design ">
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="templates-list">
                                    <div class="cat-title d-flex">
                                        <div class="w-50 d-flex">
                                            <span>Category 3</span>
                                        </div>
                                        <div class="w-50 d-flex">
                                            <a class="see_all" href="#">See all</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="template-img">
                                        <a href="#">
                                            <img src="{{asset('images/frame/frame1.png')}}" class="design ">
                                        </a>
                                        <a href="#">
                                            <img src="{{asset('images/frame/frame1.png')}}" class="design ">
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="templates-list">
                                    <div class="cat-title d-flex">
                                        <div class="w-50 d-flex">
                                            <span>Category 4</span>
                                        </div>
                                        <div class="w-50 d-flex">
                                            <a class="see_all" href="#">See all</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="template-img">
                                        <a href="#">
                                            <img src="{{asset('images/frame/frame1.png')}}" class="design ">
                                        </a>
                                        <a href="#">
                                            <img src="{{asset('images/frame/frame1.png')}}" class="design ">
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="post-upload" class="post-tabcontent">
                            <div class="post-upload">
                                <div class="input-upload">
                                    <button class="btn btn-secondary btn-add-text" id="add-logo-btn">
                                        <i class="mdi mdi-plus"></i> Add Logo
                                    </button>
                                    <input style="display: none" type="file" class="upload-post" id="your-logo-btn">
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="post-model-content d-flex">
                        <div id="preview">
                            <div id="draggable-icon">
                                <img src="" id="icon-your-logo">
                            </div>
                            <div id="crop-area">
                                <img src="" id="profile-pic"/>
                            </div>
                            <img src="" id="fg" data-design="0"/>
                            <input type="hidden" name="new_uploaded_image" id="new_uploaded_image" value="">
                            <input type="hidden" name="active_image" id="active_image" value="">
                        </div>
                        <div class="editor_div">
                            <div id="editor">

                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal " id="postIconModal" tabindex="-1" role="dialog" aria-labelledby="postIconModalLabel"
         aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header black-header">
                    <span>{{__('message.addlogo')}}</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="close-popup-btn">&times;</span>
                    </button>
                </div>
                <div class="modal-super">
                    <div class="grey-background">
                        <ul class="nav nav-tabs my-tab-icon" id="myTabIcon" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="compay-icon-tab" data-toggle="tab" href="#compay-icon" role="tab" aria-controls="compay-icon"
                                   aria-selected="true">Logo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="stickers-tab" data-toggle="tab" href="#stickers" role="tab" aria-controls="stickers"
                                   aria-selected="false">Stickers</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="compay-icon" role="tabpanel" aria-labelledby="compay-icon-tab">
                            <div class="company-icon-sec">
                                @if(count($companyIcon) >= 1)
                                    <ul class="company-icon-ul">
                                        @foreach($companyIcon as $i => $ico)
                                            <li class="company-icon-list">
                                                <img src="{{asset('storage/images/logo/'.$ico['image'])}}" class="icon-select @if($i == 0) active @endif ">
                                            </li>
                                        @endforeach
                                    </ul>
                                @else
                                    <p>{{__('message.nologotxt')}}<a href="{{url('setting')}}"> {{__('message.clickhere')}}</a></p>
                                @endif
                            </div>
                        </div>
                        <div class="tab-pane fade" id="stickers" role="tabpanel" aria-labelledby="stickers-tab">
                            <div class="bonc-sticker-btn-sec">
                                <button class="btn btn-home-grey choose-sticker" id="choose-sticker">Choose Sticker</button>
                            </div>
                            <div class="bonc-sticker-sec" id="bonc-sticker-sec">
                                <div class="bonc-sticker-back">
                                    <a class="" href="#" id="back-to-image"><i class="mdi mdi-chevron-left"></i> Back to Image</a>
                                </div>
                                <div class="bonc-stickers">
                                    <div class="bonc-st-cat">
                                        <select class="form-control bonc-txt-select" name="sticker_category" id="sticker_category">
                                            <option value="__">{{__('message.Select_category')}}</option>
                                            @foreach($boncSticker as $category)
                                                <option value="{{$category['category']}}">{{$category['category']}}</option>
                                            @endforeach
                                        </select>
{{--                                        <span class="bonc-sticker-cat-head">Welcome</span>--}}
                                        <ul class="bonc-st-ul" id="sticker_bonc">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body icon-model-body">
                    <div id="icon-preview" class="photo-icon-edit-sec">
                        <div class="main-image">
                            <img src="" id="main-image-m">
                        </div>
                        @if(count($companyIcon) >= 1)
                        <div id="icon-draggable-icon" class="icon-draggable-sec">
                            <img src="{{asset('storage/images/logo/'.$companyIcon[0]['image'])}}" class="image-logo " id="icon-selected-logo" >
                            <div class="delete remove-logo"><a href="#"><i class="fa fa-remove remove-logo"></i></a></div>
                        </div>
                        @endif

                    </div>
                    <div class="editor_div">
                        <div id="icon-editor">

                        </div>
                    </div>

                </div>
                <div class="modal-footer icon-model-footer">
                    <button type="button" class="btn btn-secondary" id="addIcon" data-dismiss="modal">{{__('message.save')}}</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="baseURL" value="{{url('')}}">
@endsection

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
    <link href="{{asset('')}}css/post/croppie.css" rel="stylesheet" async="async"/>
    {{--    <link href="{{asset('')}}css/post/style.css" rel="stylesheet" async="async" />--}}
    <link href="{{asset('css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
{{--    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/themes/cupertino/jquery-ui.css" />--}}
    <link href="{{asset('css/jquery-ui.css')}}" media="all" rel="stylesheet" type="text/css"/>

    <style>
        #your-photo {
            display: none;
        }

        .file-caption-main {
            display: none
        }

        input[type="checkbox"][id^="cb"] {
            display: none;
        }

        .krajee-default .file-upload-indicator {
            float: left;
            margin-top: 1px;
            width: 16px;
            height: 16px;
        }

        .krajee-default.file-preview-frame .kv-file-content {
            width: 110px;
            height: 110px;
        }

        .krajee-default .file-footer-caption {
            font-size: 9px;
            margin-bottom: 0;
        }

        .file-footer-buttons .btn {
            border: none;
            font-size: 12px;
            padding: 0 10px;
        }

        .file-preview {
            border: none;
            border-bottom: 2px solid #f6f6f6;
            border-radius: 0;
        }

        .krajee-default .file-upload-indicator {
            display: none
        }

        label {
            /*border: 1px solid #fff;*/
            padding: 0px;
            display: block;
            position: relative;
            margin: 0px;
            cursor: pointer;
        }

        label:before {
            background-color: white;
            color: white;
            content: " ";
            display: block;
            border-radius: 50%;
            border: 1px solid #8EC451;
            position: absolute;
            top: 26px;
            left: 0px;
            width: 15px;
            height: 15px;
            text-align: center;
            line-height: 15px;
            font-size: 8px;
            transition-duration: 0.4s;
            transform: scale(0);
            z-index: 9;
        }

        label img {
            height: 50px;
            width: 50px;
            transition-duration: 0.2s;
            transform-origin: 50% 50%;
            border-radius: 100%;
            filter: grayscale(1);
        }

        :checked + label {
            border-color: #ddd;
        }

        :checked + label:before {
            content: "✓";
            background-color: #8EC451;
            transform: scale(1);

        }

        :checked + label img {
            transform: scale(0.9);
            z-index: -1;
            filter: grayscale(0);
        }

        .select2-container--default .select2-selection--single {
            width: 375px;
            padding: 2px;
            height: 35px;
        }

        .select2-container--open .select2-dropdown--below {
            width: 100%;
        }

        /*.select2-container--default .select2-selection--single .select2-selection__arrow {*/
        /*    right: -265px;*/
        /*}*/
        .icon-draggable-sec:hover{
            border: 4px dashed black;
        }
        .icon-draggable-sec {
            position: relative;
            display: inline-block;
        }

        .icon-draggable-sec:hover .delete {
            display: block;
        }
        .border_style{
             border: 4px dashed black;
        }

        .delete {
            padding: 6px 11px;
            position: absolute;
            right: -15px;
            top: -17px;
            display: none;
            background-color: #000;
            width: 30px;
            height: 30px;
            border-radius: 20px;
            cursor: pointer;
            font-size: 13px;
            z-index: 99;
        }

        .delete a {
            color: #fff;
        }
        .delete a i{
            top: 30%;
            position: absolute;
        }
        .select2-container{
            width: 100% !important;
        }
        .select2-container--default .select2-selection--single{
            width: 100%;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow{
            right: 10px;
        }
        .upgrade-text{
            font-size: larger;
            max-width: 80%;
            margin: 35% auto;
        }
        .upgrade-button{
            margin-top: 15px;
        }

</style>
@endpush
@push('scripts')

    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

    <script src="{{asset('')}}js/post/croppie.min.js" async="async"></script>
    <script src="{{asset('')}}js/post/app.js" async="async"></script>
    <script src="{{asset('')}}js/jscolor.js"></script>
    {{--    <script src="{{asset('js/post/fileinput.js')}}"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/js/plugins/piexif.min.js"
            type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/js/plugins/sortable.min.js"
            type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/js/plugins/purify.min.js"
            type="text/javascript"></script>
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>--}}
    {{--    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>--}}


    {{--    <script src='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/js/jquery.circliful.min.js'></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"
            integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30=" crossorigin="anonymous"></script>
    <script src="{{asset('js/jquery.ui.touch-punch.js')}}" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js"></script>
    <script src="{{asset('js/html2canvas.js')}}"></script>
{{--    <link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/redmond/jquery-ui.css" rel="stylesheet" />--}}

    <script type="text/javascript">
        $(document).ready(function () {

            // $('#notificationList').modal('show');

        curSel = '';
            // $("#your-photo").fileinput({
            //     'showUpload':false,
            //     'dropZoneEnabled': false,
            //     'allowedFileTypes':['image'],
            //     'allowedFileExtensions':['jpg', 'png'],
            //     'autoReplace':false,
            //     'maxFileCount':0
            // });
            // $("#your_photo").fileinput({'showUpload':false, 'previewFileType':'any', 'dropZoneEnabled': false});

        });
        jQuery.browser = {};
        (function () {
            jQuery.browser.msie = false;
            jQuery.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                jQuery.browser.msie = true;
                jQuery.browser.version = RegExp.$1;
            }
        })();
        $('#myModal').modalSteps();
        $(".js-btn-step[data-orientation=next]").on('click',function(){
                var data=$(".js-btn-step[data-orientation=previous]").data('step');
                if(data > 0){

                $('.js-btn-channel').addClass('d-none');
                }

            });
          $(".js-btn-step[data-orientation=previous]").on('click',function(){
                    if($(this).hasClass('d-none')) {

                        $('.js-btn-channel').removeClass('d-none');
                    }

                });
          $('.upgrade_now').on('click',function(){
            $.ajax({
                    type: "GET",
                    headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                    url: "subcription/update",
                    data:{subscription_name:'STARTER',country_code:'NLD'},
                    dataType: "json",
                    success: function (data) {

                        console.log(data.data.payment_link);
                        window.open(data.data.payment_link,'width=300,height=200');
                    },
                    failure: function (response) {
                        alert("No Match");
                    }
                });
           });
        $('#draggable-icon').draggable({
            containment:"#icon-preview"
        }).resizable({
            autoHide:true,
        });
        $('#icon-draggable-icon').resizable({
            aspectRatio: true,
            handles: 'all',
            containment:"#icon-preview",
        }).draggable({
            cursor: "crosshair",
            containment:"#icon-preview"
        });
        $('#category').select2();
        $('#photo_category').select2();
        $('#quote_category').select2();
        $('#sticker_category').select2();

        $(".alert").delay(3000).slideUp(800, function () {
            $(this).alert('close');
        });
        $('#schedule').click(function () {
            $('.post-date-block').removeClass('d-none');
            $('#btn-grp-1').addClass('d-none');

        });
        $('.icon-draggable-sec').click(function(){
               $('.icon-draggable-sec').addClass('border_style');
               $(".icon-draggable-sec:hover .delete").css({display: "block"});
            });
        // $('#main-image-m').click(function(){
        //      $('#icon-draggable-icon').removeClass('border_style');
        //      $(".delete").css({display: ""});
        // });
        $('.channel-chk').click(function () {
            var sel = $(this);
            var uuid = sel.data('id');

            var chacked = sel.is(':checked');

            if (chacked) {
                // $('#date-time-'+sel.data('id')).removeClass('d-none');
                $('#date-time-' + sel.data('id')).removeClass('disable-block');
            } else {
                // $('#date-time-'+sel.data('id')).addClass('d-none');
                $('#date-time-' + sel.data('id')).addClass('disable-block');
            }

        });
        $('#your-photo-btn').click(function () {
            $('#your-photo').click();
        });
        $('#add-logo-btn').click(function () {
            $('#your-logo-btn').click();
        });

        $('.cal-ico').click(function () {
            var sel = $(this);
            // console.log('#'+sel.attr('for'));
            $('#' + sel.attr('for')).focus();
        });
        $("#your-photo").on("change", handleFileSelect);
        $("#your-logo-btn").on("change", handleLogoSelect);
        var selDiv = "";
        // var selDivM="";
        var storedFiles = [];
        var otherFiles = [];
        var st = 1;

        function handleFileSelect(e) {

            var files = e.target.files;
            var filesArr = Array.prototype.slice.call(files);
            var device = $(e.target).data("device");
            filesArr.forEach(function (f) {

//                if (!f.type.match("image.*")) {
//                    return;
//                }
                console.log(f.type.match("video.*"));
                if (f.type.match("video.*")) {
                   storedFiles.push(f);
                $(".file-input").show();
                var reader = new FileReader();
                reader.onload = function (e) {

                    var html = "<div class='file-preview-frame krajee-default  kv-preview-thumb'>" +
                        "<div class='kv-file-content'>" +
                        "<video src=\"" + e.target.result + "\" class='file-preview-image kv-preview-data' style='width:auto;height:auto;max-width:100%;max-height:100%;' id='image-"+f.lastModified+"-"+f.size+"'></video>" +
                        "<div class=\"overlay-file\"></div>" +
                        "<div class=\"button-file button-file-remove\" data-file='" + f.name + "' onclick=\"removeImg($(this))\" title ='remove'>" +
                        "<a href='#'> <img src='{{asset('')}}images/close.svg'> </a></div>" +
                        "</div></div>";
                    $("#selectedFiles").append(html);
                    // console.log(e.target.result );
                }
                reader.readAsDataURL(f);
                }
                if (f.type.match("image.*")) {
                storedFiles.push(f);
                $(".file-input").show();
                var reader = new FileReader();
                reader.onload = function (e) {

                    var html = "<div class='file-preview-frame krajee-default  kv-preview-thumb'>" +
                        "<div class='kv-file-content'>" +
                        " <img src=\"" + e.target.result + "\" class='file-preview-image kv-preview-data' style='width:auto;height:auto;max-width:100%;max-height:100%;' id='image-"+f.lastModified+"-"+f.size+"'>" +
                        "<div class=\"overlay-file\"></div>" +
                        "<div class=\"button-file button-file-remove\" data-file='" + f.name + "' onclick=\"removeImg($(this))\" title ='remove'>" +
                        "<a href='#'> <img src='{{asset('')}}images/close.svg'> </a></div>" +
                        "<div class=\"button-file button-file-edit edit-post-i \" onclick=\"editPost($(this))\" data-file='" + f.name + "' data-image='"+e.target.result+"' data-id='image-"+f.lastModified+"-"+f.size+"' title='edit'>" +
                            "<a href='#'> <img src='{{asset('')}}images/brush.svg'> </a></div>" +
                            "</div></div>";
                    $("#selectedFiles").append(html);
                    // console.log(e.target.result );
                }
                reader.readAsDataURL(f);
                }
            });

        }

        function handleLogoSelect(e) {

            var files = e.target.files;
            var filesArr = Array.prototype.slice.call(files);

            filesArr.forEach(function (f) {

                if (!f.type.match("image.*")) {
                    return;
                }
                $(".file-input").show();
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#your-logo').attr('src', e.target.result);
                    $('#draggable-icon').show();
                }
                reader.readAsDataURL(f);
            });
        }

        function removeImg(selector) {
            var file = selector.data("file");
            for (var i = 0; i < storedFiles.length; i++) {
                if (storedFiles[i].name === file) {
                    storedFiles.splice(i, 1);
                    break;
                }
            }
            selector.closest('.file-preview-frame').remove();
        }

        $("#submit, #postNow").on("click", handleForm);

        function handleForm(e) {
            e.preventDefault();
            var frombtn = $(this).attr('id');
            var data = new FormData();

            var poData = $('#post-form').serializeArray();

            for (var i = 0, len = storedFiles.length; i < len; i++) {
                data.append('your_photo[]', storedFiles[i]);
            }
            for (var i = 0, len = otherFiles.length; i < len; i++) {
                data.append('selected_photo[]', otherFiles[i]);
            }
            $('.overlay').show()
            for (var i = 0; i < poData.length; i++)
                data.append(poData[i].name, poData[i].value);

                data.append('from', frombtn);

            $.ajax({
                url: '{{url("post-data")}}',
                type: 'POST',
                // dataType: 'json',
                data: data,
                processData: false,
                cache: false,
                contentType: false,
                headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                success: function (response) {
                    if (response.status == 500){
                        swal("Select any channel ");
                    }else if (response.status) {
                        window.location.href = response.data.redirect_url;
                    }
                    $('.overlay').hide()
                }
            });
        }

        $('#save, #saveD').on('click', HandleCocept);

        function HandleCocept(e) {

            e.preventDefault();
            var data = new FormData();

            var poData = $('#post-form').serializeArray();

            for (var i = 0, len = storedFiles.length; i < len; i++) {
                data.append('your_photo[]', storedFiles[i]);
            }
            $('.overlay').show()

            for (var i = 0; i < poData.length; i++)
                data.append(poData[i].name, poData[i].value);

            $.ajax({
                url: '{{url("/concept/data")}}',
                type: 'POST',
                // dataType: 'json',
                data: data,
                processData: false,
                cache: false,
                contentType: false,
                headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                success: function (response) {

                    if (response.status) {
                        window.location.href = response.data.redirect_url;
                    }
                    $('.overlay').hide()
                }
            });

        }

        $('#BoncModalPhoto1').on('hidden.bs.modal', function () {
            var count = 0
            $(".image-checkbox").each(function () {

                if ($(this).first().hasClass('image-checkbox-checked')) {
                    count += 1;
                    var src = $(this).data('src');

                    var html = "<div class='file-preview-frame krajee-default  kv-preview-thumb'>" +
                        "<div class='kv-file-content'>" +
                        " <img src=\"" + src + "\" class='file-preview-image kv-preview-data' style='width:auto;height:auto;max-width:100%;max-height:100%;'>" +
                        "<div class=\"overlay-file\"></div>" +
                        "<div class=\"button-file button-file-remove\" data-file='' onclick=\"removeImg($(this))\" title ='remove'>" +
                        "<a href='#'> <img src='{{asset('')}}images/close.svg'> </a></div>" +
                        "<div class=\"button-file button-file-edit edit-post-i \" onclick=\"editPost($(this))\" data-image='"+e.target.result+"' title='edit'>" +
                            "<a href='#'> <img src='{{asset('')}}images/brush.svg'> </a></div>" +
                            "</div></div>";
                    $("#selectedFiles").append(html);
                }
            });
            if (count > 0) {
                $(".file-input").show();
            }
        })

        var dataId = '';
        var dataFile = '';
        var element = $("#icon-preview"); // global variable
        var getCanvas; // global variable
        function editPost(selector) {
            curSel = selector;
            var dataSrc = selector.data('image');
            dataId = selector.data('id');
            dataFile = selector.data('file');
            console.log(dataId);

            $('#icon-profile-pic').attr('src', dataSrc);
            $('#icon-new_uploaded_image').val(dataSrc);
            $('#icon-active_image').val(dataSrc);

            $('#main-image-m').attr('src', dataSrc);

            // window.iconcroppie = new Croppie(document.getElementById("icon-crop-area"), {
            //     "url": dataSrc,
            //
            // });
            // $('#postEditModal').modal('show');

            $('#postIconModal').modal('show');
            $('#icon-draggable-icon').addClass('border_style');
            $(".delete.remove-logo").css({display: "block"});

            $("#choose-sticker").on('click', function () {
                // $('#choose-sticker').hide();
                $('.bonc-sticker-btn-sec').hide();
                $('.modal-body').hide();
                $('#bonc-sticker-sec').fadeIn();
            });
            $("#back-to-image, #compay-icon-tab").on('click', function () {
                $('#bonc-sticker-sec').hide();
                $('.bonc-sticker-btn-sec').show();
                $('.modal-body').show();
            });


            $('.remove-logo').on('click',function(){
                $('#icon-draggable-icon').hide();
                $('#icon-selected-logo').attr('src','');
                $('.icon-select').removeClass('active');
            });

            $(".icon-select").on('click', function () {
                $('#icon-draggable-icon').show();
                var selectedsel = $(this);
                var selectedIcon = selectedsel.attr('src')
                $('#icon-selected-logo').attr('src', selectedIcon);
                $('.icon-select').removeClass('active');
                selectedsel.addClass('active');
            });

        }
        $("#addIcon").on('click', function () {
             $('#icon-draggable-icon').removeClass('border_style');
             $('.sticker').removeClass('border_style');
             $(".delete.rm_sticker").css({display: ""});
             $(".delete").css({display: "none"});
            html2canvas(element, {
                // width : 3000,
                // height :3000,
                // letterRendering: 1,
                // onrendered: function (canvas) {
                //     getCanvas = canvas;
                //     var imgageData = getCanvas.toDataURL("image/png");
                //     console.log(imgageData);
                //     $('#'+dataId).attr('src', imgageData);
                // }
            }).then(canvas => {
                var imageType = "image/png";
                imageData = canvas.toDataURL(imageType);
                var src = encodeURI(imageData);
                $('#'+dataId).attr('src', imageData);
                console.log('replaced', dataId);
                console.log(storedFiles);
                for (var i = 0; i < storedFiles.length; i++) {
                    if (storedFiles[i].name === dataFile) {

                        var file_ = dataURLtoFile(imageData, dataFile);
                        storedFiles.splice(i, 1,file_);
                        // storedFiles.push(file_);
                        break;
                    }
                }
            })

        });
        function remove_sticker(){
            console.log('called')
            $('.remove-sticker').on('click',function(){

                var id =$(this).data('id');
                $('#draggable-sticker'+id).remove();
            });
        }
        function selectSticker(){
            $(".bonc-st-list").on('click', function () {
                console.log('click');
                var selectedsel = $(this);
                var selectedIcon = selectedsel.data('src');
                var stickerdiv = '<div id="draggable-sticker'+st+'" class="icon-draggable-sec sticker border_style">'+
                    '<img src="'+selectedIcon+'" id="icon-selected-sticker'+st+'">' +
                    '<div class="delete rm_sticker"><a href="#" class="remove-sticker" data-id="'+st+'"><i class="fa fa-remove"></i></a></div>'+
                    '</div>';


                $('#icon-preview').append(stickerdiv);
                $('#bonc-sticker-sec').hide();
                $('.bonc-sticker-btn-sec').show();
                $('.modal-body').show();
                $('#draggable-sticker'+st).resizable({
                    aspectRatio: true,
                    handles: 'all',
                    containment:"#icon-preview",
                }).draggable({
                    cursor: "crosshair",
                    containment:"#icon-preview",
                });
                $(".delete.rm_sticker").css({display: "block"});
                remove_sticker();
                border_sticker(st);
                border_remove(st);
                st=st+1;
            });
        };
        $(".bonc-st-list").on('click', function () {
            console.log('click');
            var selectedsel = $(this);
            var selectedIcon = selectedsel.data('src');
            var stickerdiv = '<div id="draggable-sticker'+st+'" class="icon-draggable-sec sticker">'+
                '<img src="'+selectedIcon+'" id="icon-selected-sticker'+st+'">' +
                '<div class="delete rm_sticker"><a href="#" class="remove-sticker" data-id="'+st+'"><i class="fa fa-remove"></i></a></div>'+
                '</div>';


            $('#icon-preview').append(stickerdiv);
            $('#bonc-sticker-sec').hide();
            $('.bonc-sticker-btn-sec').show();
            $('.modal-body').show();

            $('#draggable-sticker'+st).resizable({
                aspectRatio: true,
                handles: 'all',
                containment:"#icon-preview",
            }).draggable({
                cursor: "crosshair",
                containment:"#icon-preview",
            });

            remove_sticker();
            border_sticker(st);
            border_remove(st);
            st=st+1;
        });
         function border_sticker(st){
            $('#draggable-sticker'+st+'').click(function(){

                $(this).addClass('border_style');
                $(".delete.rm_sticker").css({display: "block"});
                });
        }
        function border_remove(st){
        // $('#main-image-m').click(function(){
        //      $('#draggable-sticker'+st+'').removeClass('border_style');
        //      $(".delete.rm_sticker").css({display: ""});
        // });
        }

        $('form').on('focus', 'input[type=number]', function (e) {
            $(this).on('wheel.disableScroll', function (e) {
                e.preventDefault()
            })
        })
        $('form').on('blur', 'input[type=number]', function (e) {
            $(this).off('wheel.disableScroll')
        })
        $('#text_select').on('click', function () {
            $('#BoncModalText').modal('show');
        });

        $('#category').on('change', function () {
            var category = $(this).val();
            $.ajax({
                type: "POST",
                // contentType: "application/json; charset=utf-8",
                headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                url: "{{url('bonc/text/search')}}",
                data: {category: category},
                dataType: "json",
                success: function (data) {
                    var html = '';
                    $.each(data, function (i, item) {
                        html += '<li ><a class="list-group-item list-group-item-action bonc-text-list" onclick="pastetxt(\'' + item + ' \')"  href="javascript:void(0);" data-txt="' + item + '" >' + item + '</a></li>';
                    });
                    $("#list_text").html(html);
                },
                failure: function (response) {
                    alert("No Match");
                }
            });
        });

        $("#list_scroll_quote").scroll(function(e){
            var scrollTop = $(this).scrollTop();
            console.log($('#list_quote').height(),scrollTop);
            var scrollBot = scrollTop + $(this).height();
            if ((scrollTop >= $('#list_quote').height() - 900 && scrolled == 0)) {
                scrolled = 1;
                var count = parseInt($('#rowCountQuote').val()) + 1;
                var category = $('#quote_category').val();
                $.ajax({
                    url: "{{url('bonc/quote')}}",
                    type: 'POST',
                    headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                    dataType: 'json', // added data type
                    data: {category: category,count:count},
                    beforeSend: function() {
                        $('#greybagquote').removeClass('d-none');
                    },
                    success: function(data) {
                        var html = '';

                        $.each(data.data.data, function (i, value) {
                            // console.log(value);
                            html = '<div class="nopad text-center bonc-img">' +
                                '<label class="image-checkbox" data-src="'+value.metadata.thumbnail+'" data-name="'+value.uuid+'" ' +
                                'data-uuid="'+value.uuid+'">' +
                                '<img class="img-responsive img-box"' +
                                'src="'+value.metadata.thumbnail+'"/>' +
                                '<input type="checkbox" name="image[]" value=""/>' +
                                '<i class="fa fa-check hidden"></i>' +
                                '</label>'+
                                '</div>';
                            $('#list_quote').append(html);

                        });
                        scrolled=0;
                        addfrom();
                        $('#rowCountQuote').val(data.data.current_page);
                    }, complete: function() {
                        $('#greybagquote').addClass('d-none');
                    },
                });
            }
        });
        $('#quote_category').on("change", function () {
            $('#list_quote').html('');
            var category = $(this).val();
            var path="https://"+"{{env('AWS_BUCKET')}}"+".s3."+"{{env('AWS_DEFAULT_REGION')}}"+".amazonaws.com/";
            $.ajax({
                url: "{{url('bonc/quote')}}",
                type: 'POST',
                headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                dataType: 'json', // added data type
                data: {category: category},
                success: function(data) {
                    var html = '';
                    $('#list_quote').html(html);
                    $.each(data.data.data, function (i, value) {
                        // console.log(value);
                        $('#rowCountQuote').val(data.data.current_page);
                        html += '<div class="nopad text-center bonc-img">' +
                            '<label class="image-checkbox" data-src="'+value.metadata.thumbnail+'" data-name="'+value.uuid+'"' +
                            'data-uuid="'+value.uuid+'">' +
                            '<img class="img-responsive img-box"' +
                            'src="'+value.metadata.thumbnail+'"/>' +
                            '<input type="checkbox" name="image[]" value=""/>' +
                            '<i class="fa fa-check hidden"></i>' +
                            '</label>'+
                            '</div>';
                    });
                    $('#list_quote').html(html);
                    addfrom();

                }
            });
        });
        $(window).on('load',function(){
            var varified = $('#varified').val();
            if(varified == 0){
                $('#myModal').modal('show');
                $.ajax({
                    url: "{{url('user/verify')}}",
                    type: 'GET',
                    headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                    dataType: 'json', // added data type
                });

            }
            $.ajax({
                url: "{{url('bonc/quote/get')}}",
                type: 'GET',
                headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                dataType: 'json', // added data type
                beforeSend: function() {
                    $('#greybagquote').removeClass('d-none');
                },
                success: function(data) {
                    var html = '';
                    $('#list_quote').html(html);
                    $.each(data.data.data, function (i, value) {
                        // console.log(value);
                        html += '<div class="nopad text-center bonc-img">' +
                            '<label class="image-checkbox" data-src="'+value.metadata.thumbnail+'" data-name="'+value.uuid+'"' +
                            'data-uuid="'+value.uuid+'">' +
                            '<img class="img-responsive img-box"' +
                            'src="'+value.metadata.thumbnail+'"/>' +
                            '<input type="checkbox" name="image[]" value=""/>' +
                            '<i class="fa fa-check hidden"></i>' +
                            '</label>'+
                            '</div>';
                        $('#list_quote').html(html);
                    });
                    addfrom();


                }, complete: function() {
                    $('#greybagquote').addClass('d-none');
                    addfrom();
                },
            });

            $.ajax({
            url: "{{url('bonc/photo/get')}}",
            type: 'GET',
            headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
            dataType: 'json', // added data type
                beforeSend: function() {
                    $('#greybag').removeClass('d-none');
                },
            success: function(data) {
                var html = '';
                $('#list_photo').html(html);
                $.each(data.data.data, function (i, value) {
                    // console.log(value);
                    html = '<div class="nopad text-center bonc-img">' +
                        '<label class="image-checkbox" data-src="'+value.metadata.thumbnail+'" data-name="'+value.uuid+'" ' +
                        'data-uuid="'+value.uuid+'">' +
                        '<img class="img-responsive img-box"' +
                        'src="'+value.metadata.thumbnail+'"/>' +
                        '<input type="checkbox" name="image[]" value=""/>' +
                        '<i class="fa fa-check hidden"></i>' +
                        '</label>'+
                        '</div>';
                    $('#list_photo').append(html);
                });
                // $('#list_photo').html(html);
                addfrom();

            }, complete: function() {
                    $('#greybag').addClass('d-none');
                },
        });

            $.ajax({
                url: "{{url('bonc/sticker')}}",
                type: 'GET',
                headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                dataType: 'json', // added data type
                success: function(data) {
                    var html = '';
                    $.each(data.data, function (i, value) {
                        // console.log(value);
                        html ='<li class="bonc-st-list" data-src="{{asset('images/stickers')}}/'+value.name+'">' +
                            '<img src="{{asset('images/stickers')}}/'+value.name+'">' +
                            '</li>';
                        $('#sticker_bonc').append(html);
                    });

                    selectSticker();
                }
            });

        });
        $('#sticker_category').on('change',function(){
            var category=$(this).val();
            $.ajax({
                url: "{{url('bonc/sticker')}}",
                type: 'GET',
                headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                dataType: 'json', // added data type
                data:{category:category},
                success: function(data) {
                    var html = '';
                    $('#sticker_bonc').html(html);
                    $.each(data.data, function (i, value) {
                        // console.log(value);
                        html ='<li class="bonc-st-list" data-src="{{asset('images/stickers')}}/'+value.name+'">' +
                            '<img src="{{asset('images/stickers')}}/'+value.name+'">' +
                            '</li>';
                        $('#sticker_bonc').append(html);
                    });

                    selectSticker();
                }
            });

        });
        var scrolled = 0;
        $("#list_scroll").scroll(function(e){
            var scrollTop = $(this).scrollTop();
            // console.log($('#list_photo').height(),scrollTop);
            var scrollBot = scrollTop + $(this).height();
            if ((scrollTop >= $('#list_photo').height() - 900 && scrolled == 0)) {
                scrolled = 1;
                var count = parseInt($('#rowCount').val()) + 1;
                var category = $('#photo_category').val();
                $.ajax({
                    url: "{{url('bonc/photo')}}",
                    type: 'POST',
                    headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                    dataType: 'json', // added data type
                    data: {category: category,count:count},
                    beforeSend: function() {
                        $('#greybag').removeClass('d-none');
                    },
                    success: function(data) {
                        var html = '';

                        $.each(data.data.data, function (i, value) {
                            // console.log(value);
                            html = '<div class="nopad text-center bonc-img">' +
                                '<label class="image-checkbox" data-src="'+value.metadata.thumbnail+'" data-name="'+value.uuid+'" ' +
                                'data-uuid="'+value.uuid+'">' +
                                '<img class="img-responsive img-box"' +
                                'src="'+value.metadata.thumbnail+'"/>' +
                                '<input type="checkbox" name="image[]" value=""/>' +
                                '<i class="fa fa-check hidden"></i>' +
                                '</label>'+
                                '</div>';
                            $('#list_photo').append(html);

                        });
                        scrolled=0;
                        // $('#list_photo').html(html);
                        addfrom();
                        $('#rowCount').val(data.data.current_page);
                    }, complete: function() {
                        $('#greybag').addClass('d-none');
                    },
                });
            }
        });
        $('#photo_category').on("change", function () {
            $('#list_photo').html('');
            var category = $(this).val();
            var path="https://"+"{{env('AWS_BUCKET')}}"+".s3."+"{{env('AWS_DEFAULT_REGION')}}"+".amazonaws.com/";
            $.ajax({
                url: "{{url('bonc/photo')}}",
                type: 'POST',
                headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                dataType: 'json', // added data type
                data: {category: category},
                success: function(data) {
                    $('#rowCount').val(data.data.current_page);
                    var html = '';
                    $('#list_photo').html(html);
                    {{--html = '<div class="nopad text-center bonc-img">' +--}}
                    {{--    '<label class="image-checkbox" data-src="{{asset('images/img.png')}}" data-name="img.png" ' +--}}
                    {{--    'data-uuid="001">' +--}}
                    {{--    '<img class="img-responsive img-box"' +--}}
                    {{--    'src="{{asset('images/img.png')}}"/>' +--}}
                    {{--    '<input type="checkbox" name="image[]" value=""/>' +--}}
                    {{--    '<i class="fa fa-check hidden"></i>' +--}}
                    {{--    '</label>'+--}}
                    {{--    '</div>';--}}
                    {{--$('#list_photo').append(html);--}}
                    $.each(data.data.data, function (i, value) {
                        // console.log(value);
                        html = '<div class="nopad text-center bonc-img">' +
                            '<label class="image-checkbox" data-src="'+value.metadata.thumbnail+'" data-name="'+value.uuid+'" ' +
                            'data-uuid="'+value.uuid+'">' +
                            '<img class="img-responsive img-box"' +
                            'src="'+value.metadata.thumbnail+'"/>' +
                            '<input type="checkbox" name="image[]" value=""/>' +
                            '<i class="fa fa-check hidden"></i>' +
                            '</label>'+
                            '</div>';
                        $('#list_photo').append(html);
                    });
                    // $('#list_photo').html(html);
                    addfrom();

                }
            });
        });
        $("#search").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    // contentType: "application/json; charset=utf-8",
                    headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                    url: "{{url('bonc/text/search')}}",
                    data: {search: $("#search").val()},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        response(data);
                    },
                    failure: function (response) {
                        alert("No Match");
                    }
                });
            },
            appendTo: "#BoncModalText",
            select: function (e, i) {
                console.log(i);
                $("#text").val(i.item.value);
                $('#BoncModalText').modal('hide');
            },

        });

        function pastetxt(selector) {
            $('#text').val(selector);
            $('#BoncModalText').modal('hide');

        }

        $('#BoncModalText').on('hidden.bs.modal', function () {
            $(this).find(':input').val('');
        })

        $('.dateSelect').datetimepicker({
            format: 'd/m/Y',
            timepicker: false,
            minDate: new Date(),
            startDate: new Date(),
            todayButton: false,
            scrollMonth : false
        });
        $('.timeSelect').datetimepicker({
            format: 'H:i',
            datepicker: false,
            // scrollTime: false,
            step: 15,
        });
        // $("post-form").validate({
        //     rules: {
        //         time: {
        //             time: true,
        //             normalizer: function(currentValue) {
        //                 var isSimpleNumber = /^([01]\d|2[0-3]|[0-9])$/.test(currentValue);
        //                 // Append ":00" in case the user entered a simple number between 00 and 23
        //                 return isSimpleNumber ? currentValue + ":00" : currentValue;
        //             }
        //         }
        //     }
        // })
        // image gallery
        // init the state from the input
        $(".image-checkbox").each(function () {
            if ($(this).find('input[type="checkbox"]').first().attr("checked")) {
                $(this).addClass('image-checkbox-checked');
            } else {

                $(this).removeClass('image-checkbox-checked');
            }


        });

        // sync the state to the input
        var count = 0;
        function addfrom() {
            console.log('called');

            $(".image-checkbox").unbind("click");

            $(".image-checkbox").click( function (e) {
                console.log('clicked');

                $(this).toggleClass('image-checkbox-checked');
                var $checkbox = $(this).find('input[type="checkbox"]');
                $checkbox.prop("checked", !$checkbox.prop("checked"))

                e.preventDefault();

                var id = $(this).data('uuid');
                if ($(this).first().hasClass('image-checkbox-checked')) {
                    count += 1;
                    var src = $(this).data('src');
                    var name = $(this).data('name');
                    var file_ = dataURLtoFile(src, name);
                    // var file_ = new File(src, name);

                    storedFiles.push(file_);

                    var html = "<div class='file-preview-frame krajee-default  kv-preview-thumb' id='img-" + id + "'>" +
                        "<div class='kv-file-content'>" +
                        " <img src=\"" + src + "\" class='file-preview-image kv-preview-data' style='width:auto;height:auto;max-width:100%;max-height:100%;' id='image-" + id + "'>" +
                        "<div class=\"overlay-file\"></div>" +
                        "<div class=\"button-file button-file-remove\" data-file='" + id + "' onclick=\"removeImg($(this))\" title ='remove'>" +
                        "<a href='#'> <img src='{{asset('')}}images/close.svg'> </a></div>" +
                        "<div class=\"button-file button-file-edit edit-post-i \" onclick=\"editPost($(this))\" data-file='" + id + "'  data-image='" + src + "' data-id='image-" + id +"' title='edit'>" +
                        "<a href='#'> <img src='{{asset('')}}images/brush.svg'> </a></div>" +
                        "</div></div>";
                    $("#selectedFiles").append(html);
                } else {
                    count -= 1;
                    $('#img-' + id).remove();
                }

                if (count > 0) {
                    $(".file-input").show();
                } else {
                    $(".file-input").hide();
                }
            });
        }

        function dataURLtoFile(dataurl, filename) {

            var arr = dataurl.split(','),
                mime = arr[0].match(/:(.*?);/)[1],
                bstr = atob(arr[1]),
                n = bstr.length,
                u8arr = new Uint8Array(n);

            while (n--) {
                u8arr[n] = bstr.charCodeAt(n);
            }

            return new File([u8arr], filename, {type: mime});

        }

        {{--   CKEDITOR.replace( 'editor1' );--}}
        function openCnt(evt, cityName) {
            // Declare all variables
            var i, tabcontent, tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("post-tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = document.getElementsByClassName("post-tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            // Show the current tab, and add an "active" class to the link that opened the tab
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

    </script>
@endpush
