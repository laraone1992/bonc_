$(window).on("load",function(){
    $('b[role="presentation"]').hide();
    $('.select2-selection__arrow').append('<i class="fa fa-angle-down"></i>');

    $.ajax({
        type: "GET",
        // contentType: "application/json; charset=utf-8",
        headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
        url: "instagram/notification",
        dataType: "json",
        success: function (data) {
            var html='';
            $.each(data.data.notification, function (i,item) {
                html+='<li class="d-flex" id="list" onclick="viewnotification('+item.id+')">' +
                    '<span class="notification-icon d-flex">' +
                    '<img src="images/ic/insta40.png">' +
                    '</span>' +
                    '<div class="txt-nt ">' +
                    '<p class="nt-cnt">You have asked me to remind you that you want to post something on instagram '+item.date+' at '+item.time+'</p>' +
                    '</div>' +
                    '</li>'+
                    '<hr>';

            });
            $("#notification").html(html);
            if(data.data.count > 0) {
                $('#notificationCount').html(data.data.count);
                $('#notificationCount').removeClass('d-none');
            }
            $("#notifications").html(html);
            // $('#notificationCount').html(data.data.count);
        },
        failure: function (response) {
            alert("No Match");
        }
    });
});
$('#download-img').click(function () {
    window.location.href = $('.imgsrcdownload').attr('href');
})


function viewnotification(data) {
    $('#notificationList').modal('hide');
    $.ajax({
        type: "GET",
        // contentType: "application/json; charset=utf-8",
        headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
        url: "notification/view",
        data: {notification_id:data},
        dataType: "json",
        success: function (data) {
            var html='';
            $.each(data.data, function (i,item) {

              html+='<div class="col-md-12 notification-image-block">'+
                  '<img src="storage/images/post/'+item.image+'" class="notification-image">'+
                  '</div>'+
                  '<div class="col-md-12">' +
                  '<p id="p1" class="notification-content">'+item.content+'</p>'+
                  '<input type="text" value="'+item.content+'" id="copysource" style="display: none">'+
                  '</div>'+
                   '<div class="col-md-12">' +
                    // '<img src="images/Copy_Filled-512@2x.png" class="clickboard"> <a href="#" class="clipboardtext" onclick="copyToClipboard()">Copy text to clipboard</a href="#">'
                  '</div>';
                $("#download-img").attr('href', 'storage/images/post/'+item.image);
            });
            $("#viewNotification").html(html);


        },
        failure: function (response) {
            alert("No Match");
        }
    });
    $('#notificationview').modal('show');
}

function copyToClipboard(imgPath) {
// console.log(imgPath);
    // window.location.href = imgPath;
    var input = document.getElementById ("copysource");
    var textToClipboard = input.value;

    var forExecElement = CreateElementForExecCommand (textToClipboard);

    /* Select the contents of the element
        (the execCommand for 'copy' method works on the selection) */
    SelectContent (forExecElement);

    var supported = true;

    // UniversalXPConnect privilege is required for clipboard access in Firefox
    try {
        if (window.netscape && netscape.security) {
            netscape.security.PrivilegeManager.enablePrivilege ("UniversalXPConnect");
        }

        // Copy the selected content to the clipboard
        // Works in Firefox and in Safari before version 5
        success = document.execCommand ("copy", false, null);
        // window.location.href = imgPath;
    }
    catch (e) {
        success = false;
    }

    // remove the temporary element
    document.body.removeChild (forExecElement);


}
function CreateElementForExecCommand (textToClipboard) {
    var forExecElement = document.createElement ("div");
    // place outside the visible area
    forExecElement.style.position = "absolute";
    forExecElement.style.left = "-10000px";
    forExecElement.style.top = "-10000px";
    // write the necessary text into the element and append to the document
    forExecElement.textContent = textToClipboard;
    document.body.appendChild (forExecElement);
    // the contentEditable mode is necessary for the  execCommand method in Firefox
    forExecElement.contentEditable = true;

    return forExecElement;
}

function SelectContent (element) {
    // first create a range
    var rangeToSelect = document.createRange ();
    rangeToSelect.selectNodeContents (element);

    // select the contents
    var selection = window.getSelection ();
    selection.removeAllRanges ();
    selection.addRange (rangeToSelect);
}



