<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoncPhoto extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bonc_photo';
    protected $primaryKey = 'id';
}
