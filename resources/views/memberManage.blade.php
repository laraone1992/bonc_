@extends('layouts.layout')
@section('title','Brands')

@section('content')
    <!-- Content Start -->
    <div id="content-setting" class="d-flex">

        <!-- =========================================
        Content Section
        ========================================== -->
        <section class="content-section-brand flex-fill">
            <div class="brand-header col-md-9">MEMBER MANAGEMENT</div><br>
<div class="container">
  <div class="row">
    <div class="col-md-4 mb-3">
        <ul class="nav nav-pill flex-column" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><img src="{{asset('')}}images/ic/company.png" class="brandlogo" alt=""> Desilo</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><img src="{{asset('')}}images/ic/company.png" class="brandlogo" alt=""> Keri</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"> <img src="{{asset('')}}images/ic/company.png" class="brandlogo" alt=""> Brand Name 3</a>
  </li>
</ul>
    </div>
    <!-- /.col-md-4 -->
        <div class="col-md-8">
      <div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
      <div class="row setting-channel-list">
          
              <div class="col-md-10">
                  <img src="{{asset('')}}images/ic/company.png" class="memberlogo" alt="">
              </div>
              <div class="col-md-2">
                  <input type="submit" class="btn btn-secondary text-left" value="+ Add Member" id="submit">
              </div>
              
         
          
      </div>
      <div>
          <span class="m-1 pl-2 pt-2 master">Master</span>
          <div>
              <ul class="list-unstyled d-flex align-items-center m-1 pl-3 pt-2 setting-channel-list">
                <li><img src="{{asset('')}}images/ic/user-image.png" width="78%" alt=""></li>
                <li>John Doe</li>
              </ul>
          </div>
          <div>
              <ul class="list-unstyled d-flex align-items-center m-1 pl-3 pt-2 setting-channel-list">
                <li><img src="{{asset('')}}images/ic/user-image.png" width="78%" alt=""></li>
                <li>John Doe</li>
              </ul>
          </div>
        
      </div>
      <div>
          <span class="m-1 pl-2 pt-2 admin">Admin</span>
          <div>
              <ul class="list-unstyled d-flex align-items-center m-1 pl-3 pt-2 setting-channel-list">
          <li><img src="{{asset('')}}images/ic/user-image.png" width="78%" alt=""></li>
          <li>Jane Doe</li>
            <li class="menu-icon-member"><span class="menu-icon pull-right"></span></li>
                </ul>
          </div>
          <div>
              <ul class="list-unstyled d-flex align-items-center m-1 pl-3 pt-2 setting-channel-list">
              <li><img src="{{asset('')}}images/ic/user-image.png" width="78%" alt=""></li>
            <li>Lisa M.</li>
            <li class="menu-icon-member"><span class="menu-icon pull-right"></span></li>
            </ul>
          </div>
        
      </div>
    <div>
          <span class="m-1 pl-2 pt-2 user">User</span>
          <div>
             <ul class="list-unstyled d-flex align-items-center m-1 pl-3 pt-2 setting-channel-list">
          <li><img src="{{asset('')}}images/ic/user-image.png" width="78%" alt=""></li>
          <li>Andrew Lee</li>
           </ul>
          </div>
      </div>
      
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="row setting-channel-list">
          
              <div class="col-md-10">
                  <img src="{{asset('')}}images/ic/company.png" class="memberlogo" alt="">
              </div>
              <div class="col-md-2">
                  <input type="submit" class="btn btn-secondary text-left" value="+ Add Member" id="submit">
              </div>
              
         
          
      </div>
      <div>
          <span class="m-1 pl-2 pt-2 master">Master</span>
          <div>
              <ul class="list-unstyled d-flex align-items-center m-1 pl-3 pt-2 setting-channel-list">
                <li><img src="{{asset('')}}images/ic/user-image.png" width="78%" alt=""></li>
                <li>John Doe</li>
              </ul>
          </div>
          <div>
              <ul class="list-unstyled d-flex align-items-center m-1 pl-3 pt-2 setting-channel-list">
                <li><img src="{{asset('')}}images/ic/user-image.png" width="78%" alt=""></li>
                <li>John Doe</li>
              </ul>
          </div>
        
      </div>
      <div>
          <span class="m-1 pl-2 pt-2 admin">Admin</span>
          <div>
              <ul class="list-unstyled d-flex align-items-center m-1 pl-3 pt-2 setting-channel-list">
          <li><img src="{{asset('')}}images/ic/user-image.png" width="78%" alt=""></li>
          <li>Jane Doe</li>
                </ul>
          </div>
          <div>
              <ul class="list-unstyled d-flex align-items-center m-1 pl-3 pt-2 setting-channel-list">
              <li><img src="{{asset('')}}images/ic/user-image.png" width="78%" alt=""></li>
            <li>Lisa M.</li>
            </ul>
          </div>
        
      </div>
    <div>
          <span class="m-1 pl-2 pt-2 user">User</span>
          <div>
             <ul class="list-unstyled d-flex align-items-center m-1 pl-3 pt-2 setting-channel-list">
          <li><img src="{{asset('')}}images/ic/user-image.png" width="78%" alt=""></li>
          <li>Andrew Lee</li>
           </ul>
          </div>
      </div>
      
  </div>

  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
      <div class="row setting-channel-list">
          
              <div class="col-md-10">
                  <img src="{{asset('')}}images/ic/company.png" class="memberlogo" alt="">
              </div>
              <div class="col-md-2">
                  <input type="submit" class="btn btn-secondary text-left" value="+ Add Member" id="submit">
              </div>
              
         
          
      </div>
      <div>
          <span class="m-1 pl-2 pt-2 master">Master</span>
          <div>
              <ul class="list-unstyled d-flex align-items-center m-1 pl-3 pt-2 setting-channel-list">
                <li><img src="{{asset('')}}images/ic/user-image.png" width="78%" alt=""></li>
                <li>John Doe</li>
              </ul>
          </div>
          <div>
              <ul class="list-unstyled d-flex align-items-center m-1 pl-3 pt-2 setting-channel-list">
                <li><img src="{{asset('')}}images/ic/user-image.png" width="78%" alt=""></li>
                <li>John Doe</li>
              </ul>
          </div>
        
      </div>
      <div>
          <span class="m-1 pl-2 pt-2 admin">Admin</span>
          <div>
              <ul class="list-unstyled d-flex align-items-center m-1 pl-3 pt-2 setting-channel-list">
          <li><img src="{{asset('')}}images/ic/user-image.png" width="78%" alt=""></li>
          <li>Jane Doe</li>
                </ul>
          </div>
          <div>
              <ul class="list-unstyled d-flex align-items-center m-1 pl-3 pt-2 setting-channel-list">
              <li><img src="{{asset('')}}images/ic/user-image.png" width="78%" alt=""></li>
            <li>Lisa M.</li>
            </ul>
          </div>
        
      </div>
    <div>
          <span class="m-1 pl-2 pt-2 user">User</span>
          <div>
             <ul class="list-unstyled d-flex align-items-center m-1 pl-3 pt-2 setting-channel-list">
          <li><img src="{{asset('')}}images/ic/user-image.png" width="78%" alt=""></li>
          <li>Andrew Lee</li>
           </ul>
          </div>
      </div>
      
  </div>
  </div>
  </div>
</div>
    </div>
    <!-- /.col-md-8 -->
  </div>
  
  
  
</div>
<!-- /.container -->
                  </section>
            </div>

      

@endsection

@push('styles')
<style>
    input[type="file"] {
        display: none;
    }
    .delete {
        background-color: Transparent;
        background-repeat:no-repeat;
        border: none;
        cursor:pointer;
        overflow: hidden;
        position: absolute;
        top: -21px;
        right: 0;
        z-index: 100;
        color: black;
    }
    .profile-img-container {
        position: relative;
        padding-right: 8px;
    }
    .profile-img-container img{
        height: 100%;
    }

    .profile-img-container i {
        top: 45%;
        left: 45%;
        transform: translate(-23%, -27%);
        display: none;
    }

    .profile-img-container:hover img {
        opacity: 0.5;
    }

    .profile-img-container:hover i {
        display: block;
        z-index: 500;
    }
    .gallery img{

    }
    .company-logo-sec{ padding: 0; display: table}
    .company-logo-sec li{list-style: none; display: table-cell; width: 50px; height: 40px;padding-right:10px; position: relative;}
    .company-logo-sec li img{ width: 100%; height: 100%;}
    .company-icon{}

    .vl {
        border-left: 1px solid lightgrey;
        height: 46px;
        position: absolute;
        left: 18%;
        margin-left: -29px;
        top: 8px;
    }
    .user-profile{
        border-radius: 20px;
        width: 80%;
    }
    .social-summary-linked {
        padding-right: 15px;
        border-right: 1px solid #ccd0d9;
    }
    .list-unstyled.social-summary-linked li img {
        width: 35px;
    }
    .profile-name li a{
        /*margin-left: -30px;*/
    }
    .input-group-brand input {
    padding: 15px;
    border-radius: 8px;
}
.content-section-brand {
    padding: 58px 15%;
}
.content-section-brand .tab-pane {
    background: #fff;
    width: 93%;
    padding: 4px 15px;
    border-radius: 10px;
}
.content-section-brand .container{
    background-color: #fff;
    border-radius: 7px;
}
.input-group-brand label {
    margin: 15px 6px;
    font-size: 19px;
    color: #000;
}
.input-group-brand label.add-logo-btn {
    margin: 0 6px;
    border-radius: 7px;
    color: #ced4da;
}
.input-group-brand label.btn-social {
    border: 1px #d5d9de;
    border-radius: 30px;
    /*color: #aba0a0;*/
    /*background-color: #d5d9de;*/
    padding: 5px 27px 4px 57px;
    margin-top: 0px;
    margin: 0 9px;
    font-size: inherit;
}
.input-group-brand label.btn-social-facebook{
    background-color: #3b5998;
    color: #fff;
}
.input-group-brand label.btn-social-twitter{
   background-color: #d5d9de;
   color: #aba0a0;
}
.input-group-brand label.btn-social-linkdln{
    background-color: #d5d9de;
    color: #aba0a0;
}
.input-group-brand label.btn-social-instagram{
    background-color: #d5d9de;
    color: #aba0a0;
}
.input-group-brand .btn-secondary {
    border-radius: 30px;
    padding: 10px 20px;
}
.btn-social img{
    width: 28px;
    margin-left: -52px;
    margin-right: inherit;
    border: 1px solid;
    color: beige;
    border-radius: inherit;
}
.brand-header{
    color: black;
    font-weight: 500;
}
.menu-icon:after {
  content: '\2807';
  font-size: 22px;
  color:#a09c9c;
 
  }
  .menu-icon-member{
       padding-left: 23rem;
  }
  .brandlogo{
    width: 36px;
    border-radius: 7px;
    margin-left: 20px;
  }
  .memberlogo{
    width: 48px;
    border-radius: 7px;
  }
  .social-icon{
      padding-left: 19rem;
  }
  #viewbrand{
    overflow-x:hidden;
    overflow-y:visible;
    height:500px;
    overflow: scroll;
    overflow: auto;
  }
  .nav-pill {
    background: #fff;
    width: 93%;
    padding: 17px 0px;
    border-radius: 10px;
}
  .nav-pill .nav-link.active, .nav-pills .show > .nav-link {
    /* color: #fff; */
    /* background-color: #007bff; */
    border-left: 2px solid;
    margin-left: -15px;
}
  .nav-pill .nav-link, .nav-pills .show > .nav-link {
    /* color: #fff; */
    /* background-color: #007bff; */
    margin-left: -15px;
}
.tab-content{
    border-left: 2px solid #f6f6f6;
}
.setting-channel-list{
    border-bottom: 2px solid #f6f6f6;
}
.member-user{
    border-top: 2px solid #f6f6f6;
}
.master{
    color: #e86d15;
}
.user{
    color: #62d262;
}
.admin{
    color: darkorange;
}
.nav-pill .nav-link, .nav-pills .show > .nav-link{
    opacity: 0.6;
}
.nav-pill .nav-link.active, .nav-pills .show > .nav-link{
    opacity: 1;
}
</style>
@endpush
@push('scripts')
    <script src="{{asset('')}}js/jscolor.js"> </script><!-- pignose.calendar.full.min -->
    <script type="text/javascript">
        
</script>

@endpush
