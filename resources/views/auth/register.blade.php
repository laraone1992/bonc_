<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Bonc | Register') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('images/favicon.png')}}" type="image/x-icon">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style>
        .register {
            height: calc(100vh);
            overflow-x: hidden;
            overflow-y: auto;
        }
        .logo{
            background: url("../images/logo.png") no-repeat;
        }
        .register-left{
            background: url("../images/login_image.png") no-repeat;
            height: 100vh ;

        }
        .register-left .input-btn{
            border: none;
            border-radius: 1.5rem;
            padding: 4% 15%;
            width: 60%;
            background: #f8f9fa;
            font-weight: bold;
            color: #383d41;
            margin-top: 30%;
            margin-bottom: 3%;
            cursor: pointer;
        }
        .register-right{

        }
        .register-left img{
            margin: 37px 42px;
        }
        @-webkit-keyframes mover {
            0% { transform: translateY(0); }
            100% { transform: translateY(-20px); }
        }
        @keyframes mover {
            0% { transform: translateY(0); }
            100% { transform: translateY(-20px); }
        }
        .register-left p{
            font-weight: lighter;
            padding: 12%;
            margin-top: -9%;
        }
        .register .register-form{
            margin-top: 45px;
        }
        .btnRegister{
            margin-top: 2%;
            border: none;
            border-radius: 1.5rem;
            padding: 10px 40px;
            background: #000;
            color: #fff;
            font-weight: 600;
            cursor: pointer;
        }
            .register .nav-tabs{
                margin-top: 3%;
                border: none;
                background: #0062cc;
                border-radius: 1.5rem;
                width: 28%;
                float: right;
            }
            .register .nav-tabs .nav-link{
                padding: 2%;
                height: 34px;
                font-weight: 600;
                color: #fff;
                border-top-right-radius: 1.5rem;
                border-bottom-right-radius: 1.5rem;
            }
            .register .nav-tabs .nav-link:hover{
                border: none;
            }
            .register .nav-tabs .nav-link.active{
                width: 100px;
                color: #0062cc;
                border: 2px solid #0062cc;
                border-top-left-radius: 1.5rem;
                border-bottom-left-radius: 1.5rem;
            }
            .register-heading{
                text-align: center;
                margin-top: 8%;
                margin-bottom: -15%;
                color: #495057;
            }
            .register-header{
                font-size: 14px;
                margin: 20px 0;
                padding: 28px 10px;
                text-align: right;
            }
            .button-primary{
                color: #fff !important;
                background: #000;
                padding: 10px 27px;
                border-radius: 20px;
                margin-left: 10px;
            }
            .form-sec{
                padding: 0% 15%;
            }
            .form-sec .form-header p{
                font-size: 33px;
                font-weight: 300;
                line-height: 22px;
                margin-bottom: 13px;
            }
            .form-sec .form-header span{
                font-size: 16px;
                font-weight: 100;
                line-height: 22px
            }
            .form-group label{
                margin-bottom: 2px;
            }
            .form-control{
                padding: 1.87rem 1.4rem;
                border-radius: 0.55rem;
            }
            .form-group {
                margin-bottom: 2.1rem;
            }
            .logo-mobile-reg{ display: none; left: 30px; top: 15px; position: absolute; filter: invert(1);}

        @media (max-width: 767px) {
            .register{
                height: auto;
                overflow-x: hidden;
                overflow-y: auto;
            }
            .form-sec .form-header p {
                font-size: 24px;
                font-weight: 300;
                line-height: 17px;
                margin-bottom: 0px;
            }
            .form-sec .form-header span {
                font-size: 13px;
            }
            .register-left{
                background: none;
                display: none;

            }
            .register-right {
                /*margin-top: -120px;*/
            }
            .register-header span{
                display: none;
            }
            .logo-mobile-reg{ display: block;}
            .register-form-sec{padding: 0 10px;}
        }
        @media (max-width: 767px) {
            .form-sec .form-header p {
                font-size: 19px;
                line-height: 25px;
            }
        }
        .cust-select{
            padding: 10px;
            height: 63px !important;
        }
</style>
</head>
<body>
<div id="app">
    <main class="">
        <div class=" register">
            <div class="row">
                <div class="col-md-4 register-left">
{{--                    <div class="logo">--}}

{{--                    </div>--}}
                    <img src="{{asset('images/logo.png')}}" alt=""/>
                </div>
                <div class="col-md-8 register-right">
                    <div class="tab-content col-md-12" id="myTabContent">
                        <div class=" register-header">
                            <img src="{{asset('images/logo.png')}}" alt="" class="logo-mobile-reg" />
                            <span>Already have an account?</span>
                            <a href="{{url('login')}}" class="button button-primary">SIGN IN</a>
                        </div>
                        <div class="tab-pane fade show active form-sec register-form-sec" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="form-header">
                                <p>Let's boost your sales!</p>
                                <span> Start your 7-day FREE trial. No credit card needed!</span>
                            </div>
                            @if(Session::has('error'))
                                <p class="alert alert-danger">{{ Session::get('error') }}</p>
                            @endif
                            <form name="register" action="{{url('register')}}" method="post">
                                @csrf
                                <div class="register-form">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label>FIRST NAME</label>
                                                <input type="text" class="form-control" name="firstName" placeholder="First Name *" value="{{old('firstName')}}" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label>LAST NAME</label>
                                                <input type="text" class="form-control" placeholder="Last Name *" name="lastName" value="{{old('lastName')}}" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label>EMAIL</label>
                                                <input type="email" class="form-control" placeholder="Your Email *" value="{{old('email')}}" name="email" />
                                            </div>
                                        </div>
{{--                                        <div class="col-md-6 col-sm-12">--}}
{{--                                            <div class="form-group">--}}
{{--                                                <label>USER NAME</label>--}}
{{--                                                <input type="text" class="form-control" placeholder="User Name *" value="" name="username" />--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-md-6 col-sm-12 ">
                                            <div class="form-group">
                                                <label>PREFERRED LANGUAGE</label>
                                                <select class="form-control cust-select" name="language">
                                                    <option value="en" selected="selected">English</option>
                                                    <option value="nl">Dutch</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 ">
                                            <div class="form-group">
                                                <label>PASSWORD</label>
                                                <input type="password" class="form-control" placeholder="Password *" value="" name="password" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label>CONFIRM PASSWORD</label>
                                                <input type="password" class="form-control"  placeholder="Confirm Password *" value="" name="password_confirmation" />
                                            </div>
                                        </div>
                                    </div>
{{--                                    <div class="row">--}}
{{--                                        <div class="col-md-6 col-sm-12 ">--}}
{{--                                            <div class="form-group">--}}
{{--                                                <label>PREFERRED LANGUAGE</label>--}}
{{--                                                <select class="form-control cust-select" name="language">--}}
{{--                                                    <option value="en" selected="selected">English</option>--}}
{{--                                                    <option value="nl">Dutch</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-md-6 col-sm-12">--}}
{{--                                            <div class="form-group">--}}
{{--                                                <label>SELECT SUBCRIPTIONS</label>--}}
{{--                                                <select class="form-control cust-select" name="subcription">--}}
{{--                                                    <option value="0">Free</option>--}}

{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                    <div class="col-md-12 text-center">
                                        <input type="submit" class="btnRegister"  value="SIGN UP"/>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </main>
</div>
<script>
    $(document).ready(function(){
        $(".alert").slideDown(300).delay(5000).slideUp(300);
    });
</script>
</body>
</html>


