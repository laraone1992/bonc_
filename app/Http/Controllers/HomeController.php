<?php

namespace App\Http\Controllers;


use App\Base;
use App\BoncPhoto;
use App\BoncSticker;
use App\BoncText;
use App\BoncTextDutch;
use App\Notification;
use App\User;
use App\UserImages;
use Carbon\Carbon;
use http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;


class HomeController extends Controller
{
    protected $base;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Base $base)
    {
        // $this->middleware('auth');
        $this->base = $base;
//            $ip = file_get_contents("http://ipecho.net/plain");
            $ip = $_SERVER['REMOTE_ADDR'];
            $url = 'http://ip-api.com/json/'.$ip;
            $tz = file_get_contents($url);
            $data=\GuzzleHttp\json_decode($tz);
            if($data->status == 'fail'){
             $timezone='Europe/Amsterdam';
            }else{
             $timezone = json_decode($tz,true)['timezone'];
            }

            date_default_timezone_set($timezone);
            $value = config(['app.timezone'=>$timezone]);

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request, $date=null)
    {

//        dd(Cookie::queue(Cookie::forget('laravel_session')), Cookie::get());
        if (Auth::user()){
            $userDetails = Auth::user();
            $userCheck=User::where('email',$userDetails->email)->first();
            if($userCheck == null){
                $userInsert=new User();
                $userInsert->first_name =$userDetails->given_name;
                $userInsert->last_name =$userDetails->family_name;
                $userInsert->email =$userDetails->email;
                $userInsert->username =$userDetails->preferred_username;
                $userInsert->language =$userDetails->locale;
                $userInsert->password ='123';
                $userInsert->save();
            }
            $userlng=User::select('language')->where('email',$userDetails->email)->first();
            if(!Session::has('locale')){
                Session::put('locale',($userlng->language == 'en') ? 'en' :'du');
//                return redirect()->route('locale', [($userlng->language == 'en')?'en' :'du']);
            }
            $channelList=[];
            if(app()->getLocale() =='en')
                $textCategory=BoncText::select(DB::raw('distinct category'))->get()->toArray();
            else
                $textCategory=BoncTextDutch::select(DB::raw('distinct category'))->get()->toArray();

            channels:
            $channels = $this->base->request('get','channels');

            $boncPhoto=BoncPhoto::select(DB::raw('distinct category'))->get()->toArray();
            $boncQuote=BoncPhoto::select(DB::raw('distinct category'))->get()->toArray();
            $boncSticker=BoncSticker::select(DB::raw('distinct category'))->get()->toArray();
            foreach ($channels['data'] as $key => $value) {
                $channelList[]= $value['type'];
            }
            if(in_array('facebook_profile', $channelList) && !in_array('facebook_page', $channelList)){

                foreach ($channels['data'] as $key => $value) {
                    if($value['type'] == 'facebook_profile'){
                        $pages = $this->base->request('get','/facebook/'.$value['uuid'].'/pages');
                        if ($pages && count($pages) >= 1) {
                            foreach ($pages as $keyPage => $page) {
                                $pageIds[] = $page['id'];
                            }
                            $params = json_encode(['pages' => $pageIds]);
                            $pageRegister = $this->base->postRequest('/facebook/'.$value['uuid'].'/pages', $params);
                            // dd($pageRegister);
                            goto channels;
                        }
                    }
                }
            }

//            $boncPhoto = $this->base->request('get','media?public=1&page=1&limit=50');
//            $boncQuote = $this->base->request('get','media?public=1&tag[]=social&page=2&limit=50');
            $companyIcon = [];
            $companyIcon_ = UserImages::where('u_uuid', Auth::user()->org_uuid)->get();
            if($companyIcon_ != null){
                $companyIcon = $companyIcon_->toArray();
            }

            $uDetails = User::where('email', Auth::user()->email)->first();
            $varified = 1;
            if ($uDetails!= null){
                $varified = ($uDetails->email_verified_at != null)?$uDetails->email_verified_at:0;
            }
            $subcription=$this->base->request('get','orderitems');
            $subcriptionType=isset($subcription['data'][0]['product']['name']) ? $subcription['data'][0]['product']['name'] :'FREE';

//            $channels['data'] = [];
//            $boncPhoto['data'] = [];
//            $boncQuote['data'] = [];
//            $companyIcon =[];

            return view('post', compact('channels', 'date','textCategory', 'boncPhoto', 'boncQuote','boncSticker', 'companyIcon', 'varified','subcriptionType','userDetails'));
        }else{
            return redirect('login');
        }
    }
    public function userVerify(Request $request){
        $uDetails = User::where('email', Auth::user()->email)->first();
        $uDetails->email_verified_at = 1;
        $uDetails->save();

    }
    public function boncPhotos(Request $request){
//        dd($request->all());
//        $boncPhoto=Storage::disk('s3')->allFiles('photo/'.$request->get('category'));
        if($request->has('count'))
        {
            if ($request->get('category') != null)
                $boncPhoto = $this->base->request('get','media?public=1&tags[]='.$request->get('category').'&page='.$request->get('count').'&limit=50');
            else
                $boncPhoto = $this->base->request('get','media?public=1&tags[not]=text&page='.$request->get('count').'&limit=50');
        }
        else {
            if ($request->get('category') != null)
                $boncPhoto = $this->base->request('get', 'media?public=1&tags[]=' . $request->get('category') . '&page=1&limit=50');
            else
                $boncPhoto = $this->base->request('get', 'media?public=1&tags[not]=text&page=1&limit=50');
        }
        return response()->json(['status'=>200,'data'=>$boncPhoto]);

    }
    public function boncQuotes(Request $request){
        if($request->has('count'))
        {
            $boncQuote = $this->base->request('get','media?public=1&tag[]='.$request->get('category').'&page='.$request->get('count').'&limit=50');
        }else{
            $boncQuote = $this->base->request('get','media?public=1&tag[]='.$request->get('category').'&page=1&limit=50');
        }

//        $boncQuote=Storage::disk('s3')->allFiles('quote/'.$request->get('category'));
        return response()->json(['status'=>200,'data'=>$boncQuote]);

    }
    public function boncQuoteGet(Request $request){
        $boncQuote = $this->base->request('get','media?public=1&tags[]=text&page=1&limit=50');

        return response()->json(['status'=>200,'data'=>$boncQuote]);
    }
    public function boncPhotoGet(Request $request){
        $boncPhoto = $this->base->request('get','media?public=1&tags[not]=text&page=1&limit=50');

        return response()->json(['status'=>200,'data'=>$boncPhoto]);
    }
    public function boncSticker(Request $request){
        if($request->has('category')){
            $sticker=BoncSticker::select('name')->where('category',$request->get('category'))->get()->toArray();
        }
        else{
            $sticker=BoncSticker::select('name')->get()->toArray();
        }
        return response()->json(['status'=>200,'data'=>$sticker]);
    }
    public function demo()
    {


        // Load source and mask
        $source = imagecreatefrompng( public_path().'/images/demo/1578584422.png' );
        $mask = imagecreatefrompng( public_path().'/images/demo/v5.png' );

        // Apply mask to source
        $output = $this->imagealphamask($source, $mask );

        ob_start();
        imagepng($source);
        $imgData=ob_get_clean();

//        header( "Content-type: image/png");
//        imagepng( $source );

//        $img = \Image::make($output);
//
//        $originalPath = public_path().'/images/demo/';
//        $img->save($originalPath.time().'.png');

        return view('demo', compact('imgData'));

    }

    function imagealphamask( &$picture, $mask ) {

        // Get sizes and set up new picture
        $xSize = imagesx( $picture );
        $ySize = imagesy( $picture );
        $newPicture = imagecreatetruecolor( $xSize, $ySize );
        imagesavealpha( $newPicture, true );
        imagefill( $newPicture, 0, 0, imagecolorallocatealpha( $newPicture, 0, 0, 0, 127 ) );

        // Resize mask if necessary
        if( $xSize != imagesx( $mask ) || $ySize != imagesy( $mask ) ) {
            $tempPic = imagecreatetruecolor( $xSize, $ySize );
            imagecopyresampled( $tempPic, $mask, 0, 0, 0, 0, $xSize, $ySize, imagesx( $mask ), imagesy( $mask ) );
            imagedestroy( $mask );
            $mask = $tempPic;
        }

        // Perform pixel-based alpha map application
        for( $x = 0; $x < $xSize; $x++ ) {
            for( $y = 0; $y < $ySize; $y++ ) {
                $alpha = imagecolorsforindex( $mask, imagecolorat( $mask, $x, $y ) );
                $alpha = 127 - floor( $alpha[ 'red' ] / 2 );
                $color = imagecolorsforindex( $picture, imagecolorat( $picture, $x, $y ) );
                imagesetpixel( $newPicture, $x, $y, imagecolorallocatealpha( $newPicture, $color[ 'red' ], $color[ 'green' ], $color[ 'blue' ], $alpha ) );
            }
        }

        // Copy back to original picture
        imagedestroy( $picture );
        $picture = $newPicture;

        return $newPicture;
    }


    public function test()
    {
//        dd(Auth::user());
        dd($this->base->request('get','channels'));

        dd(Auth::user());

    }
    public function subcription(){
         dd($this->base->request('delete','downgrade'));
    }
    public function linkdin()
    {
        dd($this->base->request('get','linkedin/oauth'));
        dd(Auth::user());
    }
    public function fabebook()
    {
//        $params = json_encode(['subscription_name' => "STARTER", "country_code"=> "NLD"]);
//        $pageRegister = $this->base->postRequest('upgrade', $params);

//        dd($pageRegister);
        dd($this->base->request('get','orderitems'));
        dd(Auth::user());
    }
    public function twitter()
    {
        dd($this->base->request('get','/twitter/oauth'));
        dd(Auth::user());
    }
    public function textSearch(Request $request){
    $search=$request->get('category');
        if(app()->getLocale() =='en')
            $b_Text=BoncText::where('category',$search)
                ->pluck('text');
        else
            $b_Text=BoncTextDutch::where('category',$search)
                ->pluck('text');


//        foreach ($b_Text as $k => $text){
//            $newtxt = str_replace('[company name]', 'asd', $text);
//        }

        return response()->json($b_Text);

    }
    public function notificationList(Request $request){
        $carbon=Carbon::now()->toDateString();
        $notication=Notification::select('id',DB::raw("DATE_FORMAT(date, '%d-%b-%Y') as date"),'time', 'content', 'image')->where('date',$carbon)->where('status',0)->get();

        return view('notification', compact('notication'));
    }
    public function notification(Request $request){
        $carbon=Carbon::now()->toDateString();
        $notication=Notification::select('id',DB::raw("DATE_FORMAT(date, '%d-%b-%Y') as date"),'time')->where('date',$carbon)->where('status',0)->get();

        return response()->json(['status'=>200,'data'=>['notification'=>$notication,'count'=>count($notication)]]);
    }

    public function notificationView(Request $request){
        $id=$request->get('notification_id');
        $noticationView=Notification::select('id','content','image')->where('id',$id)->first();

        return response()->json(['status'=>200,'data'=>['notificationView'=>$noticationView]]);
    }
    public function photoEditor(Request $request){


        return view('photoEditor');
    }

    public function needHelpEmail(Request $request){

        $to_name = 'chirag';//$request->cntName;
        $to_email = 'bonckenya1@gmail.com';//$request->cntEmail;
        $data = ['name'=> $request->cntName, 'email' => $request->cntEmail, 'phone'=>$request->cntPhone];
        Mail::send('email.mail', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject('Bonc Subscription Inquiry');
        $message->from(env('MAIL_FROM_ADDRESS'),env('MAIL_FROM_NAME'));
        });

        Session::flash('success', 'Thanks for inquiry, our consultants will contact you soon');
        return redirect()->back();
    }
}
