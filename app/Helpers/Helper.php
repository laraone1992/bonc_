<?php

namespace App\Helpers;


class Helper
{
    public function generateResponse($status = false, $message = NULL, $statusCode = 200, $data = array(), $error = array())
    {
        $response["status"]     = $status;
        $response["message"]    = $message;
        $response["data"]     = $data;
        $response["error"]    = $error;

        return response()->json($response, $statusCode);
    }

    public function logError(\Throwable $e)
    {
        $request = $_REQUEST;
        $error = $e->getMessage() . "\n" . $e->getFile() . " (line : " . $e->getLine() . ")\n" . $e->getTraceAsString() . "\n\nHTTP_USER_AGENT : " . (isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : " ") . "\n\nRequestData : " . json_encode($request);
        \Log::error($error);
    }

    public static function urlCorrector($url){

        $pattern = '!([^:])(//)!';

        return preg_replace( $pattern, "$1/", $url );

    }

}