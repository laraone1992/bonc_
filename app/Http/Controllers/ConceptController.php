<?php

namespace App\Http\Controllers;

use App\BoncPhoto;
use App\BoncText;
use App\BoncTextDutch;
use App\PostMaster;
use App\Posts;
use Illuminate\Http\Request;
use App\Base;
use App\ConceptMaster;
use App\Concept;
use App\ConceptImage;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Image;

class ConceptController extends Controller
{
    protected $base;

    public function __construct(Base $base)
    {
        // $this->middleware('auth');
        $this->base = $base;
//        $ip = file_get_contents("http://ipecho.net/plain");
           $ip = $_SERVER['REMOTE_ADDR'];
            $url = 'http://ip-api.com/json/'.$ip;
            $tz = file_get_contents($url);
            $data=\GuzzleHttp\json_decode($tz);
            if($data->status == 'fail'){
             $timezone='Europe/Amsterdam';   
            }else{
             $timezone = json_decode($tz,true)['timezone'];
            }
           
            date_default_timezone_set($timezone);
            $value = config(['app.timezone'=>$timezone]);


    }

    public function index(Request $request)
    {

        $concept_ = ConceptMaster::where('is_scheduled', '0')->get();

        if (count($concept_) >= 1) {
            $concept_ = $concept_->toArray();

            foreach ($concept_ as $k => $value) {

                $concept_[$k]['creator'] = Auth::user()->name;
                $imgs = ConceptImage::where('master_id', $value['id'])->get();
                $concept_[$k]['channels'] = explode(',', $value['channels']);
                if ($imgs != null && count($imgs) > 1) {
                    $concept_[$k]['images'] = $imgs->toArray();
                    $concept_[$k]['cover'] = asset('storage/images/post/'.$concept_[$k]['images'][0]['image']);
                } else {
                    $concept_[$k]['images'] = [];
                    $concept_[$k]['cover'] = '';
                }
            }
        }

        $count = count($concept_);

        return view('concept', compact('concept_', 'count'));

    }

    public function conceptData(Request $request)
    {
        $data = $request->all();

        try {

            $postImage['imagename'] = [];
            foreach ($data['channel'] as $key => $value) {
                $channel[] = $key;
            }

            $conceptMaster = new ConceptMaster;
            $conceptMaster->u_uuid = Auth::user()->org_uuid;
            $conceptMaster->content = $data['text'];
            $conceptMaster->channels = implode(',', $channel);
            $conceptMaster->is_scheduled = '0';
            $conceptMaster->is_posted = '0';
            $conceptMaster->save();

            if ($request->has('your_photo')) {
                $postImage = $this->thumbnailImage($request, 'your_photo');

                $images = $request->file('your_photo');

                foreach ($postImage['imagename'] as $k => $image) {
                    $conceptImage = new ConceptImage;
                    $conceptImage->master_id = $conceptMaster->id;
                    $conceptImage->image = $image;
                    $conceptImage->save();
                }
            }

            foreach ($data['channel'] as $key => $value) {
                $datePost = Carbon::createFromFormat('d/m/Y', $data['date'][$key])->toDateString();
                $timePost = Carbon::createFromFormat('H:i', $data['time'][$key])->toTimeString();

                $concept = new Concept;
                $concept->master_id = $conceptMaster->id;
                $concept->text = $data['text'];
                $concept->u_uuid = Auth::user()->org_uuid;
                $concept->c_uuid = $data['uuid'][$key];
                $concept->channel = $key;
                $concept->image = isset($postImage['imagename'][0])?$postImage['imagename'][0]:'';
                $concept->date = $datePost;
                $concept->time = $timePost;
                $concept->save();


            }


            Session::flash('success', 'Concept Scheduled');
            return response()->json(['status' => 200, 'data' => ['redirect_url' => url('concept')]]);
        } catch (\Exception $e) {
            dd('here', $e);
            Session::flash('erro', 'Something went wrong');
            return redirect()->back();

        } catch (GuzzleException $ge) {
            dd('herea');
            Session::flash('erro', 'Something went wrong');
            return redirect()->back();

        }


    }

    public function compareByTimeStamp($time1, $time2)
    {
        if (strtotime($time1) > strtotime($time2))
            return 1;
        else if (strtotime($time1) < strtotime($time2))
            return -1;
        else
            return 0;
    }

    public function editConcept($id, Request $request)
    {

        $u_uuid = Auth::user()->org_uuid;
        $concept = [];

        $concept_ = ConceptMaster::with(['concept', 'conceptImage'])->where('id', $id)->first()->toArray();

        $concept_['channels'] = explode(',', $concept_['channels']);

        $channels = $this->base->request('get', 'channels');

        $channels = $channels['data'];

        foreach($concept_['concept'] as $k => $value){
            foreach ($channels as $ck => $channel){
                if($value['c_uuid'] == $channel['uuid']){
                    $channels[$ck]['date'] = $value['date'];
                    $channels[$ck]['time'] = $value['time'];
                    continue;
                }else if (!isset($channel['date'])){
                    $channels[$ck]['date'] = Carbon::now()->toDateString();
                    $channels[$ck]['time'] = Carbon::now()->toTimeString();
                }
            }
        }
        $boncPhoto=BoncPhoto::select(DB::raw('distinct category'))->where('type','photo')->get()->toArray();
        $boncQuote=BoncPhoto::select(DB::raw('distinct category'))->where('type','quote')->get()->toArray();
        if(app()->getLocale() =='en')
            $textCategory=BoncText::select(DB::raw('distinct category'))->get()->toArray();
        else
            $textCategory=BoncTextDutch::select(DB::raw('distinct category'))->get()->toArray();

        return view('editConcept', compact('channels', 'concept_', 'boncPhoto', 'boncQuote', 'textCategory'));
    }

    public function updateConcept(Request $request)
    {

        $data = $request->all();


        if (isset($data['delete_image_id']) && $data['delete_image_id'] != null) {
            $deleteId = explode(',', $data['delete_image_id']);
            ConceptImage::whereIn('id', $deleteId)->delete();
        }

        if (isset($data['your_photo'])) {
            $postImage = $this->thumbnailImage($request, 'your_photo');
            foreach ($postImage['imagename'] as $k => $image) {
                $conceptImage = new ConceptImage();
                $conceptImage->master_id = $data['master_id'];
                $conceptImage->image = $image;
                $conceptImage->save();
            }
        }

        foreach ($data['channel'] as $key => $value) {
            $channel[] = $key;
        }

        $conceptMaster = ConceptMaster::where('id', $data['master_id'])->first();
        $conceptMaster->u_uuid = Auth::user()->org_uuid;
        $conceptMaster->content = $data['text'];
        $conceptMaster->channels = implode(',', $channel);
        $conceptMaster->is_scheduled = '0';
        $conceptMaster->is_posted = '0';
        $conceptMaster->save();

        Concept::where('master_id', $conceptMaster->id)->delete();
        foreach ($data['channel'] as $key => $value) {
            $datePost = Carbon::createFromFormat('d/m/Y', $data['date'][$key])->toDateString();
            $timePost = Carbon::createFromFormat('H:i', $data['time'][$key])->toTimeString();

            $concept = new Concept();
            $concept->master_id = $conceptMaster->id;
            $concept->text = $data['text'];
            $concept->u_uuid = Auth::user()->org_uuid;
            $concept->c_uuid = $data['uuid'][$key];
            $concept->channel = $key;
            $concept->image = '';
            $concept->date = $datePost;
            $concept->time = $timePost;
            $concept->save();


        }
        if (isset($data['schedule']) && $data['schedule'] == 1){
            $res = $this->scheduleData($request, $data['master_id']);
            if ($res['status'] == 200){
                Session::flash('success', 'Concept Scheduled Successfully ');
                return response()->json(['status' => 200, 'data' => ['redirect_url' => $res['redirect_url']]]);
            }
        }

        Session::flash('success', 'Concept Schedule Updated Successfully ');
        return response()->json(['status' => 200, 'data' => ['redirect_url' => url('concept')]]);
    }
    public function scheduleData(Request $request, $master_id)
    {
        $conceptMaster = ConceptMaster::where('id', $master_id)->first();

        try {
            $conceptMaster = ConceptMaster::where('id', $master_id)->first();
            $imgCvr = '';
            $mediaUuid = [];
            $images_ = ConceptImage::where('master_id',$conceptMaster->id)->get();
            if($images_ != null && count($images_) > 1){
                $images = $images_->toArray();

                $this->client = new Client();
                $uri = env('API_URL') . '/media';
                $multipart = [];
                foreach ($images as $k => $img) {

                    $multipart[] = [
                        'name' => 'data',
                        'contents' => fopen(public_path('storage/images/post/'.$img['image']), 'r'),
                    ];
                    $media = $this->base->postRequest('media', $multipart);
                    $mediaUuid[] =$media->uuid;

                }

                $imgCvr = $images[0]['image'];
            }

            $concepts_ = Concept::where('master_id', $conceptMaster->id)->get();

            if ($concepts_ != null){
                $concepts = $concepts_->toArray();
                foreach ($concepts as $key => $value) {
                    $channel[] = $value['channel'];
                }
            }

            $postMaster = new PostMaster;
            $postMaster->u_uuid = Auth::user()->org_uuid;
            $postMaster->image = $imgCvr;
            $postMaster->content = $conceptMaster->content;
            $postMaster->channels = implode(',', $channel);
            $postMaster->is_scheduled = '1';
            $postMaster->is_posted = '0';
            $postMaster->save();

            $body = ['text' => $conceptMaster->content, 'media_uuid' =>  $mediaUuid];
            $publication = [];
            $i = 0;

            foreach ($concepts as $key => $value) {

                $datePost = Carbon::createFromFormat('Y-m-d', $value['date'])->toDateString();
                $timePost = Carbon::createFromFormat('H:i:s', $value['time'])->toTimeString();

                $post = new Posts;
                $post->master_id = $postMaster->id;
                $post->text = $value['text'];
                $post->u_uuid = Auth::user()->org_uuid;
                $post->c_uuid = $value['c_uuid'];
                $post->channel = $value['channel'];
                $post->image = $imgCvr;
                $post->media = '';
                $post->date = $datePost;
                $post->time = $timePost;

                $datetime = Carbon::createFromFormat('Y-m-d H:i:s', $value['date'] . ' ' . $value['time'], 'Asia/Kolkata')->setTimezone('UTC')->toDateTimeString();

                if ($value['channel'] == 'instagram') {
                    $notification = new \App\Notification();
                    $notification->content = $value['text'];
                    $notification->image = $imgCvr;
                    $notification->status = 0;
                    $notification->date = $datePost;
                    $notification->time = $timePost;
                    $notification->save();
                } else {
                    $publication[$i] = (object)[
                        'channel_uuid' => $value['c_uuid'],
                        'publishTime' => $datetime,
                        'body' => (object)$body
                    ];
                    $publications = $this->base->postRequest('publications', json_encode(['publications' => $publication]));
                    $post->post_id =$publications['data'][0]['uuid'];
                    $post->save();

                }
                Concept::find($value['id'])->delete();

            }
            ConceptMaster::find($conceptMaster->id)->delete();

            return ['status' => 200, 'redirect_url' => url('schedule')];
        } catch (\Exception $e) {
            dd('here', $e);
            Session::flash('erro', 'Something went wrong');
            return redirect()->back();

        } catch (GuzzleException $ge) {
            dd('herea');
            Session::flash('erro', 'Something went wrong');
            return redirect()->back();

        }

    }
    public function confirmConceptPost(Request $request,$master_id){
        $res = $this->scheduleData($request, $master_id);
        if ($res['status'] == 200){
            Session::flash('success', 'Concept Confirm Successfully ');
            return redirect()->route('schedule');
        }else{
            return response()->json(['status' => 500]);
        }
    }

    public function deleteConcept($id,Request $request){

        $concept_Master=ConceptMaster::where('id',$id)->delete();
        $concept=Concept::where('master_id',$id)->delete();
        $concept_Image=ConceptImage::where('master_id',$id)->delete();
        Session::flash('success', 'Delete Concept Successfully ');
        return redirect()->back();
    }
}
