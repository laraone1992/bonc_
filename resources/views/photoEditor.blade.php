<!DOCTYPE html>
<html>
<head>
    <!-- React Dependencies for the SDK UI -->
    <script src="{{asset('')}}js/vendor/react.production.min.js"></script>
    <script src="{{asset('')}}js/vendor/react-dom.production.min.js"></script>
    <!-- PhotoEditor SDK-->
    <script src="{{asset('js/PhotoEditorSDK.min.js')}}"></script>
    <!-- PhotoEditor SDK UI -->
    <script src="{{asset('js/PhotoEditorSDK.UI.DesktopUI.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('css/PhotoEditorSDK.UI.DesktopUI.min.css')}}" />
</head>

<body>
<div class="modal fade bd-example-modal-lg" id="Photo" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="editor" style="width: 100vw; height: 100vh;"></div>
        </div>
    </div>
</div>
<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script>
    window.onload = function () {
        $('#Photo').modal('show');
        var image = new Image()
        image.onload = function () {
            var container = document.getElementById('editor')
            var editor = new PhotoEditorSDK.UI.DesktopUI({
                container: container,
                // Please replace this with your license: https://www.photoeditorsdk.com/dashboard/subscriptions
                license: '{"api_token":"KXfrRUwfZO7L6ewcfMw2YQ","app_identifiers":["localhost"],"available_actions":[],"domains":["https://api.photoeditorsdk.com"],"enterprise_license":false,"expires_at":1583366400,"features":["camera","library","export","customassets","whitelabel","focus","textdesign","transform","brush","text","frame","overlay","sticker","adjustment","filter"],"issued_at":1580809789,"minimum_sdk_version":"1.0","owner":"pratik patel","platform":"HTML5","products":["pesdk"],"version":"2.4","signature":"P6wrgtIFg39/RLVOlu4QhNP2ET0MFYb09FygUk435Fuo0Hi0DzuT6dBUibxFzNXsoBDZ1qnRvzxSbFakA06ov1Qe2/w4T4PiLnnJuKQLX5MK+KmrPKBIsVddTc73Utct4H+BJVQLmcpU+felFEbDUntBiigRzPNR3mAnVYAhSXm/kA82Am7/4A4MImRVsz3zS2H+VNio+Klj3Cb9/g0oAL56M9pYTlVbAFllfUgFs4W0vGV+iKD0ylkfV6s7V6fyNGcr2U//XQIr6ITv491wkKzw6rvfkLwxfkEvss6QbcTevt1E2OSyOiZeZQNdDGZQeaNVQQNhG95RvrMgWqOzSa6B+z78/TGg3TdiyZvuoX9t4TH3cRo+H0REI/Qrb9p7ttyNHPCdYoLI4dGvHin9WH4mT/KbxrwWrCJo2mf3hvIo4Vt+mUJYPyd6UTn89vhwc5Ru78z0SjY8l6bvrQO1gI9x3THbflLOnOH33CiCEXVQIrArKewibqsUPrD6a3XBHO3sFdciOXVOQROBq841trBOJIgsz2t3fF4igj4pPgwvPH0nFItX6mKAmPVth4tTImh+v5nUUxctRRK2GFo5BRvti7GN8qwuuSnxH9HLH/IXaihNFXNFMv18yGTj5UuMINqp5ijeXhPDSaLxlhF/C9GB8Ehw91mNXVTBlu3eR80="}',
                editor: {
                    image: image
                },
                assets: {
                    // This should be the absolute path to your `assets` directory
                    baseUrl: '{{asset('assets')}}'
                }
            })
        }
        // image.crossOrigin = 'Anonymous'  // Setup CORS accordingly if needed
        image.src = 'images/img.png'
    }
</script>
</body>
</html>
