@extends('layouts.layout')
@section('title','Schedule')

@section('content')
    <!-- Content Start -->
    <div id="content" class="d-flex">
        <!-- =========================================
        Left Section
        ========================================== -->
        <section class="left-section sidebar">
            <div id="calendar"></div>
        </section>
        <!-- =========================================
        Content Section
        ========================================== -->
        <section class="content-section section-schedule flex-fill">

            @if(Session::has('success'))
                <p class="alert alert-success">{{ Session::get('success') }}</p>
            @endif
            @if(Session::has('error'))
                <p class="alert alert-danger">{{ Session::get('error') }}</p>
            @endif
                <div class="page-header">{{__('message.NOTIFICATION_HEADER')}}</div>
            <div class="card mx-100">
                <div class="card-body py-0">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="summary" role="tabpanel" aria-labelledby="summary-tab">
                            <div class="scroll-cust">
                                @if(count($notication) == 0)
                                    <div class="no-post-summ no-notification-summ summary-block d-flex align-items-center justify-content-center mt-3">
                                        {{__('message.No_notification_found')}}
                                    </div>
                                @else
                                @foreach($notication as $key => $item)
                                    <div class="notification-block">
                                        <li class="d-flex" onclick="viewMobileNotification('{{$item->content}}', '{{$item->image}}')">
                                            <span class="notification-icon d-flex">
                                                <img src="{{asset('images/instagram.svg')}}">
                                            </span>
                                            <div class="txt-nt ">
                                                <p class="nt-cnt">
                                                    You have asked me to remind you that you want to post something on instagram {{$item->date}} at {{$item->time}}</p>
                                            </div>
                                        </li>
                                    </div>
                                @endforeach
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- =========================================
        Right Section
        ========================================== -->
        <section class="right-section sidebar scroll-cust">

            <div class="side-accord">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            {{__('message.Need_Help_with_Schedulling_Posts')}}
                            <span class="acc-arrow">
									<em class="fa fa-angle-right"></em>
								</span>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                {{__('message.Need_Help_with_Schedulling_Posts_content')}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
{{--            <div class="start-chat d-flex align-items-center justify-content-between">--}}
{{--                <span>Start Chatting</span>--}}
{{--                <button type="button" class="btn btn-secondary">Click here</button>--}}
{{--            </div>--}}
            <div class="thumbnail-group ">
                <div class="thumbnail-block">
                    <div class="thumb">
                        <img src="{{asset('')}}images/ic/image-1.jpg" alt="">
                    </div>
                    <div class="caption">
                        <h4>{{__('message.How_to_schedule_post')}}</h4>
                        <p>{{__('message.How_to_schedule_post_content')}}</p>
                        <a href="https://bonconline.com/en/blog/online-marketingtrends.html?fbclid=IwAR0sryOaPf0rgbZXKVJIEbTY3YP0zRpPvh-Fc8_0fIHS1X8FkZdS_00aLic" target="_blank">{{__('message.Read_more')}}</a>
                    </div>
                </div>

            </div>
        </section>
    </div>

@endsection

@push('styles')
    <link href="{{asset('')}}css/pignose.calendar.min.css" rel="stylesheet"><!-- pignose.calendar css -->
@endpush
@push('scripts')
    <script src="{{asset('')}}js/pignose.calendar.full.min.js"> </script><!-- pignose.calendar.full.min -->
    <script>

    $(".alert").delay(3000).slideUp(800, function() {
        $(this).alert('close');
    });
    $(document).ready(function (){
        $("#click").click(function (){
            $('html, body').animate({
                scrollTop: $("#div1").offset().top
            }, 2000);
        });
    });

    function viewMobileNotification(content, image) {
        console.log(content, image);
        var html = '';
        html +='<div class="col-md-12 notification-image-block">'+
                    '<img src="storage/images/post/'+image+'" class="notification-image">'+
            '</div>'+
            '<div class="col-md-12">' +
                '<p id="p1" class="notification-content">'+content+'</p>'+
                '<input type="text" value="'+content+'" id="copysource" style="display: none">'+
            '</div>'+
            '<div class="col-md-12">' +
            // '<img src="images/Copy_Filled-512@2x.png" class="clickboard"> <a href="#" class="clipboardtext" onclick="copyToClipboard()">Copy text to clipboard</a href="#">'
            '</div>';
        $("#viewNotification").html(html);
        $("#download-img").attr('href',"storage/images/post/"+image);
        $('#notificationview').modal('show');
    }


    </script>

@endpush
