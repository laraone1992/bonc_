@extends('layouts.layout')
@section('title','Edit Concept')

@section('content')

    <!-- Content Start -->
    <div id="content" class="d-flex">
        <!-- =========================================
        Left Section
        ========================================== -->
        <section class="left-section sidebar">

            {{--            <div class="recent-post">--}}
            {{--                <div class="recent-post-head d-flex align-items-center justify-content-between">--}}
            {{--                    <h3 class="mb-0">{{__('message.Recent_Posts')}}</h3>--}}

            {{--                </div>--}}
            {{--            </div>--}}
            <div class="recent-post recent-post-cnt">
                <div class="recent-post-body scroll-cust">
                    <div class="recent-post-block">
                        <div class="thumb">
                            <img src="{{asset('')}}images/img1.png" alt="">
                        </div>
                        <div class="caption">
                            <div class="cap-heading">
                                <h4> {{__('message.Need help to get started')}}</h4>
                                <p>{{__('message.To get you started')}} {{__('message.Starters_package')}}{!! __('message.We will create Business')!!}
                                <div class="help-form">
                                    <form action="{{url('need-help')}}" method="get" class="form" >
                                        <div class="row mt-3">
                                            <div class="col-md-3">
                                                <label class="">Name</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="cntName" required>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-3">
                                                <label class="">Phone</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control " name="cntPhone" required>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-3">
                                                <label class="">Email</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="email" class="form-control" name="cntEmail" required>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-3">

                                            </div>
                                            <div class="col-md-9">
                                                <input type="submit" class="btn btn-secondary help-form-send" name="cnt-submit" id="cnt-submit" value="Send">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                {{--{{__('message.Click_here')}}</a> {{__('message.consultants')}}--}}
                                </p>
                            </div>
                            {{--                            <div class="post-likes-wrap overlays">--}}
                            {{--                                <div class="text">Coming Soon..!</div>--}}
                            {{--                                <div class="post-likes d-flex align-items-center justify-content-between">--}}
                            {{--                                    <div class="d-flex align-items-center">--}}
                            {{--                                        <img src="{{asset('')}}images/facebook.svg" alt="" class="analytics">--}}
                            {{--                                        <div class="like-counter d-flex align-items-center">--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/like-icon.png" alt="">171</span>--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/comment-icon.png" alt="">1</span>--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/share-icon.png" alt="">102</span>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <span class="cursor-pointer">--}}
                            {{--											<img src="{{asset('')}}images/ic/upload-icon.png" alt="">--}}
                            {{--										</span>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="post-likes d-flex align-items-center justify-content-between">--}}
                            {{--                                    <div class="d-flex align-items-center">--}}
                            {{--                                        <img src="{{asset('')}}images/twitter.svg" alt="" class="analytics">--}}
                            {{--                                        <div class="like-counter d-flex align-items-center">--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/t-comment-icon.png" alt="">1</span>--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/t-share-icon.png" alt="">30</span>--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/icon-heart.png" alt="">51</span>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <span class="cursor-pointer">--}}
                            {{--											<img src="{{asset('')}}images/ic/upload-icon.png" alt="">--}}
                            {{--										</span>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="post-likes d-flex align-items-center justify-content-between">--}}
                            {{--                                    <div class="d-flex align-items-center">--}}
                            {{--                                        <img src="{{asset('')}}images/linkedin.svg" alt="" class="analytics">--}}
                            {{--                                        <div class="like-counter d-flex align-items-center">--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/like-icon.png" alt="">171</span>--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/comment-icon.png" alt="">1</span>--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/share-icon.png" alt="">102</span>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <span class="cursor-pointer">--}}
                            {{--											<img src="{{asset('')}}images/ic/upload-icon.png" alt="">--}}
                            {{--										</span>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                        </div>
                    </div>
                    <div class="recent-post-block">
                        <div class="thumb">
                            <img src="{{asset('')}}images/ic/image-2.jpg" alt="">
                        </div>
                        <div class="caption">
                            <div class="cap-heading">
                                <h4>{{__('message.Your analytics on one page')}}</h4>
                                <p>{{__('message.Recent_Post_Content')}}</p>
                            </div>
                            <div class="post-likes-wrap overlays">
                                <div class="coming-soon-text">{{__('message.Coming Soon..!')}}</div>
                                <div class="post-likes d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center">
                                        <img src="{{asset('')}}images/facebook.svg" alt="" class="analytics">
                                        <div class="like-counter d-flex align-items-center">
                                            <span><img src="{{asset('')}}images/ic/like-icon.png" alt="">171</span>
                                            <span><img src="{{asset('')}}images/ic/comment-icon.png" alt="">1</span>
                                            <span><img src="{{asset('')}}images/ic/share-icon.png" alt="">102</span>
                                        </div>
                                    </div>
                                    <span class="cursor-pointer">
											<img src="{{asset('')}}images/ic/upload-icon.png" alt="" style="opacity: 0.3">
										</span>
                                </div>
                                <div class="post-likes d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center">
                                        <img src="{{asset('')}}images/twitter.svg" alt="" class="analytics">
                                        <div class="like-counter d-flex align-items-center">
                                            <span><img src="{{asset('')}}images/ic/t-comment-icon.png" alt="">1</span>
                                            <span><img src="{{asset('')}}images/ic/t-share-icon.png" alt="">30</span>
                                            <span><img src="{{asset('')}}images/ic/icon-heart.png" alt="">51</span>
                                        </div>
                                    </div>
                                    <span class="cursor-pointer">
											<img src="{{asset('')}}images/ic/upload-icon.png" alt="" style="opacity: 0.3">
										</span>
                                </div>
                                <div class="post-likes d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center">
                                        <img src="{{asset('')}}images/linkedin.svg" alt="" class="analytics">
                                        <div class="like-counter d-flex align-items-center">
                                            <span><img src="{{asset('')}}images/ic/like-icon.png" alt="">171</span>
                                            <span><img src="{{asset('')}}images/ic/comment-icon.png" alt="">1</span>
                                            <span><img src="{{asset('')}}images/ic/share-icon.png" alt="">102</span>
                                        </div>
                                    </div>
                                    <span class="cursor-pointer">
											<img src="{{asset('')}}images/ic/upload-icon.png" alt="" style="opacity: 0.3">
										</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- =========================================
        Content Section
        ========================================== -->
        <section class="content-section flex-fill scroll-cust">
            {{--            <div class="d-flex align-items-center justify-content-between mb-4">--}}
            {{--                <button type="button" class="side-button left-sidebutton btn btn-secondary">Left Sidebar</button>--}}
            {{--                <button type="button" class="side-button right-sidebutton btn btn-secondary">Right Sidebar</button>--}}
            {{--            </div>--}}
            @if(Session::has('success'))
                <p class="alert alert-success">{{ Session::get('success') }}</p>
            @endif
            @if(Session::has('error'))
                <p class="alert alert-danger">{{ Session::get('error') }}</p>
            @endif
            <form action="{{url('post-data')}}" method="post" enctype="multipart/form-data" id="post-form">
                @csrf
                {{--            <div class="card">--}}
                {{--                <div class="card-body py-0">--}}
                <input type="hidden" name="master_id" value="{{$concept_['id']}}">
                <div class="tab-content">
                    <div class="post-wrap">
                        <div class="post-block position-relative">
                            {{--                                <span class="post-small-log"><img src="{{asset('')}}images/ic/post-logo.png" alt=""></span>--}}
                            <div class="panel">
                                {{--                                    <div class="panel-head">--}}
                                {{--                                        Great to see you! Lets post something online!--}}
                                {{--                                    </div>--}}
                                <div class="panel-body p-0">
{{--                                    <div class="add-pic d-md-flex align-items-center --}}{{--justify-content-between--}}{{--">--}}
{{--                                        {{__('message.Add_picture_video')}}--}}
{{--                                        <input type="file" name="your_photo[]" id="your-photo" multiple>--}}
{{--                                        <div class="d-md-flex d-block " >--}}
{{--                                            <div class="btn-group pl-5 img-sel-post" >--}}

{{--                                                <button type="button" class="btn btn-secondary" id="your-photo-btn" type="images" >--}}
{{--                                                    <img src="{{asset('')}}images/upload.svg" class="wh15" alt="">--}}
{{--                                                    {{__('message.Your_photos')}}--}}
{{--                                                </button>--}}
{{--                                                <button type="button" class="btn btn-secondary">--}}
{{--                                                    <img src="{{asset('')}}images/upload.svg" alt="" class="wh15">--}}
{{--                                                    {{__('message.Bonc_photos')}}--}}
{{--                                                </button>--}}
{{--                                                <button type="button" class="btn btn-secondary" >--}}
{{--                                                    <img src="{{asset('')}}images/upload.svg" alt="" class="wh15">--}}
{{--                                                    {{__('message.Bonc_quotes')}}--}}
{{--                                                </button>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="write-here row ">
                                            <div class="col-12">
                                                <textarea class="form-control" rows="4" name="text" id="text"
                                                          placeholder="{{__('message.Write_your_post')}}">{{isset($concept_['content'])? trim($concept_['content']) :''}}</textarea>
                                            </div>
                                        </div>

                                        <div class="file-input">
                                            <div class="file-preview scroll-cust">
                                                <div class="file-drop-disabled">
                                                    <div class="file-preview-thumbnails" id="selectedFiles">
                                                        @foreach($concept_['concept_image'] as $image)

                                                        <div class='file-preview-frame krajee-default  kv-preview-thumb'>
                                                            <div class='kv-file-content'>
                                                                <img src="{{asset('storage/images/post/'.$image['image'])}}" class='file-preview-image kv-preview-data' style='width:auto;height:auto;max-width:100%;max-height:100%;'>
                                                                <div class="overlay-file"></div>
                                                                <div class="button-file button-file-remove" data-file=""  data-img-id="{{$image['id']}}" onclick="removeImg($(this))" title ='remove'>
                                                                <a href='#'> <img src='{{asset('')}}images/close.svg'> </a></div>
    {{--                                                        <div class="button-file button-file-edit edit-post-i" onclick="editPost($(this))" data-image="{{$image['img']}}" title='edit'>--}}
    {{--                                                        <a href='#'> <img src='{{asset('')}}images/brush.svg'> </a></div>--}}
                                                        </div></div>
                                                            @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="media-btn-group">

                                        <div class="row ">

                                            <div class="col-media">
                                                <button type="button" class="btn btn-home-grey btn-block " id="your-photo-btn"
                                                        type="images">
                                                    <img src="{{asset('')}}images/upload_black.svg" class="wh15 inverse-clr" alt="">
                                                    {{__('message.Your_photos')}}
                                                </button>
                                            </div>
                                            <div class="col-media">
                                                <button type="button" class="btn btn-home-grey btn-block " id="bonc-photo"
                                                        data-toggle="modal" data-target="#BoncModalPhoto">
                                                    <img src="{{asset('')}}images/photos.svg" alt="" class="wh15">
                                                    {{__('message.Bonc_photos')}}
                                                </button>
                                            </div>
                                            <div class="col-media">
                                                <button type="button" id="BoncQuote" class="btn btn-home-grey btn-block " data-toggle="modal"
                                                        data-target="#BoncModalQuote">
                                                    <img src="{{asset('')}}images/quote.svg" alt="" class="wh15">
                                                    {{__('message.Bonc_quotes')}}
                                                </button>
                                            </div>
                                            <div class="col-media">
                                                <button type="button" id="text_select"
                                                        class="btn btn-home-grey btn-block ">
                                                    <img src="{{asset('')}}images/bonctext.svg" alt="" class="wh15">
                                                    {{__('message.Bonc_text')}}
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                    <input type="file" name="your_photo[]" id="your-photo" multiple>
                                    <div class="post-on d-md-flex align-items-center">
                                        {{__('message.Post_on')}}
                                        <div class="post-persons d-flex flex-wrap">
                                            @foreach($channels as $key => $channel)
                                                @if($channel['type'] != 'facebook_profile')
                                                    <div class="posters position-relative">
                                                        @if($channel['type'] == 'facebook_profile')
                                                        @elseif($channel['type'] == 'facebook_page')
                                                            <img src="{{asset('')}}images/facebook.svg" class="social-post" alt="" >
                                                        @elseif($channel['type'] == 'linkedin_profile')
                                                            <img src="{{asset('')}}images/linkedin.svg" class="social-post" alt="" >
                                                        @elseif($channel['type'] == 'twitter')
                                                            <img src="{{asset('')}}images/twitter.svg" class="social-post" alt="" >
                                                        @endif
                                                        <span class="post-user">
                                                                <input type="checkbox" id="cb{{$key}}" data-id="{{$channel['uuid']}}" name="channel[{{$channel['type']}}]" class="channel-chk" @if(in_array($channel['type'],$concept_['channels'])) checked @else  @endif />
                                                                <label for="cb{{$key}}">
                                                                    <img src="{{$channel['profile_picture_url']}}" alt="" onerror="this.onerror=null;this.src='{{asset('images/noimage.png')}}';">
                                                                </label>
                                                            </span>
                                                        <input type="hidden" name="uuid[{{$channel['type']}}]" value="{{$channel['uuid']}}"/>
                                                    </div>
                                                @endif

                                            @endforeach
                                            <div class="posters position-relative">

                                                <img src="{{asset('')}}images/instagram.svg" class="social-post" alt="" >
                                                <span class="post-user">
                                                            <input type="checkbox" id="cb{{count($channels)+1}}" data-id="123123" name="channel[instagram]" class="channel-chk" @if(in_array('instagram',$concept_['channels'])) checked @endif />
                                                            <label for="cb{{count($channels)+1}}">
                                                                <img src="" alt="" onerror="this.onerror=null;this.src='{{asset('images/noimage.png')}}';">
                                                            </label>
                                                        </span>
                                                <input type="hidden" name="uuid[instagram]" value="123123"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="post-date-block post-block position-relative">
                            {{--                                <span class="post-small-log"><img src="{{asset('')}}images/ic/post-logo.png" alt=""></span>--}}
                            <div class="panel">

                                <div class="panel-body">
                                    <div class="date-time-wrap">
                                        @foreach($channels as $key => $channel)
                                            @if($channel['type'] != 'facebook_profile')
                                                <div @if(in_array($channel['type'],$concept_['channels'])) class="date-time-block position-relative" @else class="date-time-block position-relative disable-block" @endif id="date-time-{{$channel['uuid']}}">
                                                    @if($channel['type'] == 'facebook_profile')
                                                        <img src="{{asset('')}}images/facebook.svg" class="lg-social" alt="" >
                                                    @elseif($channel['type'] == 'facebook_page')
                                                        <img src="{{asset('')}}images/facebook.svg" class="lg-social" alt="" >
                                                    @elseif($channel['type'] == 'linkedin_profile')
                                                        <img src="{{asset('')}}images/linkedin.svg" class="lg-social" alt="" >
                                                    @elseif($channel['type'] == 'twitter')
                                                        <img src="{{asset('')}}images/twitter.svg" class="lg-social" alt="" >
                                                    @endif
                                                    <div class="row">
                                                        <div class="col-7">
                                                            <div class="d-flex align-items-center date-group">
                                                                <label>{{__('message.Date')}}</label>
                                                                <div class="datepick flex-fill form-group">
                                                                    <input type="text" class="form-control dateSelect noscroll"  id="date{{$key}}"  value="{{Carbon\Carbon::parse($channel['date'])->format('d/m/Y')}}"  name="date[{{$channel['type']}}]" readonly='true' placeholder="dd/mm/yyyy">
                                                                    <span class="cal-ico date-ico" for="date{{$key}}">
                                                                        <img src="{{asset('')}}images/calendar.svg">
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-5">
                                                            <div class="d-flex align-items-center time-group">
                                                                <label>{{__('message.Time')}}</label>
                                                                <div class="datepick flex-fill form-group">
                                                                    <input type="text" class="form-control timeSelect" id="time{{$key}}" value="{{Carbon\Carbon::parse($channel['time'])->format('H:i')}}" name="time[{{$channel['type']}}]" readonly='true' placeholder="hh:mm">
                                                                    <span class="cal-ico time-ico" for="time{{$key}}">
                                                                        <img src="{{asset('')}}images/clock.svg">
                                                                </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach

                                        <div class="date-time-block position-relative @if(in_array('instagram',$concept_['channels'])) @else disable-block @endif " id="date-time-123123">
                                            <img src="{{asset('')}}images/instagram.svg" alt="" class="lg-social">
                                            <div class="row">
                                                <div class="col-7">
                                                    <div class="d-flex align-items-center date-group">
                                                        <label>{{__('message.Date')}}</label>
                                                        <div class="datepick flex-fill form-group">
                                                            <input type="text" class="form-control dateSelect" id="date40" value="{{Carbon\Carbon::now()->format('d/m/Y')}}" name="date[instagram]" readonly='true' placeholder="dd/mm/yyyy">
                                                            <span class="cal-ico date-ico" for="date40">
                                                                        <img src="{{asset('')}}images/calendar.svg">
                                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-5">
                                                    <div class="d-flex align-items-center time-group">
                                                        <label>{{__('message.Time')}}</label>
                                                        <div class="datepick flex-fill form-group">
                                                            <input type="text" class="form-control timeSelect" id="time40" value="{{Carbon\Carbon::now()->addHour()->startOfHour()->format('H:i')}}" name="time[instagram]" readonly='true' placeholder="hh:mm">
                                                        </div>
                                                        <span class="cal-ico time-ico" for="time40">
                                                                    <img src="{{asset('')}}images/clock.svg">
                                                                </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="btn-post-grp">
                                <button class="btn btn-secondary btn-cnfm btn-lg" id="submit" type="submit">{{__('message.Update')}}</button>
                                <button class="btn btn-secondary btn-cnfm btn-lg" id="save" type="submit">{{__('message.Post')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
        <div class="modal " id="BoncModalText" role="dialog" aria-labelledby="exampleModalCenterTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header black-header">
                        <span>{{__('message.Bonc_text')}}</span>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="close-popup-btn">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body bonc-txt-body">

                        <select class="form-control bonc-txt-select" name="category" id="category">
                            <option value="__">{{__('message.Select_category')}}</option>
                            @foreach($textCategory as $category)
                                <option value="{{$category['category']}}">{{$category['category']}}</option>
                            @endforeach
                        </select>
                        <input type="hidden" id="org_name" value="">
                        <ul class="bonc-txt-ul" id="list_text">

                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="BoncModalPhoto" tabindex="-1" role="dialog" aria-labelledby="BoncModalCenterTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header black-header">
                        <span>{{__('message.Bonc_photo_text')}}</span>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="close-popup-btn">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body bonc-photo-body">
{{--                        <select class="form-control bonc-txt-select" name="photo_category" id="photo_category">--}}
{{--                            <option value="__">{{__('message.Select_category')}}</option>--}}
{{--                            @foreach($boncPhoto as $category)--}}
{{--                                <option value="{{$category['category']}}">{{$category['category']}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
                        <div class="bonc-photo-div" id="list_text">
                            <div class="row" id="list_photo">


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="BoncModalQuote" tabindex="-1" role="dialog" aria-labelledby="BoncModalCenterTitle"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header black-header">
                        <span >{{__('message.Bonc_photo_text')}}</span>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="close-popup-btn">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body bonc-photo-body">
{{--                        <select class="form-control bonc-txt-select" name="quote_category" id="quote_category">--}}
{{--                            <option value="__">{{__('message.Select_category')}}</option>--}}
{{--                            @foreach($boncQuote as $category)--}}
{{--                                <option value="{{$category['category']}}">{{$category['category']}}</option>--}}
{{--                            @endforeach--}}
{{--                        </select>--}}
                        <div class="bonc-photo-div" id="list_text">
                            <div class="row" id="list_quote">


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- =========================================
        Right Section
        ========================================== -->
        <section class="right-section sidebar scroll-cust">
            <div class="right-sec-top">
                <h4>{{__('message.Sidebar_right_question')}}</h4>
            </div>
            <div class="right-sec-header">
                {{__('message.Sidebar_right_content')}}
            </div>
            <div class="side-accord">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne"
                             aria-expanded="false" aria-controls="collapseOne">
                            {{__('message.How_to_create_LinkedIn_account')}}
                            <span class="acc-arrow">
									<em class="fa fa-angle-right"></em>
								</span>
                        </div>

                        <div id="collapseOne" class="collapse " aria-labelledby="headingOne"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                {{__('message.How_to_create_LinkedIn_account_content')}}
                                <a href="https://vimeo.com/310342097" target="_blank"> Click here </a> to watch the
                                video

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo"
                             aria-expanded="false" aria-controls="collapseTwo">
                            {{__('message.How_to_create_Twitter_business_account')}}
                            <span class="acc-arrow">
									<em class="fa fa-angle-right"></em>
								</span>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                {{__('message.How_to_create_Twitter_business_account_content')}}
                                <a href="https://vimeo.com/310342245"
                                   target="_blank"> {{__('message.Click_here')}} </a> {{__('message.to_watch_the_video')}}
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree"
                             aria-expanded="false" aria-controls="collapseThree">
                            {{__('message.How_to_create_Facebook_business_account')}}
                            <span class="acc-arrow">
									<em class="fa fa-angle-right"></em>
								</span>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                {{__('message.How_to_create_Facebook_business_account_content')}}

                                <a href="https://vimeo.com/310341968"
                                   target="_blank"> {{__('message.Click_here')}} </a> {{__('message.to_watch_the_video')}}


                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <img src="{{asset('')}}images/chart-image.png" height="100%" width="100%">
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour" data-toggle="collapse" data-target="#collapseFour"
                             aria-expanded="false" aria-controls="collapseFour">
                            {{__('message.How_to_create_Instagram_business_account')}}
                            <span class="acc-arrow">
									<em class="fa fa-angle-right"></em>
								</span>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                {{__('message.How_to_create_Instagram_business_account_content')}}

                                <a href="https://vimeo.com/310342044"
                                   target="_blank"> {{__('message.Click_here')}} </a> {{__('message.to_watch_the_video')}}


                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingFive" data-toggle="collapse" data-target="#collapseFive"
                             aria-expanded="false" aria-controls="collapseFive">
                            {{__('message.How_to_attract_the_attention_of_your_target_audience')}}
                            <span class="acc-arrow">
									<em class="fa fa-angle-right"></em>
								</span>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                             data-parent="#accordionExample">
                            <div class="card-body">
                                {{__('message.How_to_attract_the_attention_of_your_target_audience_content')}}
                                <a href="https://bonconline.com/en/blog/online-marketingtrends.html"
                                   target="_blank"> {{__('message.Click_here')}} </a>
                            </div>
                        </div>
                    </div>
                </div>
                {{--                <div class="accordion" id="accordionExample">--}}
                {{--                    <div class="card">--}}
                {{--                        <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">--}}
                {{--                            {{__('message.Help_Me_Post')}}--}}
                {{--                            <span class="acc-arrow">--}}
                {{--									<em class="fa fa-angle-right"></em>--}}
                {{--								</span>--}}
                {{--                        </div>--}}

                {{--                        <div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">--}}
                {{--                            <div class="card-body">--}}
                {{--                                {{__('message.Help_Me_Post_Content')}}--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                    <div class="card">--}}
                {{--                        <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">--}}
                {{--                            {{__('message.Do_not_know_schedule_post')}}--}}
                {{--                            <span class="acc-arrow">--}}
                {{--									<em class="fa fa-angle-right"></em>--}}
                {{--								</span>--}}
                {{--                        </div>--}}
                {{--                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">--}}
                {{--                            <div class="card-body">--}}
                {{--                                {{__('message.Do_not_know_schedule_post_content')}}--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                    <div class="card">--}}
                {{--                        <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">--}}
                {{--                            {{__('message.Need_Help_with_Settings')}}--}}
                {{--                            <span class="acc-arrow">--}}
                {{--									<em class="fa fa-angle-right"></em>--}}
                {{--								</span>--}}
                {{--                        </div>--}}
                {{--                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">--}}
                {{--                            <div class="card-body">--}}
                {{--                                {{__('message.Need_Help_with_Settings_content')}}--}}
                {{--                            </div>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>

        </section>
    </div>

    <div class="modal " id="postEditModal" tabindex="-1" role="dialog" aria-labelledby="postEditModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-post-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <span>Edit Photo</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body post-modal-body d-flex">
                    <div class="modal-sidebar d-flex">
                        <div class="post-menu-tab">
                            <button class="post-tablinks" onclick="openCnt(event, 'text')"><img src="{{asset('images/text.png')}}"></button>
                            <button class="post-tablinks" onclick="openCnt(event, 'filter')"><img src="{{asset('images/post_filter.png')}}"></button>
                            <button class="post-tablinks" onclick="openCnt(event, 'post-templates')"><img src="{{asset('images/template.png')}}"></button>
                            <button class="post-tablinks" onclick="openCnt(event, 'post-upload')"><img src="{{asset('images/post_upload.png')}}"></button>
                        </div>

                        <div id="text" class="post-tabcontent active">
                            <h3>Text</h3>
                            <p>Add Text Here</p>

                        </div>

                        <div id="filter" class="post-tabcontent">
                            <div class="filters">
                                <ul>
                                    <li>
                                        <input type="button" class="img_action" id="neg" value="Negative">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="blr" value="Blur">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="brg" value="Brighten">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="clr" value="Colorize">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="cntr" value="Contrast">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="edgd" value="Edge Detect">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="gray" value="Grayscale">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="mean" value="Mean">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="seleb" value="Selective Blur">
                                    </li>
                                    <li>
                                        <input type="button" class="img_action" id="smth" value="Smoothen">
                                    </li>
                                </ul>

                            </div>

                        </div>

                        <div id="post-templates" class="post-tabcontent">
                            <div class="templates-div" id="designs">
                                <input type="text" class="search-inp" placeholder="Search Template">
                                <div class="templates-list">
                                    <div class="cat-title d-flex">
                                        <div class="w-50 d-flex">
                                            <span>Category 1</span>
                                        </div>
                                        <div class="w-50 d-flex">
                                            <a class="see_all" href="#">See all</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="template-img">
                                        <a href="#">
                                            <img src="{{asset('images/frame/frame1.png')}}" class="design ">
                                        </a>
                                        <a href="#">
                                            <img src="{{asset('images/frame/frame1.png')}}" class="design ">
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="templates-list">
                                    <div class="cat-title d-flex">
                                        <div class="w-50 d-flex">
                                            <span>Category 2</span>
                                        </div>
                                        <div class="w-50 d-flex">
                                            <a class="see_all" href="#">See all</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="template-img">
                                        <a href="#">
                                            <img src="{{asset('images/frame/frame1.png')}}" class="design ">
                                        </a>
                                        <a href="#">
                                            <img src="{{asset('images/frame/frame1.png')}}" class="design ">
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="templates-list">
                                    <div class="cat-title d-flex">
                                        <div class="w-50 d-flex">
                                            <span>Category 3</span>
                                        </div>
                                        <div class="w-50 d-flex">
                                            <a class="see_all" href="#">See all</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="template-img">
                                        <a href="#">
                                            <img src="{{asset('images/frame/frame1.png')}}" class="design ">
                                        </a>
                                        <a href="#">
                                            <img src="{{asset('images/frame/frame1.png')}}" class="design ">
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="templates-list">
                                    <div class="cat-title d-flex">
                                        <div class="w-50 d-flex">
                                            <span>Category 4</span>
                                        </div>
                                        <div class="w-50 d-flex">
                                            <a class="see_all" href="#">See all</a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="template-img">
                                        <a href="#">
                                            <img src="{{asset('images/frame/frame1.png')}}" class="design ">
                                        </a>
                                        <a href="#">
                                            <img src="{{asset('images/frame/frame1.png')}}" class="design ">
                                        </a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="post-upload" class="post-tabcontent">
                            <div class="post-upload">
                                <div class="input-upload">
                                    <input type="file" class="upload-post">
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="post-model-content d-flex">
                        <div id="preview">
                            <div id="crop-area">
                                <img src="" id="profile-pic" />
                            </div>
                            <img src="" id="fg" data-design="0" />
                            <input type="hidden" name="new_uploaded_image" id="new_uploaded_image" value="">
                            <input type="hidden" name="active_image" id="active_image" value="">
                        </div>
                        <div class="editor_div">

                            <div id="editor">
                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Save</button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="baseURL" value="{{url('')}}">
@endsection

@push('styles')
    <link href="{{asset('')}}css/post/croppie.css" rel="stylesheet" async="async" />
    {{--    <link href="{{asset('')}}css/post/style.css" rel="stylesheet" async="async" />--}}
    <link href="{{asset('css/fileinput.css')}}" media="all" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">



    <style>
        #your-photo { visibility: hidden; }
        .file-caption-main {display: none}
        input[type="checkbox"][id^="cb"] {
            display: none;
        }
        .krajee-default .file-upload-indicator {
            float: left;
            margin-top: 1px;
            width: 16px;
            height: 16px;
        }
        .krajee-default.file-preview-frame .kv-file-content{
            width: 75px;
            height: 75px;
        }
        .krajee-default .file-footer-caption{ font-size: 9px; margin-bottom: 0;}
        .file-footer-buttons .btn{ border: none; font-size: 12px; padding:  0 10px;}
        .file-preview{ border: none; border-bottom: 2px solid #f6f6f6; border-radius: 0;}
        .krajee-default .file-upload-indicator{ display: none}

        label {
            /*border: 1px solid #fff;*/
            padding: 0px;
            display: block;
            position: relative;
            margin: 0px;
            cursor: pointer;
        }

        label:before {
            background-color: white;
            color: white;
            content: " ";
            display: block;
            border-radius: 50%;
            border: 1px solid #8EC451;
            position: absolute;
            top: 26px;
            left: 0px;
            width: 15px;
            height: 15px;
            text-align: center;
            line-height: 15px;
            font-size: 8px;
            transition-duration: 0.4s;
            transform: scale(0);
            z-index: 9;
        }

        label img {
            height: 50px;
            width: 50px;
            transition-duration: 0.2s;
            transform-origin: 50% 50%;
            border-radius: 100%;
            filter: grayscale(1);
        }

        :checked + label {
            border-color: #ddd;
        }

        :checked + label:before {
            content: "✓";
            background-color: #8EC451;
            transform: scale(1);

        }

        :checked + label img {
            transform: scale(0.9);
            z-index: -1;
            filter: grayscale(0);
        }
        .select2-container--default .select2-selection--single {
            width: 375px;
            padding: 2px;
            height: 35px;
        }

        .select2-container--open .select2-dropdown--below {
            width: 375px !important;
        }
        .select2-container{
            width: 100% !important;
        }
        .select2-container--default .select2-selection--single{
            width: 100%;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow{
            right: 10px;
        }
        .overlays{
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(255, 255, 255, 0.8);
            z-index: 5;
            cursor: pointer;

        }
        .text{
            position: absolute;
            top: 88%;
            left: 46%;
            font-size: 22px;
            color: #131212;
            transform: translate(-47%,-16%);
            -ms-transform: translate(-50%,-50%);
            font-weight: 600;
            width: max-content;
        }
    </style>
@endpush
@push('scripts')
    <script src="{{asset('')}}js/post/croppie.min.js" async="async"></script>
    <script src="{{asset('')}}js/post/app.js" async="async"></script>
    {{--    <script src="{{asset('js/post/fileinput.js')}}"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/js/plugins/piexif.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/js/plugins/sortable.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.1/js/plugins/purify.min.js" type="text/javascript"></script>
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>--}}
    {{--    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>--}}
    <link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
    {{--    <script src='https://cdn.rawgit.com/pguso/jquery-plugin-circliful/master/js/jquery.circliful.min.js'></script>--}}
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>



    <script type="text/javascript">
        $(document).ready(function() {
            $(".file-input").show();
            // $('#postEditModal').modal('show');


            // $("#your-photo").fileinput({
            //     'showUpload':false,
            //     'dropZoneEnabled': false,
            //     'allowedFileTypes':['image'],
            //     'allowedFileExtensions':['jpg', 'png'],
            //     'autoReplace':false,
            //     'maxFileCount':0
            // });
            // $("#your_photo").fileinput({'showUpload':false, 'previewFileType':'any', 'dropZoneEnabled': false});

        });

        $(".alert").delay(3000).slideUp(800, function() {
            $(this).alert('close');
        });
        $('.channel-chk').click(function(){
            var sel = $(this);
            var uuid = sel.data('id');

            var chacked =sel.is(':checked');

            if (chacked){
                // $('#date-time-'+sel.data('id')).removeClass('d-none');
                $('#date-time-'+sel.data('id')).removeClass('disable-block');
            }else {
                // $('#date-time-'+sel.data('id')).addClass('d-none');
                $('#date-time-'+sel.data('id')).addClass('disable-block');
            }

        });
        $('#your-photo-btn').click(function(){
            $('#your-photo').click();
        });

        $('.cal-ico').click(function(){
            var sel = $(this);
            // console.log('#'+sel.attr('for'));
            $('#'+sel.attr('for')).focus();
        });
        $("#your-photo").on("change", handleFileSelect);
        var selDiv = "";
        // var selDivM="";
        var storedFiles = [];
        function handleFileSelect(e) {

            var files = e.target.files;
            var filesArr = Array.prototype.slice.call(files);
            var device = $(e.target).data("device");
            filesArr.forEach(function(f) {

                if (!f.type.match("image.*")) {
                    return;
                }
                storedFiles.push(f);

                var reader = new FileReader();
                reader.onload = function(e) {
                    console.log(e.target.fileName);
                    var html = "<div class='file-preview-frame krajee-default  kv-preview-thumb'>" +
                        "<div class='kv-file-content'>" +
                        " <img src=\"" + e.target.result + "\" class='file-preview-image kv-preview-data' style='width:auto;height:auto;max-width:100%;max-height:100%;'>" +
                        "<div class=\"overlay-file\"></div>" +
                        "<div class=\"button-file button-file-remove\" data-file='"+f.name+"' onclick=\"removeImg($(this))\" title ='remove'>" +
                        "<a href='#'> <img src='{{asset('')}}images/close.svg'> </a></div>" +
                        {{--"<div class=\"button-file button-file-edit edit-post-i \" onclick=\"editPost($(this))\" data-image='"+e.target.result+"' title='edit'>" +--}}
                        {{--"<a href='#'> <img src='{{asset('')}}images/brush.svg'> </a></div>" +--}}
                        "</div></div>";
                    $("#selectedFiles").append(html);
                    // console.log(e.target.result );
                }
                reader.readAsDataURL(f);
                console.log(f)
            });

        }
        function removeImg(selector){
            var file = selector.data("file");
            var id = selector.data('img-id');

            if(id != undefined){
                var oldids = $('#delete_image_id').val();
                if(oldids == '') {
                    $('#delete_image_id').val(id);
                }else {
                    $('#delete_image_id').val(oldids + ',' + id);
                }
            }
            for (var i = 0; i < storedFiles.length; i++) {
                if (storedFiles[i].name === file) {
                    storedFiles.splice(i, 1);
                    break;
                }
            }
            selector.closest('.file-preview-frame').remove();
        }

        $("#submit").on("click", handleForm);

        function handleForm(e) {
            e.preventDefault();
            var data = new FormData();

            var poData = $('#post-form').serializeArray();

            for (var i = 0, len = storedFiles.length; i < len; i++) {
                data.append('your_photo[]', storedFiles[i]);
            }

            for (var i=0; i<poData.length; i++)
                data.append(poData[i].name, poData[i].value);
            console.log(data);
            $.ajax({
                url: '{{url("update/concept/data")}}',
                type: 'POST',
                // dataType: 'json',
                data: data,
                processData: false,
                cache: false,
                contentType: false,
                headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                success: function (response) {
                    if(response.status){
                        window.location.href = response.data.redirect_url;
                    }
                }
            });
        }
        $('#save').on('click',HandleCocept);

        function HandleCocept(e) {
            e.preventDefault();
            var data = new FormData();
            var poData = $('#post-form').serializeArray();
            for (var i = 0, len = storedFiles.length; i < len; i++) {
                data.append('your_photo[]', storedFiles[i]);
            }

            data.append('schedule', '1');
            for (var i=0; i<poData.length; i++)
                data.append(poData[i].name, poData[i].value);

            $.ajax({
                url: '{{url("/update/concept/data")}}',
                type: 'POST',
                // dataType: 'json',
                data: data,
                processData: false,
                cache: false,
                contentType: false,
                headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                success: function (response) {
                    if(response.status){
                        window.location.href = response.data.redirect_url;
                    }
                }
            });

        }


        function editPost(selector){
            var dataSrc = selector.data('image');
            // console.log(dataSrc);
            $('#profile-pic').attr('src', dataSrc);
            $('#new_uploaded_image').val(dataSrc);
            $('#active_image').val(dataSrc);
            updatePreview(dataSrc);
            $('#postEditModal').modal('show');
        }


        $('form').on('focus', 'input[type=number]', function (e) {
            $(this).on('wheel.disableScroll', function (e) {
                e.preventDefault()
            })
        })
        $('form').on('blur', 'input[type=number]', function (e) {
            $(this).off('wheel.disableScroll')
        })
        $('#text_select').on('click',function(){

            $('#BoncModalText').modal('show');
        });
        $("#search").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: "POST",
                    // contentType: "application/json; charset=utf-8",
                    headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                    url: "{{url('bonc/text/search')}}",
                    data: {search:$("#search").val()},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        response(data);
                    },
                    failure: function (response) {
                        alert("No Match");
                    }
                });
            },
            appendTo: "#BoncModalText",
            select: function (e, i) {
                console.log(i);
                $("#text").val(i.item.value);
                $('#BoncModalText').modal('hide');
            },

        });
        $('#BoncModalText').on('hidden.bs.modal', function () {
            $(this).find(':input').val('');
        })

        $('.dateSelect').datetimepicker({
            format:'d/m/Y',
            timepicker:false,
            minDate:new Date(),
            startDate: new Date(),
            todayButton:false,
            scrollMonth : false
        });
        $('.timeSelect').datetimepicker({
            format:'H:i',
            datepicker:false,
            scrollTime:true,
            step:15,

        });
        {{--   CKEDITOR.replace( 'editor1' );--}}
        function openCnt(evt, cityName) {
            // Declare all variables
            var i, tabcontent, tablinks;

            // Get all elements with class="tabcontent" and hide them
            tabcontent = document.getElementsByClassName("post-tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            // Get all elements with class="tablinks" and remove the class "active"
            tablinks = document.getElementsByClassName("post-tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }

            // Show the current tab, and add an "active" class to the link that opened the tab
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

    </script>
@endpush
