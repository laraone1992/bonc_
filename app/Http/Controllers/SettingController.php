<?php

namespace App\Http\Controllers;

use App\Base;
use App\User;
use App\Channel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\UserImages;


class SettingController extends Controller
{
    protected $base;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Base $base)
    {
//        if(Auth::user() == null){
////            return redirect('register');
//        }
//        $this->middleware('auth');
        $this->base = $base;
    }
    public function index(Request $request)
    {
        if(Auth::user() == null){
            return redirect('login');
        }
        $userDetails = Auth::user();
        $userlng=User::select('id','language')->where('email',$userDetails->email)->first();

        $userLogo=UserImages::select('id','image')->where('u_uuid',Auth::user()->org_uuid)->get()->toArray();
        foreach($userLogo as $key=>$user){
//            $userLogo[$key]['image']="https://".env('AWS_BUCKET').".s3.".env('AWS_DEFAULT_REGION').".amazonaws.com/logo/".$user['image'];
            $userLogo[$key]['image']=asset('storage/images/logo/'.$user['image']);
        }
        $channelList=[];
        $channels = $this->base->request('get','channels');
        foreach($channels['data'] as $channel){
            $channelList[] = $channel['type'];
            $isPresent = Channel::where('channel_id',$channel['uuid'])->where('user_id',$userlng->id)->count();
            if($isPresent == 0){
                if($channel['type'] == 'facebook_profile'){
                    continue;
                }
                else{
                    Channel::create(['user_id'=>$userlng->id,
                        'channel_name'=>$channel['type'],
                        'channel_id'=>$channel['uuid'],
                        'ch_user_name'=>$channel['name'],
                        'status'=>'1',
                        'image'=>$channel['profile_picture_url'],]);
                }
                
            }
        }
        $isInsta= Channel::where('channel_name','instagram')->where('user_id',$userlng->id)->count();
        if($isInsta == 0){
            Channel::create([
                'user_id' => $userlng->id,
                'channel_name' => 'instagram',
                'channel_id' => '', 'ch_user_name'=>'',
                'status' => '0',
                'image' => ''
            ]);
        }
        $channelListData=Channel::select('channel_name','channel_id','ch_user_name','status','image')
            ->where('user_id',$userlng->id)
            ->get()
            ->toArray();
       
        
        
            
        $linkOuth = [];
        $linkdin = $this->base->request('get','linkedin/oauth');
        $facebook = $this->base->request('get','facebook/oauth');
        $twitter = $this->base->request('get','twitter/oauth');

        $linkOuth['linkdin'] = $linkdin['redirect_url'];
        $linkOuth['facebook'] = $facebook['redirect_url'];
        $linkOuth['twitter'] = $twitter['redirect_url'];
        $count=count($userLogo);
        $subcription=$this->base->request('get','orderitems');
        $subcriptionType=isset($subcription['data'][0]['product']['name']) ? $subcription['data'][0]['product']['name'] :'FREE';
        return view('setting', compact('userDetails', 'channels', 'channelList','channelListData','linkOuth','userLogo','count','userlng','subcriptionType'));

    }
    public function subcriptionUpdate(Request $request){
       $params = json_encode(['subscription_name' => $request->subscription_name, 'country_code' => $request->country_code]);
//       dd($params);
       $update = $this->base->postRequest('/upgrade', $params);
//       dd($update);
       return response()->json(['status'=>200,'data'=>$update]);
    }
    public function savePersonalDetail(Request $request)
    {
//        dd($request->all());
        $params = json_encode(['firstName' => $request->f_name, 'lastName' => $request->l_name]);
        $update = $this->base->putRequest('/user', $params);
        if (isset($update['status']) && $update['status'] ==201){

        }
        $data=\App\User::where('email',$request->email)->update(array('first_name' => $request->f_name,'last_name'=>$request->l_name
                ,'email'=>$request->email,'language'=>$request->language));
        if($request->has('logo')) {
            $postImage = $this->thumbnailImage($request, 'logo', 'logo');

            foreach ($postImage['imagename'] as $k => $image) {
                $upload_Photo = new UserImages();
                $upload_Photo->user_id = 0;
                $upload_Photo->u_uuid = Auth::user()->org_uuid;
                $upload_Photo->image = $image;
                $upload_Photo->save();
            }
        }

        return redirect()->route('locale', [($request->language == 'en')?'en' :'du']);
    }
    public function deleteImage($id,Request $request){

        $deleteImage=UserImages::where('id',$id)->delete();


        return response()->json([
            'success' => 'Record deleted successfully!'
        ]);
    }

    public function disconnectAccount(Request $request){
        if($request->has('uuid')){
        $disconnectAccount = $this->base->request('delete','channels/'.$request->get('uuid'));
        
        $channelDisconnect= Channel::where('channel_id',$request->get('uuid'))->delete();
        }
        if($request->has('channel_id')){
           $channelDisconnect= Channel::where('channel_id',$request->get('channel_id'))->update(array('status'=>'0'));
        }
        return response()->json([
            'success' => 'Disconnected successfully!'
        ]);
    }
    public function reConnectAccount(Request $request){
         if($request->has('uuid')){
             
             $channelReconnect= Channel::where('channel_id',$request->get('uuid'))->update(array('status'=>'1'));
         }
        return response()->json([
            'success' => 'Re-connected successfully!'
        ]);
    }
    public function addBrand(Request $request){
        
        $userLogo=UserImages::select('id','image')->where('u_uuid',Auth::user()->org_uuid)->get()->toArray();
        foreach($userLogo as $key=>$user){
//            $userLogo[$key]['image']="https://".env('AWS_BUCKET').".s3.".env('AWS_DEFAULT_REGION').".amazonaws.com/logo/".$user['image'];
            $userLogo[$key]['image']=asset('storage/images/logo/'.$user['image']);
        }
         $count=count($userLogo);
//          $user = $this->base->request('get','/organizations/'.Auth::user()->org_uuid.'/users');
//          dd($user);
        return view('addBrand', compact('userLogo','count'));
    }
    
    public function viewBrand(Request $request){
        
         return view('viewBrand');
    }
    public function memberManage(Request $request){
        
          return view('memberManage');
    }
    public function addMember(Request $request){
        
       $data=$request->all();
        
       $user = new User();
       $user->role=$data['role'];
       $user->username=$data['username'];
       $user->password=$data['password'];
       $user->email=$data['email'];
       $user->save();
       
        return response()->json([
            'success' => 'Member add successfully!'
        ]);
        
    }
}
