@extends('layouts.layout')
@section('title','Concept')

@section('content')
    <!-- Content Start -->
    <div id="content" class="d-flex">
        <!-- =========================================
        Left Section
        ========================================== -->
        <section class="left-section sidebar">

            {{--            <div class="recent-post">--}}
            {{--                <div class="recent-post-head d-flex align-items-center justify-content-between">--}}
            {{--                    <h3 class="mb-0">{{__('message.Recent_Posts')}}</h3>--}}

            {{--                </div>--}}
            {{--            </div>--}}
            <div class="recent-post recent-post-cnt">
                <div class="recent-post-body scroll-cust">
                    <div class="recent-post-block">
                        <div class="thumb">
                            <img src="{{asset('')}}images/img1.png" alt="">
                        </div>
                        <div class="caption">
                            <div class="cap-heading">
                                <h4> {{__('message.Need help to get started')}}</h4>
                                <p>{{__('message.To get you started')}} {{__('message.Starters_package')}}{!! __('message.We will create Business')!!}
                                <div class="help-form">
                                    <form action="{{url('need-help')}}" method="get" class="form" >
                                        <div class="row mt-3">
                                            <div class="col-md-3">
                                                <label class="">Name</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="cntName" required>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-3">
                                                <label class="">Phone</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control " name="cntPhone" required>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-3">
                                                <label class="">Email</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="email" class="form-control" name="cntEmail" required>
                                            </div>
                                        </div>
                                        <div class="row mt-3">
                                            <div class="col-md-3">

                                            </div>
                                            <div class="col-md-9">
                                                <input type="submit" class="btn btn-secondary help-form-send" name="cnt-submit" id="cnt-submit" value="Send">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                {{--{{__('message.Click_here')}}</a> {{__('message.consultants')}}--}}
                                </p>
                            </div>
                            {{--                            <div class="post-likes-wrap overlays">--}}
                            {{--                                <div class="text">Coming Soon..!</div>--}}
                            {{--                                <div class="post-likes d-flex align-items-center justify-content-between">--}}
                            {{--                                    <div class="d-flex align-items-center">--}}
                            {{--                                        <img src="{{asset('')}}images/facebook.svg" alt="" class="analytics">--}}
                            {{--                                        <div class="like-counter d-flex align-items-center">--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/like-icon.png" alt="">171</span>--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/comment-icon.png" alt="">1</span>--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/share-icon.png" alt="">102</span>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <span class="cursor-pointer">--}}
                            {{--											<img src="{{asset('')}}images/ic/upload-icon.png" alt="">--}}
                            {{--										</span>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="post-likes d-flex align-items-center justify-content-between">--}}
                            {{--                                    <div class="d-flex align-items-center">--}}
                            {{--                                        <img src="{{asset('')}}images/twitter.svg" alt="" class="analytics">--}}
                            {{--                                        <div class="like-counter d-flex align-items-center">--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/t-comment-icon.png" alt="">1</span>--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/t-share-icon.png" alt="">30</span>--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/icon-heart.png" alt="">51</span>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <span class="cursor-pointer">--}}
                            {{--											<img src="{{asset('')}}images/ic/upload-icon.png" alt="">--}}
                            {{--										</span>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="post-likes d-flex align-items-center justify-content-between">--}}
                            {{--                                    <div class="d-flex align-items-center">--}}
                            {{--                                        <img src="{{asset('')}}images/linkedin.svg" alt="" class="analytics">--}}
                            {{--                                        <div class="like-counter d-flex align-items-center">--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/like-icon.png" alt="">171</span>--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/comment-icon.png" alt="">1</span>--}}
                            {{--                                            <span><img src="{{asset('')}}images/ic/share-icon.png" alt="">102</span>--}}
                            {{--                                        </div>--}}
                            {{--                                    </div>--}}
                            {{--                                    <span class="cursor-pointer">--}}
                            {{--											<img src="{{asset('')}}images/ic/upload-icon.png" alt="">--}}
                            {{--										</span>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}
                        </div>
                    </div>
                    <div class="recent-post-block">
                        <div class="thumb">
                            <img src="{{asset('')}}images/ic/image-2.jpg" alt="">
                        </div>
                        <div class="caption">
                            <div class="cap-heading">
                                <h4>{{__('message.Your analytics on one page')}}</h4>
                                <p>{{__('message.Recent_Post_Content')}}</p>
                            </div>
                            <div class="post-likes-wrap overlays">
                                <div class="coming-soon-text">{{__('message.Coming Soon..!')}}</div>
                                <div class="post-likes d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center">
                                        <img src="{{asset('')}}images/facebook.svg" alt="" class="analytics">
                                        <div class="like-counter d-flex align-items-center">
                                            <span><img src="{{asset('')}}images/ic/like-icon.png" alt="">171</span>
                                            <span><img src="{{asset('')}}images/ic/comment-icon.png" alt="">1</span>
                                            <span><img src="{{asset('')}}images/ic/share-icon.png" alt="">102</span>
                                        </div>
                                    </div>
                                    <span class="cursor-pointer">
											<img src="{{asset('')}}images/ic/upload-icon.png" alt="" style="opacity: 0.3">
										</span>
                                </div>
                                <div class="post-likes d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center">
                                        <img src="{{asset('')}}images/twitter.svg" alt="" class="analytics">
                                        <div class="like-counter d-flex align-items-center">
                                            <span><img src="{{asset('')}}images/ic/t-comment-icon.png" alt="">1</span>
                                            <span><img src="{{asset('')}}images/ic/t-share-icon.png" alt="">30</span>
                                            <span><img src="{{asset('')}}images/ic/icon-heart.png" alt="">51</span>
                                        </div>
                                    </div>
                                    <span class="cursor-pointer">
											<img src="{{asset('')}}images/ic/upload-icon.png" alt="" style="opacity: 0.3">
										</span>
                                </div>
                                <div class="post-likes d-flex align-items-center justify-content-between">
                                    <div class="d-flex align-items-center">
                                        <img src="{{asset('')}}images/linkedin.svg" alt="" class="analytics">
                                        <div class="like-counter d-flex align-items-center">
                                            <span><img src="{{asset('')}}images/ic/like-icon.png" alt="">171</span>
                                            <span><img src="{{asset('')}}images/ic/comment-icon.png" alt="">1</span>
                                            <span><img src="{{asset('')}}images/ic/share-icon.png" alt="">102</span>
                                        </div>
                                    </div>
                                    <span class="cursor-pointer">
											<img src="{{asset('')}}images/ic/upload-icon.png" alt="" style="opacity: 0.3">
										</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- =========================================
        Content Section
        ========================================== -->
        <section class="content-section section-schedule flex-fill">

            @if(Session::has('success'))
                <p class="alert alert-success">{{ Session::get('success') }}</p>
            @endif
            @if(Session::has('error'))
                <p class="alert alert-danger">{{ Session::get('error') }}</p>
            @endif
                <div class="page-header">{{__('message.CONCEPT_POST')}}</div>
            <div class="card mx-100">
{{--                <div class="card-head">--}}
{{--                    <ul class="nav nav-tabs">--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link active"data-toggle="tab" href="#summary" role="tab" aria-controls="summary" aria-selected="true">{{__('message.Summary')}}</a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" data-toggle="tab" href="#post" role="tab" aria-controls="post" aria-selected="false">{{__('message.Full_Post')}}</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
                <div class="card-body py-0">
                    <div class="tab-content">
                        <div class="tab-pane fade show  active" id="post" role="tabpanel" aria-labelledby="post-tab">
                            <div class="scroll-cust">
                                @if(count($concept_) == 0)
                                    <div class="no-post-summ summary-block d-flex align-items-center justify-content-center mt-3">
                                        No concept post available
                                    </div>
                                @else
                                @foreach($concept_ as $pk => $post)
                                    <div class="full-day-block">
                                        <div class="full-post-group">
                                            <div class="full-post-block concept-post-block d-md-flex align-items-center justify-content-between" >
                                                <div class="d-flex align-items-center">
                                                    <span class="full-post-image"><img src="{{$post['cover']}}" onerror="this.onerror=null;this.src='{{asset('images/photo.svg')}}';" alt=""></span>
                                                </div>

                                                <div class="full-post-content">
                                                    <div class="full-post-desc">
                                                        @if(strlen($post['content']) > 300)
                                                            {{substr($post['content'], 0, 297)." ..."}}
                                                        @else
                                                            {{$post['content']}}
                                                        @endif
                                                    </div>


                                                    <div class="full-post-footer">
                                                        <div class="btn-sec ">
                                                            <div class="full-post-soc-btns d-flex">
                                                                <ul class="list-unstyled social-full-post d-flex align-items-center m-0 pt-7">
                                                                    @if(in_array('facebook_page',$post['channels']))
                                                                        <li><a href="#"><img src="{{asset('')}}images/facebook.svg" alt=""></a></li>
                                                                    @else
                                                                    <!-- <li><a href="#"><img src="{{asset('')}}images/ic/grey-facebook.png" alt=""></a></li> -->
                                                                    @endif

                                                                    @if(in_array('linkedin_profile',$post['channels']))
                                                                        <li><a href="#"><img src="{{asset('')}}images/twitter.svg" alt=""></a></li>
                                                                    @else
                                                                    <!-- <li><a href="#"><img src="{{asset('')}}images/ic/grey-twitter.png" alt=""></a></li> -->
                                                                    @endif

                                                                    @if(in_array('twitter',$post['channels']))
                                                                        <li><a href="#"><img src="{{asset('')}}images/linkedin.svg" alt=""></a></li>
                                                                    @else
                                                                    <!-- <li><a href="#"><img src="{{asset('')}}images/ic/grey-linkedin.png" alt=""></a></li> -->
                                                                    @endif

                                                                    @if(in_array('instagram',$post['channels']))
                                                                        <li><a href="#"><img src="{{asset('')}}images/instagram.svg" alt=""></a></li>
                                                                    @else
                                                                    <!-- <li><a href="#"><img src="{{asset('')}}images/ic/grey-instagram.png" alt=""></a></li> -->
                                                                    @endif
                                                                </ul>
                                                            </div>
                                                            <div class="full-post-soc-btns">
                                                                <div class="row">
                                                                    <div class="col-6">
{{--                                                                         <span class="full-post-desc">This post was created by--}}

{{--                                                                    </span><br>--}}
{{--                                                                        <div style="margin-top: 7px;">{{\Illuminate\Support\Facades\Auth::User()->name}}</div>--}}
                                                                        <?php
                                                                        $ndate = \Carbon\Carbon::parse($post['created_at'])->locale('en')->formatLocalized('%A,%B,%e');
                                                                        $ddate = explode(',',$ndate);


                                                                        ?>
                                                                        <div style="margin-top: 7px;">{{__('message.'.$ddate[0]).', '. $ddate[2].' '. __('message.'.$ddate[1]) }}</div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                    <ul class="list-unstyled action-list d-flex m-0 right-fix">

                                                                        <li class="pt-7"><a href="{{url('edit/concept',$post['id'])}}"><img src="{{asset('')}}images/edit.svg" alt=""></a></li>
                                                                        <li class="pt-7"><a href="{{url('delete/concept',$post['id'])}}"><img src="{{asset('')}}images/delete.svg" alt=""></a></li>
                                                                        <li>
                                                                            <a href="{{url('confirm-concept',$post['id'])}}" class="btn btn-outline-secondary btn-share-now">{{__('message.Confirm')}}</a>
                                                                        </li>

                                                                    </ul>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================================
        Right Section
        ========================================== -->
        <section class="right-section sidebar scroll-cust">

            <div class="side-accord">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            {{__('message.Need_Help_with_Schedulling_Posts')}}
                            <span class="acc-arrow">
									<em class="fa fa-angle-right"></em>
								</span>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                {{__('message.Need_Help_with_Schedulling_Posts_content')}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--            <div class="start-chat d-flex align-items-center justify-content-between">--}}
            {{--                <span>Start Chatting</span>--}}
            {{--                <button type="button" class="btn btn-secondary">Click here</button>--}}
            {{--            </div>--}}
            <div class="thumbnail-group ">
                <div class="thumbnail-block">
                    <div class="thumb">
                        <img src="{{asset('')}}images/ic/image-1.jpg" alt="">
                    </div>
                    <div class="caption">
                        <h4>{{__('message.How_to_schedule_post')}}</h4>
                        <p>{{__('message.How_to_schedule_post_content')}}</p>
                        <a href="https://bonconline.com/en/blog/online-marketingtrends.html?fbclid=IwAR0sryOaPf0rgbZXKVJIEbTY3YP0zRpPvh-Fc8_0fIHS1X8FkZdS_00aLic" target="_blank">{{__('message.Read_more')}}</a>
                    </div>
                </div>

            </div>
        </section>
    </div>

@endsection

@push('styles')
    <link href="{{asset('')}}css/pignose.calendar.min.css" rel="stylesheet"><!-- pignose.calendar css -->
    <style>

    </style>
@endpush
@push('scripts')
    <script src="{{asset('')}}js/pignose.calendar.full.min.js"> </script><!-- pignose.calendar.full.min -->
    <script>
        $(function () {

            var locale = '{{ config('app.locale') }}';
            var langs='';
            if(locale=='en'){

                var langs='en';

            }
            else{
                var langs='nl';
            }
            $('#calendar').pignoseCalendar({

                'lang': langs,
                'click': function(event, context) {
                    // this is clicked button.
                    var this_ = $(this);

                    // event is general event object.
                    var selDate = this_[0].dataset.date;
                }
            });
        });

        $(".alert").delay(3000).slideUp(800, function() {
            $(this).alert('close');
        });
        $(document).ready(function (){
            $("#click").click(function (){
                $('html, body').animate({
                    scrollTop: $("#div1").offset().top
                }, 2000);
            });
        });


    </script>

@endpush
