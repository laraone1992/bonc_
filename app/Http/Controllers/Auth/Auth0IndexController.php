<?php

namespace App\Http\Controllers\Auth;


use App\User;
use Auth0\Login\Facade\Auth0;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use mysql_xdevapi\Exception;
use Illuminate\Support\Facades\Session;

class Auth0IndexController extends Controller
{
    /**
     * Redirect to the Auth0 hosted login page
     *
     * @return mixed
     */
    public function register()
    {
        if (Auth::user()){
            return redirect('home');
        }else {
            return view('auth/register');
        }
    }
    public function registerUser(Request $request)
    {
       $validator = \Validator::make($request->all(), [
           'firstName' => 'required|max:20',
           'lastName' => 'required|max:20',
           'password' => 'required|confirmed|min:6',
           'email' => 'required|string|email|max:255',
//           'phone' => 'required|max:10',
//           'username'=>'required',
       ]);

         if ($validator->fails()) {
             $errors = $validator->errors()->all();
//             dd($errors);
             return redirect()->back()->withErrors($errors);
         }
        try {


            $client = new Client();
            $requestOption = [];
            $requestMethod = 'POST';
            $requestUri = env('API_URL') . '/users';

            $requestOption['username'] = $request->email; // $request->username;
            $requestOption['password'] = $request->password;
            $requestOption['firstName'] = $request->firstName;
            $requestOption['lastName'] = $request->lastName;
            $requestOption['email'] = $request->email;
            $requestOption['phone'] = 00;//$request->phone;
            $requestOption['appSettings'] = 'test';
            $requestOption['locale'] = $request->language;
            $requestOption['organization'] = [
                'name' => $request->has('company') ? $request->company : 'master',
                'motto' => '',
                'branche' => '',
                'kvknr' => '',
                'appSettings' => '',
            ];
//        dd($requestOption, $requestUri, $requestMethod);
            $response = $client->post($requestUri, ['headers' => ['Accept' => 'application/json'], 'json' => $requestOption]);
            $user = new User();
            $user->first_name=$request->firstName;
            $user->last_name=$request->lastName;
            $user->email=$request->email;
            $user->company=$request->has('company') ? $request->company : $request->username;
            $user->username= $request->username;
            $user->language=$request->language;
            $user->subcription=0;
            $user->password=$request->password;
            $user->save();

            //Payment Here

            return redirect('/home');
//            $responseData = json_decode($response->getBody()->getContents(), true);
//            dd($response->getStatusCode());

        }
         catch (Exception $e){
            dd($e);
         }
         catch (BadResponseException $ex){
             $code = $ex->getCode();

             if ($code == '201'){
                 Session::flash('success', 'Registration successfull Please verify your email.');
                 return redirect()->back();
             }else if($code == '409'){
                 Session::flash('error', 'Email Already Exist');
                 return redirect()->back()->withInput();
             }else {
                dd($code, $ex);
             }
         }

//        dd($response,$responseData, $requestOption, $requestUri, $requestMethod);



    }

    public function login()
    {
        $authorize_params = [
//            'scope' => 'openid profile email',
        ];

        return \App::make('auth0')->login(null, null, $authorize_params);
    }

    /**
     * Log out of Auth0
     *
     * @return mixed
     */
    public function logout(Request $request)
    {
//        Auth::logout();
//        $request->session()->flush();
//        dd('here');
        Session::forget('locale');
        \Auth::logout();

        $logoutUrl = sprintf(
            'https://%s/logout?client_id=%s&redirect_uri=%s',
            env('AUTH0_DOMAIN'),
            env('AUTH0_CLIENT_ID'),
            env('APP_URL'));

        return  \Redirect::intended($logoutUrl);
    }
}
