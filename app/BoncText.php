<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoncText extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'b_text';
    protected $primaryKey = 'id';
}
