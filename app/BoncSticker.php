<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoncSticker extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bonc_sticker';
    protected $primaryKey = 'id';
}
