"use strict";
// Window Load
$(window).on("load",function(){
    $(".scroll-cust").mCustomScrollbar({
        theme:"dark"
    });
});

// Document Ready
$(document).ready(function(){
    $(".left-sidebutton").on('click',function(){
        $('#content').addClass("Leftbar-open");
    });
    $(".left-section .close-side").on('click',function(){
        $('#content').removeClass("Leftbar-open");
    });

    $(".right-sidebutton").on('click',function(){
        $('#content').addClass("Rightbar-open");
    });
    $(".right-section .close-side").on('click',function(){
        $('#content').removeClass("Rightbar-open");
    });
});