<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'posts';
    protected $primaryKey = 'id';

    public function postImage() {
        return $this->hasMany(PostImage::class, 'post_id');
    }
}
