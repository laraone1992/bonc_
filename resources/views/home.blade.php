<!doctype html>
<html lang="{{app()->getLocale()}}">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- vendor CSS  -->
{{--    <link rel="stylesheet" href="{{asset('css/vendor.css')}}">--}}
    <link rel="stylesheet" href="{{asset('css/fontawesome/fontawesome.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Roboto|Titillium+Web&display=swap" rel="stylesheet">


    <!-- Theme CSS  -->
    <link rel="stylesheet" href="{{asset('css/bonc.css')}}">

    <title>Bonc</title>
</head>

<body id="page-top" class="sidebar-toggled">

<!-- Page Wrapper -->
<div id="wrapper" class="fixed-header fixed-sidebar">


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">

            <!-- Topbar -->
            <nav class="navbar top-header navbar-expand header-bg-color topbar mb-4 static-top ">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3 d-sm-none">
                    <i class="fa fa-bars"></i>
                </button>

                <a class="navbar-brand" href="#"><img src="{{asset('')}}images/logo.png" alt="Bonc Logo" title="Bonc"></a>

                <ul class="navbar-nav mr-auto">

                    <!-- Nav Item -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link " href="#" id="">
                            <span class=" d-none d-lg-inline text-white small ">Post <img src="{{asset('images/arr-down.png')}}"></span>
                        </a>
                        <a class="nav-link " href="#" >
                            <span class=" d-none d-lg-inline text-white small ">Schedule</span>
                        </a>
                        <a class="nav-link " href="#" >
                            <span class=" d-none d-lg-inline text-white small ">Analyze</span>
                        </a>
                        <a class="nav-link " href="#" >
                            <span class=" d-none d-lg-inline text-white small ">Design</span>
                        </a>
{{--                        <a class="nav-link dropdown-toggle" href="#" id="" role="button" data-toggle="dropdown"--}}
{{--                           aria-haspopup="true" aria-expanded="false">--}}
{{--                            <span class=" d-none d-lg-inline text-white small ">Settings</span>--}}
{{--                        </a>--}}
{{--                        <a class="nav-link dropdown-toggle" href="#" id="" role="button" data-toggle="dropdown"--}}
{{--                           aria-haspopup="true" aria-expanded="false">--}}
{{--                            <span class=" d-none d-lg-inline text-white small ">Get Inspired</span>--}}
{{--                        </a>--}}
{{--                        <a class="nav-link dropdown-toggle" href="#" id="" role="button" data-toggle="dropdown"--}}
{{--                           aria-haspopup="true" aria-expanded="false">--}}
{{--                            <span class=" d-none d-lg-inline text-white small ">Let us help you</span>--}}
{{--                        </a>--}}
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <div class="d-flex no-block align-items-center p-3 mb-2 border-bottom">
                                <div class=""><img src="{{asset('')}}images/users/1.jpg" alt="user" class="rounded" width="80"></div>
                                <div class="ml-2">
                                    <h6 class="mb-0">User name</h6>
                                    <p class=" mb-0 text-muted">yourweb@gmail.com</p>
                                    <a href="javascript:void(0)"
                                       class="btn btn-sm btn-primary text-white mt-2 btn-rounded shadow-sm">View Profile</a>
                                </div>
                            </div>

                            <a class="dropdown-item" href="#">
                                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                Settings
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                Activity Log
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Logout
                            </a>
                        </div>
                    </li>

                </ul>
                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <li class="nav-item ">
                        <a class="nav-link " href="#" id="" role="button" >
                            <i class="fas">
                                <img src="{{asset('')}}images/help.png"/>
                            </i>
                        </a>

                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " href="#" id="" role="button" >
                            <i class="fas">
                                <img src="{{asset('')}}images/notification.png"/>
                            </i>
                        </a>

                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " href="#" id="" role="button" >
                            <i class="fas">
                                <img src="{{asset('')}}images/setting.png"/>
                            </i>
                        </a>

                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " href="#" id="" role="button" >
                            <i class="fas">
                                <img src="{{asset('')}}images/sep.png"/>
                            </i>
                        </a>

                    </li>

                    <!-- Nav Item - Setting -->

                    <!-- <div class="topbar-divider d-none d-sm-block"></div> -->

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle pl-cs" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <img class="img-profile rounded-circle mr-2" src="{{asset('')}}images/users/1.jpg">
                            <span class=" d-none d-lg-inline user_name">John Doe <img src="{{asset('images/arr-down.png')}}"></span>
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <div class="d-flex no-block align-items-center p-3 mb-2 border-bottom">
                                <div class=""><img src="{{asset('')}}images/users/1.jpg" alt="user" class="rounded" width="80"></div>
                                <div class="ml-2">
                                    <h6 class="mb-0">User name</h6>
                                    <p class=" mb-0 text-muted">yourweb@gmail.com</p>
                                    <a href="javascript:void(0)"
                                       class="btn btn-sm btn-primary text-white mt-2 btn-rounded shadow-sm">View Profile</a>
                                </div>
                            </div>

                            <a class="dropdown-item" href="#">
                                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                Settings
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                Activity Log
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Logout
                            </a>
                        </div>
                    </li>

                </ul>

            </nav>
            <!-- End of Topbar -->

            <!-- Begin Page Content -->
            <div class="container-fluid p-0">

                <!-- ============================================================== -->
                <!-- Top Dashboard cards &  Row  -->
                <!-- ============================================================== -->
                <div class="row ">
                    <div class="col-xl-3 col-md-3 mb-12">
                        <div class="panel panel-left  justify-content-center">
                            <div class="pt-1 pb-1 ">
                                <div class="align-items-center">

                                        <div class="panel-header" >
                                            <span>{{__('message.Recent_Posts')}}</span>
                                            <img src="{{asset('images/filter.png')}}" style="float: right; padding: 9px 25px">
                                        </div>

                                        <div class="recent-post">
                                            <div class="post-image">
                                                <img src="{{asset('/images/img.png')}}">
                                            </div>
                                            <div class="post-header">
                                                {{__('message.Your analytics on one page')}}
                                            </div>
                                            <div class="post-desc">
                                                Bonc shows you the results of your post in a super easy way.
                                            </div>
                                            <div class="post-feed">
                                                <table class="feed-tbl">
                                                    <tbody>
                                                        <tr>
                                                            <td class="disp-feed-ico"><img src="{{asset('images/fb.png')}}"></td>
                                                            <td class="feed-count"><img src="{{asset('images/dm.png')}}"></td>
                                                            <td class="feed-expand"><img src="{{asset('images/expand.png')}}"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="disp-feed-ico"><img src="{{asset('images/twitter.png')}}"></td>
                                                            <td class="feed-count"><img src="{{asset('images/dm.png')}}"></td>
                                                            <td class="feed-expand"><img src="{{asset('images/expand.png')}}"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="disp-feed-ico"><img src="{{asset('images/linkedin.png')}}"></td>
                                                            <td class="feed-count"><img src="{{asset('images/dm.png')}}"></td>
                                                            <td class="feed-expand"><img src="{{asset('images/expand.png')}}"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="recent-post">
                                            <div class="post-image">
                                                <img src="{{asset('/images/post2.png')}}">
                                            </div>
                                            <div class="post-header">
                                                Your analytics on one page
                                            </div>
                                            <div class="post-desc">
                                                Can you use some help making creative posts? Ask one of the Bonc Marketing Consultants to help you!
                                            </div>
                                            <div class="post-feed">
                                                <table class="feed-tbl">
                                                    <tbody>
                                                        <tr>
                                                            <td class="disp-feed-ico"><img src="{{asset('images/fb.png')}}"></td>
                                                            <td class="feed-count"><img src="{{asset('images/dm.png')}}"></td>
                                                            <td class="feed-expand"><img src="{{asset('images/expand.png')}}"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="disp-feed-ico"><img src="{{asset('images/twitter.png')}}"></td>
                                                            <td class="feed-count"><img src="{{asset('images/dm.png')}}"></td>
                                                            <td class="feed-expand"><img src="{{asset('images/expand.png')}}"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="disp-feed-ico"><img src="{{asset('images/linkedin.png')}}"></td>
                                                            <td class="feed-count"><img src="{{asset('images/dm.png')}}"></td>
                                                            <td class="feed-expand"><img src="{{asset('images/expand.png')}}"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6 mb-12">
                        <div class="main-body-content">

                            <ul class="chat">
                                <li class="left clearfix">
                                    <div class="chat-img ">
                                        <img src="{{asset('images/icon_blue.png')}}" alt="User Avatar" class="img-circle" />
                                    </div>
                                    <div class="chat-box ">
                                        <div class="question-1">
                                            <div class="header">
                                                <span>Great to see you! Lets post something online!</span>
                                            </div>
                                            <div class="que-input">
                                                <textarea class="chat-que-input" placeholder="Write your post here..." name="que-1" id="que-1"></textarea>
                                            </div>
                                            <div class="upload-sec">
                                                <label for="">
                                                    Add picture(s)
                                                </label>
                                                <button class="file-upload-btn"> <img src="{{asset('')}}images/upload.png"/>Your photo’s</button>
                                                <button class="file-upload-btn"><img src="{{asset('')}}images/upload.png"/>Bonc photo’s</button>
                                                <button class="file-upload-btn"><img src="{{asset('')}}images/upload.png"/>Bonc quotes</button>
                                                <input type="file" class="file-upload-ctrl" id="file-1" name="file-1" style="display: none">
                                                <input type="file" class="file-upload-ctrl" id="file-2" name="file-2" style="display: none">
                                                <input type="file" class="file-upload-ctrl" id="file-2" name="file-2" style="display: none">
                                            </div>
                                            <div class="post-sec">
                                                <div class="feed-list">

                                                    <label for=""> Post on </label>
                                                    <a href="#" class="fabebook-item">
                                                        <img src="{{asset('/images/users/usr.png')}}">
                                                        <span class="fb-badge"><img src="{{asset('/images/twitter.png')}}"></span>
                                                    </a>
                                                    <a href="#" class="fabebook-item">
                                                        <img src="{{asset('/images/users/usr.png')}}">
                                                        <span class="fb-badge"><img src="{{asset('/images/link.png')}}"></span>
                                                    </a>
                                                    <a href="#" class="fabebook-item">
                                                        <img src="{{asset('/images/users/usr.png')}}">
                                                        <span class="fb-badge"><img src="{{asset('/images/fb.png')}}"></span>
                                                    </a>
                                                    <a href="#" class="fabebook-item">
                                                        <img src="{{asset('/images/users/usr.png')}}">
                                                        <span class="fb-badge"><img src="{{asset('/images/insta.png')}}"></span>
                                                    </a>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </li>
                                <li class="left clearfix">
                                    <div class="chat-img ">
                                        <img src="{{asset('images/icon_blue.png')}}" alt="User Avatar" class="img-circle" />
                                    </div>
                                    <div class="chat-box que-rel">
                                        <div class="question-rel">
                                            <div class="header">
                                                <span>Choose Date and Time</span>
                                            </div>
                                            <div class="date-time-sec">
                                                <table class="tbl-date-time">
                                                    <tbody>
                                                        <tr>
                                                            <td class="feed-ico"><img src="{{asset('images/facebook_bb.png')}}"></td>
                                                            <td class="lable-date">Date</td>
                                                            <td class="input-date cal-ico">
                                                                <input class="cstm-que-ctrl" placeholder="27/11/2019" type="text" name="date-1" id="date-1">
                                                                <img src="{{asset('images/calendar.png')}}" width="25">
                                                            </td>
                                                            <td class="lable-time">Time</td>
                                                            <td class="input-time"><input class="cstm-que-ctrl" placeholder="22:24" type="text" name="time-1" id="time-1"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="feed-ico"><img src="{{asset('images/twitter_bb.png')}}"></td>
                                                            <td class="lable-date">Date</td>
                                                            <td class="input-date cal-ico">
                                                                <input class="cstm-que-ctrl" placeholder="27/11/2019" type="text" name="date-1" id="date-1">
                                                                <img src="{{asset('images/calendar.png')}}" width="25">
                                                            </td>
                                                            <td class="lable-time">Time</td>
                                                            <td class="input-time"><input class="cstm-que-ctrl" placeholder="22:24" type="text" name="time-1" id="time-1"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="feed-ico"><img src="{{asset('images/linkedin_bb.png')}}"></td>
                                                            <td class="lable-date">Date</td>
                                                            <td class="input-date cal-ico">
                                                                <input class="cstm-que-ctrl" placeholder="27/11/2019" type="text" name="date-1" id="date-1">
                                                                <img src="{{asset('images/calendar.png')}}" width="25">
                                                            </td>
                                                            <td class="lable-time">Time</td>
                                                            <td class="input-time"><input class="cstm-que-ctrl" placeholder="22:24" type="text" name="time-1" id="time-1"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="feed-ico"><img src="{{asset('images/insta_dd.png')}}"></td>
                                                            <td class="lable-date">Date</td>
                                                            <td class="input-date cal-ico">
                                                                <input class="cstm-que-ctrl" placeholder="27/11/2019" type="text" name="date-1" id="date-1">
                                                                <img src="{{asset('images/calendar.png')}}" width="25">
                                                            </td>
                                                            <td class="lable-time">Time</td>
                                                            <td class="input-time"><input class="cstm-que-ctrl" placeholder="22:24" type="text" name="time-1" id="time-1"></td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>

                                    </div>
                                </li>
                                <li class="left clearfix">

                                    <div class="chat-box que-result">

                                        <div class="confirm-btn">
                                            <button class="confirm-result-btn"> Confirm</button>
                                        </div>

                                    </div>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3 mb-12">
                        <div class="panel panel-right justify-content-center">
                            <div class="pt-1 pb-1">
                                <div class="align-items-center">
                                    <div class="help-panel ">
                                        <div class="panel-logo text-center">
                                            <img src="{{asset('/images/icon_blue.png')}}">
                                        </div>
                                        <div class="post-desc ">
                                            If you have any questions, please contact us. With the button below you can chat with us. At the moment we only can chat in English. We hope this is not a problem for you.
                                        </div>
                                        <div class="post-feed">
                                            <div class="accordion" id="accordionExample">
                                                <div class="card">
                                                    <div class="card-header" id="headingOne">
                                                        <h2 class="mb-0">
                                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                Help me post my message
                                                            </button>
                                                        </h2>
                                                    </div>

                                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                        <div class="card-body">
                                                            If you have any questions, please contact us. With the button below you can chat with us. At the moment we only can chat in English. We hope this is not a problem for you.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingTwo">
                                                        <h2 class="mb-0">
                                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                                I do not know how to schedule my post
                                                            </button>
                                                        </h2>
                                                    </div>
                                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                                        <div class="card-body">
                                                            If you have any questions, please contact us. With the button below you can chat with us. At the moment we only can chat in English. We hope this is not a problem for you.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingThree">
                                                        <h2 class="mb-0">
                                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                                Need Help with Settings
                                                            </button>
                                                        </h2>
                                                    </div>
                                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                                        <div class="card-body">
                                                            If you have any questions, please contact us. With the button below you can chat with us. At the moment we only can chat in English. We hope this is not a problem for you.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="chatting-sec">
                                            <span>Start Chatting</span>
                                            <a class="chatting-btn" href="#">Click here</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <!-- <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
      aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div> -->

    <!-- Optional JavaScript -->
    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{asset('bonc_custom.jsom.js')}}"></script>


</body>

</html>
