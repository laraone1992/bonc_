@extends('layouts.layout')
@section('title','Brands')

@section('content')
    <!-- Content Start -->
    <div id="content-setting" class="d-flex">

        <!-- =========================================
        Content Section
        ========================================== -->
        <section class=" content-section-setting  content-section-brand flex-fill">

            <div class="row">
                <div class="col-md-12" style="align-items: center;">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="row ">
                            <div class="brand-header col-6">BRANDS</div>
                            <div class="col-6 text-right">
                                <input type="button" data-toggle="modal" data-target="#addmemberText" class="btn btn-secondary text-left"
                                                         value="+ Add Member" id="submit">
                            </div>
                        </div>
                        <br>

                        <div class="tab-pane active scroll-cust">
                            <div class="tab-input">
                                <div class="input-group-brand form-group">

                                    <div class="row setting-channel-list">


                                            <div class="col-md-9 col-sm-6">
                                                <ul class="list-unstyled d-flex align-items-center m-0 pl-2">
                                                    <li><img src="{{asset('')}}images/ic/company.png" class="brandlogo"
                                                             alt="">
                                                        <label>Desilo<br>
                                                            <small><img src="{{asset('')}}images/ic/user-image.png"
                                                                        style="width: 20px;" alt=""></small>
                                                            <small><img src="{{asset('')}}images/ic/user-image.png"
                                                                        style="width: 20px;" alt=""></small>
                                                            <small><img src="{{asset('')}}images/ic/user-image.png"
                                                                        style="width: 20px;" alt=""></small>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3 col-sm-6 text-right">

                                                <label>
                                                    <span class="menu-icon"></span><br>
                                                    <small>
                                                        <img src="{{asset('')}}images/facebook.svg"
                                                                style="width: 20px;" alt="">
                                                    </small>
                                                    <small>
                                                        <img src="{{asset('')}}images/twitter.svg"
                                                                style="width: 20px;" alt="">
                                                    </small>
                                                    <small>
                                                        <img src="{{asset('')}}images/linkedin.svg"
                                                                style="width: 20px;" alt="">
                                                    </small>
                                                    <small>
                                                        <img src="{{asset('')}}images/instagram.svg"
                                                                style="width: 20px;" alt="">
                                                    </small>
                                                </label>
                                            </div>

                                    </div>



                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </section>
    </div>
    <div class="modal " id="addmemberText" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-sm" style="width: 30%;" role="document">
            <div class="modal-content">
                <div class="modal-header black-header black-header-member">
                    <span>Add Member</span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="close-popup-btn">&times;</span>
                    </button>
                </div>
                <div class="modal-body bonc-txt-body">
                    <form action="" method="post" name="addmember">
                        @csrf
                     <div class="input-group-member form-group">
                        <label for="email" class="control-label">Email Address</label>
                        <input type="text" class="form-control setting" id="email" name="email" placeholder="Email">
                        <span class="d-none validate" id="emailErr">Please enter email</span>
                    </div>
                     <div class="input-group-member form-group">
                        <label for="email" class="control-label">Username</label>
                        <input type="text" class="form-control setting" id="username" name="username" placeholder="Username">
                        <span class="d-none validate" id="userErr">Please enter username</span>
                     </div>
                    <div class="input-group-member form-group">
                        <label for="password" class="control-label">Password</label>
                         <input type="password" class="form-control setting" id="password" name="password" placeholder="Password">
                          <span class="d-none validate" id="passwordErr">Please enter password</span>
                    </div>
                    <div class="input-group-member form-group">
                         <div class="row">
                             <div class="col-md-4">
                                 <input type="radio" value="2" name="role" id="admin" />
                                  <label for="admin">Admin</label>
                             </div>
                             <div class="col-md-7">
                                 <input type="radio" value="3" name="role" id="user" checked=""/>
                                  <label for="user">User</label>
                             </div>

                         </div>

                    </div>
                    <div class="input-group-member form-group">

                        <div class="col-md-12 text-center">
                            <input type="button" class="btn btn-secondary" value="Add Member" id="addmember">
                        </div>
                    </div>
                    </form>
            </div>
            </div>
        </div>
    </div>


@endsection

@push('styles')
    <style>
        input[type="file"] {
            display: none;
        }

        .delete {
            background-color: Transparent;
            background-repeat: no-repeat;
            border: none;
            cursor: pointer;
            overflow: hidden;
            position: absolute;
            top: -21px;
            right: 0;
            z-index: 100;
            color: black;
        }

        .profile-img-container {
            position: relative;
            padding-right: 8px;
        }

        .profile-img-container img {
            height: 100%;
        }

        .profile-img-container i {
            top: 45%;
            left: 45%;
            transform: translate(-23%, -27%);
            display: none;
        }

        .profile-img-container:hover img {
            opacity: 0.5;
        }

        .profile-img-container:hover i {
            display: block;
            z-index: 500;
        }

        .gallery img {

        }

        .company-logo-sec {
            padding: 0;
            display: table
        }

        .company-logo-sec li {
            list-style: none;
            display: table-cell;
            width: 50px;
            height: 40px;
            padding-right: 10px;
            position: relative;
        }

        .company-logo-sec li img {
            width: 100%;
            height: 100%;
        }

        .company-icon {
        }

        .vl {
            border-left: 1px solid lightgrey;
            height: 46px;
            position: absolute;
            left: 18%;
            margin-left: -29px;
            top: 8px;
        }

        .user-profile {
            border-radius: 20px;
            width: 80%;
        }

        .social-summary-linked {
            padding-right: 15px;
            border-right: 1px solid #ccd0d9;
        }

        .list-unstyled.social-summary-linked li img {
            width: 35px;
        }

        .profile-name li a {
            /*margin-left: -30px;*/
        }

        .input-group-brand input {
            padding: 15px;
            border-radius: 8px;
        }

        .content-section-brand {
            padding: 58px 24%;
        }

        .content-section-brand .tab-pane {
            background: #fff;
            width: 100% !important;
            padding: 4px 15px;
            border-radius: 10px;
        }

        .input-group-brand label {
            margin: 15px 6px;
            font-size: 19px;
            color: #000;
        }

        .input-group-brand label.add-logo-btn {
            margin: 0 6px;
            border-radius: 7px;
            color: #ced4da;
        }

        .input-group-brand label.btn-social {
            border: 1px #d5d9de;
            border-radius: 30px;
            /*color: #aba0a0;*/
            /*background-color: #d5d9de;*/
            padding: 5px 27px 4px 57px;
            margin-top: 0px;
            margin: 0 9px;
            font-size: inherit;
        }

        .input-group-brand label.btn-social-facebook {
            background-color: #3b5998;
            color: #fff;
        }

        .input-group-brand label.btn-social-twitter {
            background-color: #d5d9de;
            color: #aba0a0;
        }

        .input-group-brand label.btn-social-linkdln {
            background-color: #d5d9de;
            color: #aba0a0;
        }

        .input-group-brand label.btn-social-instagram {
            background-color: #d5d9de;
            color: #aba0a0;
        }

        .input-group-brand .btn-secondary {
            border-radius: 30px;
            padding: 10px 20px;
        }

        .btn-social img {
            width: 28px;
            margin-left: -52px;
            margin-right: inherit;
            border: 1px solid;
            color: beige;
            border-radius: inherit;
        }

        .brand-header {
            color: black;
            font-weight: 500;
        }

        .menu-icon:after {
            content: '\2807';
            font-size: 22px;
            color: #a09c9c;
            position: absolute;
            right: 30px;
        }

        .brandlogo {
            margin-top: -30px;
            width: 48px;
            border-radius: 7px;
        }

        .social-icon {
            padding-left: 19rem;
        }

        #viewbrand {
            overflow-x: hidden;
            overflow-y: visible;
            height: 500px;
            overflow: scroll;
            overflow: auto;
        }
        .input-group-member label {
            
            font-size: 19px;
            color: #000;
        }
        .input-group-member input {
                padding: 24px;
                border-radius: 8px;
            }
            .input-group-member .btn-secondary {
                border-radius: 30px;
                padding: 10px 20px;
            }
            .black-header-member {
                background-color: #000000;
                padding: 15px 30px;
                color: #FFF;
                font-size: 22px;
            }
            .validate{
                color: red;
            }
    </style>
@endpush
@push('scripts')
    <script src="{{asset('')}}js/jscolor.js"></script><!-- pignose.calendar.full.min -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('#addmember').on('click', function () {
               var email=$('#email').val();
               var username=$('#username').val();
               var password=$('#password').val();
               var role=$("input[name='role']:checked").val();
               if(email == ''){
                   $('#emailErr').removeClass('d-none');
               }
               else{
                 $('#emailErr').addClass('d-none');  
               }
               if(username == ''){
                   $('#usernameErr').removeClass('d-none');
               }
               else{
                 $('#usernameErr').addClass('d-none');  
               }
               if(password == ''){
                   $('#passwordErr').removeClass('d-none');
               }
               else{
                 $('#passwordErr').addClass('d-none');  
               }
               if(email != '' && username != '' && password != ''){
                $.ajax({
                    type: "POST",
                    headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                    url: "{{url('add/member')}}",
                    dataType: "json",
                    data:{
                        email:email,
                        username:username,
                        password:password,
                        role:role,
                    },
                    success: function (data) {
                        
                      $('#addmemberText').modal('hide');  
                    },
                    failure: function (response) {
                        alert("No Match");
                    }
                });
                 }
            });
            $('#addmemberText').on('hidden.bs.modal', function () {
                $(this).find('form').trigger('reset');
            })
       
        });
    </script>

@endpush
