<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'channel';
    protected $fillable = ['id','user_id','uuid', 'channel_name','channel_id','ch_user_name','status', 'image','created_at','updated_at'];
//    public $incrementing = false;
//    public $timestamps = false;
//    protected $primaryKey = null;

}
