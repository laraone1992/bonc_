<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('locale/{locale}',function($locale){ Session::put('locale',$locale); return redirect()->back();})->name('locale');

Route::post('bonc/text/search','HomeController@textSearch');
//Route::get('/', 'HomeController@index')->name('home');
Route::get('/', 'Auth\Auth0IndexController@register')->name('home');
Route::get('/home/{date?}', 'HomeController@index')->name('home');
Route::get('user/verify', 'HomeController@userVerify')->name('user.verify');
Route::get('/schedule', 'ScheduleController@index')->name('schedule');
Route::get('/concept', 'ConceptController@index')->name('concept');
Route::get('/previousPost', 'ScheduleController@previousPost')->name('previousPost');
Route::get('/post/{id}/edit', 'ScheduleController@editPost')->name('edit_post');
Route::get('/setting', 'SettingController@index')->name('setting');
Route::post('/save_presonal_detail', 'SettingController@savePersonalDetail')->name('save_presonal_detail');
Route::delete('delete/image/{id}','SettingController@deleteImage');
Route::post('disconnect/account','SettingController@disconnectAccount');
Route::post('reconnect/account','SettingController@reConnectAccount');
Route::get('brand/add','SettingController@addBrand');
Route::get('brand/view','SettingController@viewBrand');
Route::get('member/manage','SettingController@memberManage');
Route::post('add/member','SettingController@addMember')->name('add-member');
Route::get('/notificationList', 'HomeController@notificationList')->name('notificationList');

Route::post('share/now/{id}','ScheduleController@shareNow')->name('share-now');

Route::post('/post-data', 'ScheduleController@postData')->name('post-data');
Route::post('/savePostDesign', 'ScheduleController@savePostDesign')->name('savePostDesign');
Route::post('/filterPostDesign', 'ScheduleController@filterPostDesign')->name('filterPostDesign');
Route::get('/test', 'HomeController@test')->name('test');
Route::get('/linkdin', 'HomeController@linkdin')->name('linkdin');
Route::get('/twitter', 'HomeController@twitter')->name('twitter');
Route::get('/fb', 'HomeController@fabebook')->name('fabebook');
Route::get('subcription/delete','HomeController@subcription')->name('subcription');
Route::get('subcription/update','SettingController@subcriptionUpdate')->name('subcription');

Route::post('concept/data','ConceptController@conceptData')->name('concept-data');
Route::get('edit/concept/{id?}','ConceptController@editConcept')->name('edit-concept');
Route::get('delete/concept/{id}','ConceptController@deleteConcept')->name('delete-concept');
Route::post('update/concept/data','ConceptController@updateConcept')->name('update-concept-data');
Route::post('/schedule-concept/{id}', 'ConceptController@scheduleData')->name('schedule-concept');
Route::get('/confirm-concept/{id}','ConceptController@confirmConceptPost');
Route::get('edit/post/{id}/{p_id}','ScheduleController@editPost')->name('edit-post');
Route::post('update/post/data','ScheduleController@updatePost')->name('update-post-data');
Route::get('delete/post/{id}/{p_id?}','ScheduleController@deletePost')->name('delete-post-data');

Route::get( '/auth0/callback', '\Auth0\Login\Auth0Controller@callback' )->name( 'auth0-callback' );
Route::get( '/login', 'Auth\Auth0IndexController@login' )->name( 'login' );
Route::get( '/logout', 'Auth\Auth0IndexController@logout' )->name( 'logout' )->middleware('auth');
Route::get( '/register', 'Auth\Auth0IndexController@register' )->name( 'register' );
Route::post( '/register', 'Auth\Auth0IndexController@registerUser')->name( 'user.register');

Route::get( '/demo', 'HomeController@demo')->name('demo');
Route::get('/instagram/notification','HomeController@notification')->name('notification');
Route::get('notification/view','HomeController@notificationView')->name('notification-view');
Route::post('bonc/quote','HomeController@boncQuotes')->name('Bonc-Quote');
Route::post('bonc/photo','HomeController@boncPhotos')->name('Bonc-Photo');
Route::get('bonc/quote/get','HomeController@boncQuoteGet')->name('Bonc-Quote-Get');
Route::get('bonc/photo/get','HomeController@boncPhotoGet')->name('Bonc-Photo-Get');
Route::get('bonc/sticker','HomeController@boncSticker')->name('Bonc-Sticker');

Route::get('need-help','HomeController@needHelpEmail');

Route::get('photoEditor','HomeController@photoEditor');
