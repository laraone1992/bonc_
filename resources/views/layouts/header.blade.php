    <!-- =========================================
    Header Section
    ========================================== -->
    @php
        $uriName = request()->route()->getName();
    @endphp

    <header class="navbar navbar-expand-md navbar-dark bg-dark justify-content-between flex-md-row-reverse">
        <div class="header-right d-flex align-items-center justify-content-between justify-content-md-start">
            <ul class="quick-icons-header list-unstyled mb-0 d-flex align-items-center">
                <li><a target="_blank" href="https://bonconline.com/en/faq/"><img src="{{asset('')}}images/ic/icon-qustion.png" alt=""></a></li>
                <li>
                    <div class="notification-tag dropdown">
                        <button type="button" class="dropdown-toggle notification-btn d-flex align-items-center" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="notification">
                                <p class="notification-count d-none" id="notificationCount"></p>
                                <img src="{{asset('')}}images/ic/icon-bell.png" alt="">
                            </span>
                        </button>
                        <div class="dropdown-menu notification-box dropdown-menu-right" aria-labelledby="dropdownMenuOffset">

                            <div class="header-notification"> Noftifications </div>
                            <div class="content-notification">
                                <ul id="notification">
                                    <li class="d-flex">
                                        <span class="notification-icon d-flex">
                                            <img src="{{asset('').'images/ic/insta40.png'}}">
                                        </span>
                                        <div class="txt-nt ">
                                            <p class="nt-cnt">You have asked me to remind you that you want to post something on instagram today at 10:00</p>
                                        </div>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
{{--                <li>--}}
{{--                    <a href="#"><img src="{{asset('')}}images/ic/icon-bell.png" alt=""></a>--}}

{{--                </li>--}}
                <li class=" @if($uriName == 'setting') active @endif "><a href="{{url('setting')}}"><img src="{{asset('')}}images/ic/icon-setting.png" alt=""></a></li>
            </ul>
            <div class="header-user dropdown">
                <button type="button" class="dropdown-toggle d-flex align-items-center" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="user-image"><img src="{{asset('')}}images/ic/user-image.png" alt=""></span>
                    {{\Illuminate\Support\Facades\Auth::user()->name}}
                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuOffset">
{{--                    <a class="dropdown-item" href="#">Profile</a>--}}
                    <ul class="language">
                        <li> <a class="{{ app()->getLocale()=='en' ? 'active' : ''}}" href="locale/en"> EN</a></li>
                        <li> <a class="{{ app()->getLocale()=='du' ? 'active' : ''}}" href="locale/du"> NL</a></li>
                    </ul>
                    <div class="clearfix"></div>
                    <hr>
                    <a class="dropdown-item" href="{{url('logout')}}">{{__('message.Sign_Out')}}</a>
                </div>
            </div>
        </div>
        <div class="d-md-flex align-items-center logo-header">
            <div class="d-flex align-items-center justify-content-between">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="{{asset('')}}images/logo.svg" alt=""></a>
                <div class="header-user-mob dropdown">
                    <button type="button" class="dropdown-toggle user-drp d-flex align-items-center" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="user-image"><img src="{{asset('')}}images/ic/user-image.png" alt=""></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuOffset">
                        <ul class="language">
                            <li> <a class="{{ app()->getLocale()=='en' ? 'active' : ''}}" href="locale/en"> EN</a></li>
                            <li> <a class="{{ app()->getLocale()=='du' ? 'active' : ''}}" href="locale/du"> NL</a></li>
                        </ul>
                        <div class="clearfix"></div>
                        <hr>
                        <a class="dropdown-item" href="#">Sign Out</a>
                    </div>
                </div>
            </div>

            <nav id="nav">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
{{--                        <li class="nav-item dropdown">--}}
{{--                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                                Post--}}
{{--                            </a>--}}
{{--                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">--}}
{{--                                <a class="dropdown-item" href="#">Post</a>--}}
{{--                                <a class="dropdown-item" href="#">Post 1</a>--}}
{{--                                <a class="dropdown-item" href="#">Post 2</a>--}}
{{--                            </div>--}}
{{--                        </li>--}}
                        <li class="nav-item @if($uriName == 'home') active @endif">
                            <a class="nav-link" href="{{url('home')}}">{{__('message.Post')}}</a>
                        </li>
                        <li class="nav-item @if($uriName == 'schedule') active @endif ">
                            <a class="nav-link" href="{{url('schedule')}}">{{__('message.Schedule')}}</a>
                        </li>
                        <li class="nav-item @if($uriName == 'concept') active @endif ">
                            <a class="nav-link" href="{{url('concept')}}">Concept</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="http://boncshop.com/">Bonc Shop</a>
                        </li>
                        <li class="nav-item mob-only @if($uriName == '') active @endif ">
                            <a class="nav-link" href="https://bonconline.com/en/faq/">Help</a>
                        </li>
                        <li class="nav-item mob-only @if($uriName == '') active @endif ">
                            <a class="nav-link" href="{{url('notificationList')}}">Notification</a>
                        </li>
                        <li class="nav-item mob-only @if($uriName == '') active @endif ">
                            <a class="nav-link" href="{{url('setting')}}">Settings</a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
        <div class="modal fade" id="notificationview" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header black-header">
                        <h5 class="modal-title notification-title" id="exampleModalLongTitle">Share on Instagram</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="viewNotification">


                    </div>
                    <div class="modal-footer">
                        <a href="" class="btn btn-dark" id="download-img" download>Share post on Instagram</a>
                        @if(stripos($_SERVER['HTTP_USER_AGENT'],"iPhone"))
                            <a href="instagram://user?username=bonc_online" class="btn btn-dark shareInsta imgsrcdownload d-none" data-src="" onclick="copyToClipboard($(this).data('src'))">Share post on Instagram</a>
                        @elseif(stripos($_SERVER['HTTP_USER_AGENT'],"android"))
                            <a href="intent://instagram.com/#Intent;package=com.instagram.android;scheme=https;end" class="btn btn-dark shareInsta imgsrcdownload d-none" data-src="" onclick="copyToClipboard($(this).data('src'))">Share post on Instagram</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="notificationList" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header black-header">
                        <h5 class="modal-title notification-title" >Notifications</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="viewNotificationList">
                            <div class="content-notification">
                                <ul id="notifications">

                                </ul>
                            </div>
                    </div>
                </div>
            </div>
        </div>

    </header>

