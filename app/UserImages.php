<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserImages extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_images';
    protected $primaryKey = 'id';
}
