@extends('layouts.layout')
@section('title','Setting')

@section('content')
    <!-- Content Start -->
    <div id="content-setting" class="d-flex">

        <!-- =========================================
        Content Section
        ========================================== -->
        <section class="content-section-setting flex-fill">

            <div class="row">
                <div class="col-md-3">
                    <!-- required for floating -->
                    <!-- Nav tabs -->
                    <ul class="nav flex-column setting-tab">
                        <li class="active"><a href="#linked" class="active" data-toggle="tab">{{__('message.Linked_Account')}}</a></li>
                        <li ><a href="#account"  data-toggle="tab">{{__('message.Account')}}</a></li>
{{--                        <li><a href="#design" data-toggle="tab">{{__('message.Design')}}</a></li>--}}
{{--                        <li><a href="#security" data-toggle="tab">{{__('message.Security')}}</a></li>--}}
                        <li><a href="#billing" data-toggle="tab">{{__('message.Billing_payment')}}</a></li>
                    </ul>
                </div>
                <div class="col-md-9">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="linked">
                            <div class="tab-header col-md-12">
                                <div class="row">
                                    <div class="col-md-9">{{__('message.LINKED_ACCOUNT')}}</div>
                                    <div class="col-md-3 text-right">
                                        <div class="btn-group">
                                            <a href="" class="btn btn-secondary btn-web" data-toggle="dropdown">+ {{__('message.Add_Post')}}</a>

                                            <div class="dropdown-menu">
                                                <a href="{{$linkOuth['facebook']}}" target="_blank" class="dropdown-item"><img src="{{asset('')}}images/ic/facebook40.png" alt="" width="25%">  Facebook</a>
                                                <div class="dropdown-divider"></div>
                                                <a href="{{$linkOuth['twitter']}}" target="_blank" class="dropdown-item"><img src="{{asset('')}}images/ic/twitter40.png" alt="" width="25%">  Twitter</a>
                                                <div class="dropdown-divider"></div>
                                                <a href="{{$linkOuth['linkdin']}}" target="_blank" class="dropdown-item"><img src="{{asset('')}}images/ic/linkedin40.png" alt=""  width="25%">  Linkedln</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="tab-input">
                                <div class="input-group-setting form-group">
                                    <div class="row">
                                        @foreach($channels['data'] as $key => $channel)
                                            @if($channel['type'] != 'facebook_profile')
                                                @if($channel['type'] == 'facebook_profile')
                                                @elseif($channel['type'] == 'facebook_page')
                                                    <div class="col-md-12" id="disconnect{{$channel['uuid']}}">
                                                        <div class="d-flex align-items-center channel-set">
                                                            <div class="col-md-2">
                                                                <ul class="list-unstyled social-summary-linked d-flex align-items-center m-0 ">
                                                                    <li><a href="#"><img src="{{asset('')}}images/facebook.svg" alt=""></a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <ul class="list-unstyled d-flex align-items-center m-0 pl-2">
                                                                    <li><a href="#"> <img src="{{$channel['profile_picture_url']}}"
                                                                                          alt="" class="user-profile"
                                                                                          onerror="this.onerror=null;this.src='{{asset('images/noimage.png')}}';"></a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <ul class="list-unstyled profile-name d-flex align-items-center m-0">
                                                                    <li><a href="#" class="">{{$channel['name']}}</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <ul class="list-unstyled d-flex align-items-center m-0 pull-right">
                                                                    <li><button type="button" id="disconnect" data-id="{{$channel['uuid']}}" class="btn btn-home-grey btn-block disconnet ">{{__('message.disconnect')}}</button></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                @elseif($channel['type'] == 'twitter')
                                                    <div class="col-md-12" id="disconnect{{$channel['uuid']}}">
                                                        <div class="d-flex align-items-center channel-set">
                                                            <div class="col-md-2">
                                                                <ul class="list-unstyled social-summary-linked d-flex align-items-center m-0">
                                                                    <li><a href="#"><img src="{{asset('')}}images/twitter.svg" alt=""></a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <ul class="list-unstyled d-flex align-items-center m-0 pl-2">
                                                                    <li><a href="#"> <img src="{{$channel['profile_picture_url']}}"
                                                                                          alt="" class="user-profile"
                                                                                          onerror="this.onerror=null;this.src='{{asset('images/noimage.png')}}';"></a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <ul class="list-unstyled profile-name d-flex align-items-center m-0">
                                                                    <li><a href="#" class="">{{$channel['name']}}</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <ul class="list-unstyled d-flex align-items-center m-0 pull-right">
                                                                    <li><button type="button" id="disconnect" data-id="{{$channel['uuid']}}" class="btn btn-home-grey btn-block disconnet ">{{__('message.disconnect')}}</button></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                @elseif($channel['type'] == 'linkedin_profile')
                                                    <div class="col-md-12" id="disconnect{{$channel['uuid']}}">
                                                        <div class="d-flex align-items-center channel-set">
                                                            <div class="col-md-2">
                                                                <ul class="list-unstyled social-summary-linked d-flex align-items-center m-0">
                                                                    <li><a href="#"><img src="{{asset('')}}images/linkedin.svg" alt=""></a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <ul class="list-unstyled d-flex align-items-center m-0 pl-2">
                                                                    <li><a href="#"> <img src="{{$channel['profile_picture_url']}}"
                                                                                          alt="" class="user-profile"
                                                                                          onerror="this.onerror=null;this.src='{{asset('images/noimage.png')}}';"></a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <ul class="list-unstyled profile-name d-flex align-items-center m-0">
                                                                    <li><a href="#" class="">{{$channel['name']}}</a></li>
                                                                </ul>
                                                            </div>
                                                            <div class="col-md-3 ">
                                                                <ul class="list-unstyled d-flex align-items-center m-0 pull-right">
                                                                    <li><button type="button" id="disconnect" data-id="{{$channel['uuid']}}" class="btn btn-home-grey btn-block disconnet">{{__('message.disconnect')}}</button></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                @endif
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                                {{--                                    <div class="input-group-setting form-group">--}}
                                {{--                                        <div class="row">--}}
                                {{--                                            <label for="submit" class="col-md-5 control-label"></label>--}}
                                {{--                                            <div class="col-md-6">--}}
                                {{--                                                <input type="submit" class="btn btn-secondary" value="{{__('message.Save_Changes')}}" id="submit">--}}
                                {{--                                            </div>--}}
                                {{--                                        </div>--}}

                                {{--                                    </div>--}}
                            </div>

                        </div>
                        <div class="tab-pane" id="account">
                            <form name="personal_detail_form" method="post" id="personal-detail-form" enctype="multipart/form-data" action="{{url('save_presonal_detail')}}">
                                @csrf
                                <div class="tab-header">
                                    {{__('message.PERSONAL_DETAILS')}}
                                </div>
                                <div class="tab-input">
                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="f_name" class="col-md-3 control-label">{{__('message.First_Name')}}</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control setting" value="{{$userDetails->given_name}}" id="f_name" name="f_name" placeholder="First Name">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="l_name" class="col-md-3 control-label">{{__('message.Last_Name')}}</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control setting" value="{{$userDetails->family_name}}" id="l_name" name="l_name" placeholder="Last Name">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="email" class="col-md-3 control-label">{{__('message.Email')}}</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control setting" id="email" value="{{$userDetails->email}}" placeholder="Email" readonly>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="cur_pwd" class="col-md-3 control-label">{{__('message.Logo')}}</label>
                                            <div class="col-md-9">
                                                @foreach($userLogo as $image)
                                                    <span id="image{{$image['id']}}" class="profile-img-container">
                                                        <button type="button" class="delete" data-id="{{$image['id']}}">
                                                            <i class="fa fa-remove"></i>
                                                        </button>
                                                        <img src="{{$image['image']}}"  width="10%" height="50%">
                                                    </span>
                                                @endforeach
                                                @if($count != 3)
                                                        <span class="gallery ">

                                                        </span>
                                                <label class="btn btn-setting add-logo-btn"><input type="file" multiple name="logo[]" id="imgInp">+</label>

                                                 @else
                                                        <label class="btn btn-setting" style="background-color:#dddddd;"><input type="file" name="logo" disabled>+</label>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="cur_pwd" class="col-md-3 control-label">{{__('message.Language')}}</label>
                                            <div class="col-md-9">
                                               <select class="form-control setting cstm-select" id="language" name="language">
                                                   <option value="en" @if(app()->getLocale() == 'en') selected @endif>English</option>
                                                   <option value="nl" @if(app()->getLocale() == 'du') selected @endif>Dutch</option>
                                               </select>
                                            </div>
                                        </div>
                                    </div>
{{--                                    <hr>--}}
{{--                                    <div class="input-group-setting form-group">--}}
{{--                                        <div class="row">--}}
{{--                                            <label for="social" class="col-md-3 control-label">{{__('message.Linked_Account')}}</label>--}}
{{--                                            <div class="col-md-9">--}}
{{--                                                <ul class="list-unstyled social-setting d-flex align-items-center m-3">--}}
{{--                                                    @if(in_array('facebook_profile', $channelList))--}}
{{--                                                        <li><a href="#"><img src="{{asset('')}}images/ic/facebook40.png" alt=""></a></li>--}}
{{--                                                    @else--}}
{{--                                                        <li><a href="{{$linkOuth['facebook']}}"><img src="{{asset('')}}images/ic/facebook40.png" class="disable-img" alt=""></a></li>--}}
{{--                                                    @endif--}}
{{--                                                    @if(in_array('twitter', $channelList))--}}
{{--                                                        <li><a href="#"><img src="{{asset('')}}images/ic/twitter40.png" alt=""></a></li>--}}
{{--                                                    @else--}}
{{--                                                        <li><a href="{{$linkOuth['twitter']}}"><img src="{{asset('')}}images/ic/twitter40.png" class="disable-img" alt=""></a></li>--}}
{{--                                                    @endif--}}
{{--                                                    @if(in_array('linkedin_profile', $channelList))--}}
{{--                                                        <li><a href="#"><img src="{{asset('')}}images/ic/linkedin40.png" alt=""></a></li>--}}
{{--                                                    @else--}}
{{--                                                        <li><a href="{{$linkOuth['linkdin']}}"><img src="{{asset('')}}images/ic/linkedin40.png" class="disable-img" alt=""></a></li>--}}
{{--                                                    @endif--}}
{{--                                                    <li><a href="#"><img src="{{asset('')}}images/ic/insta40.png" class="disable-img" alt=""></a></li>--}}
{{--                                                </ul>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                    </div>--}}
                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="submit" class="col-md-3 control-label"></label>
                                            <div class="col-md-9">
                                                <input type="submit" class="btn btn-secondary" value="{{__('message.Save_Changes')}}" id="submit">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane" id="design">
                            <form>
                                <div class="tab-header">
                                    {{__('message.TEMPLATE')}}
                                </div>
                                <div class="tab-input">
                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="" class="col-md-3 control-label">{{__('message.Logo')}}</label>
                                            <div class="col-md-9">
                                                <button class="btn btn-setting add-logo-btn">+</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="new_pwd" class="col-md-3 control-label">{{__('message.Company_Colors')}}</label>
                                            <div class="col-md-9">
                                                <button class="jscolor {valueElement:null,value:'000'}" style="width:50px; height:50px; margin-top: 10px; border: none"></button>
                                                <button class="jscolor {valueElement:null,value:'ccc'}" style="width:50px; height:50px; margin-top: 10px; border: none;  margin-left: -5px"></button>
                                                <button class="jscolor {valueElement:null,value:'a5a5a5'}" style="width:50px; height:50px; margin-top: 10px; border: none;  margin-left: -5px"></button>
                                                <button class="jscolor {valueElement:null,value:'fff'}" style="width:50px; height:50px; margin-top: 10px; border: none;  margin-left: -5px"></button>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="con_pwd" class="col-md-3 control-label">{{__('message.Font')}}</label>
                                            <div class="col-md-9">
                                                <select name="font" id="font" class="form-control setting cstm-select">
                                                    <option selected disabled>Select Font</option>
                                                    <option value="1">Arial</option>
                                                    <option value="2">Bookman</option>
                                                    <option value="3">Candara</option>
                                                    <option value="4">Courier</option>
                                                    <option value="5">Garamond</option>
                                                    <option value="6">Georgia</option>
                                                    <option value="7">Palatino</option>
                                                    <option value="8">Roboto</option>
                                                    <option value="9">Times New Roman</option>
                                                    <option value="10">Verdana</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="con_pwd" class="col-md-3 control-label">{{__('message.Font_color')}}</label>
                                            <div class="col-md-9">
                                                <button class="jscolor {valueElement:null,value:'000'}" style="width:50px; height:50px; margin-top: 10px; border: none"></button>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="con_pwd" class="col-md-3 control-label">{{__('message.Background_Color')}}</label>
                                            <div class="col-md-9">
                                                <button class="jscolor {valueElement:null,value:'000'}" style="width:50px; height:50px; margin-top: 10px; border: none"></button>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="con_pwd" class="col-md-3 control-label">{{__('message.Favourite_Template')}}</label>
                                            <div class="col-md-9">
                                                <button class="btn btn-setting add-logo-btn">+</button>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="submit" class="col-md-3 control-label"></label>
                                            <div class="col-md-9">
                                                <input type="submit" class="btn btn-secondary" value="{{__('message.Save_Changes')}}" id="submit">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
{{--                        <div class="tab-pane" id="security">--}}
{{--                            <form>--}}
{{--                                <div class="tab-header">--}}
{{--                                    {{__('message.CHANGE_PASSWORD')}}--}}
{{--                                </div>--}}
{{--                                <div class="tab-input">--}}
{{--                                    <div class="input-group-setting form-group">--}}
{{--                                        <div class="row">--}}
{{--                                            <label for="cur_pwd" class="col-md-3 control-label">{{__('message.Current_Password')}}</label>--}}
{{--                                            <div class="col-md-9">--}}
{{--                                                <input type="password" class="form-control setting"  id="cur_pwd" name="cur_pwd" placeholder="">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="input-group-setting form-group">--}}
{{--                                        <div class="row">--}}
{{--                                            <label for="new_pwd" class="col-md-3 control-label">{{__('message.New_Password')}}</label>--}}
{{--                                            <div class="col-md-9">--}}
{{--                                                <input type="password" class="form-control setting" id="new_pwd" name="new_pwd" placeholder="">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                    </div>--}}
{{--                                    <div class="input-group-setting form-group">--}}
{{--                                        <div class="row">--}}
{{--                                            <label for="con_pwd" class="col-md-3 control-label">{{__('message.Confirm_Password')}}</label>--}}
{{--                                            <div class="col-md-9">--}}
{{--                                                <input type="password" class="form-control setting" id="con_pwd" placeholder="" >--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                    </div>--}}

{{--                                    <div class="input-group-setting form-group">--}}
{{--                                        <div class="row">--}}
{{--                                            <label for="submit" class="col-md-3 control-label"></label>--}}
{{--                                            <div class="col-md-9">--}}
{{--                                                <input type="submit" class="btn btn-secondary" value="{{__('message.Save_Changes')}}" id="submit">--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                        </div>--}}
                        <div class="tab-pane" id="billing">
                            <form>
                                <div class="tab-header">
                                    {{__('message.Billing')}}
                                </div>
                                <div class="tab-input">
                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="company_name" class="col-md-3 control-label">{{__('message.Company_Name')}}</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control setting"  id="company_name" name="company_name" placeholder="{{__('message.Company_Name')}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="f_name" class="col-md-3 control-label">{{__('message.Full_Name')}}</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control setting" id="f_name" name="f_name" placeholder="{{__('message.Full_Name')}}">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="address" class="col-md-3 control-label">{{__('message.Address')}}</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control setting" id="address" name="address" placeholder="Street name and number" >
                                                <input type="text" class="form-control setting mt-2" id="address2" name="address2" placeholder="{{__('message.Address')}}" >
                                            </div>
                                        </div>

                                    </div>
                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="city" class="col-md-3 control-label">{{__('message.City')}}</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control setting" id="city" name="city" placeholder="{{__('message.City')}}" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="zipcode" class="col-md-3 control-label">{{__('message.Zipcode')}}</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control setting" id="zipcode" name="zipcode" placeholder="{{__('message.Zipcode')}}" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="country" class="col-md-3 control-label">country</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control setting" id="country" name="country" placeholder="country" >
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label class="col-md-3 control-label">Payment</label>
                                            <div class="col-md-9">
                                                <div class="setting-payment">
                                                    <input type="radio" name="payment" id="paypal" />
                                                    <label for="paypal"><img src="{{asset('').'images/paypal.png'}}" alt="PayPal" /></label>

                                                    <input type="radio" name="payment" id="mpesa" />
                                                    <label for="mpesa"><img src="{{asset('').'images/mpesa.png'}}" alt="Mpesa" /></label>

                                                    <input type="radio" name="payment" id="ideal" />
                                                    <label for="ideal"><img src="{{asset('').'images/ideal.png'}}" alt="ideal" /></label>

                                                    <input type="radio" name="payment" id="credit-card" />
                                                    <label for="ideal">Credit Card</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

{{--                                    <div class="input-group-setting form-group">--}}
{{--                                        <div class="row">--}}
{{--                                            --}}
{{--                                        </div>--}}
{{--                                    </div>--}}

                                    <div class="input-group-setting form-group">
                                        <div class="row">
                                            <label for="submit" class="col-md-3 control-label"></label>
                                            <div class="col-md-9">
                                                <input type="submit" class="btn btn-secondary" value="{{__('message.Save_Changes')}}" id="submit">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

        </section>

    </div>

@endsection

@push('styles')
<style>
    input[type="file"] {
        display: none;
    }
    .delete {
        background-color: Transparent;
        background-repeat:no-repeat;
        border: none;
        cursor:pointer;
        overflow: hidden;
        position: absolute;
        top: -21px;
        right: 0;
        z-index: 100;
        color: black;
    }
    .profile-img-container {
        position: relative;
        padding-right: 8px;
    }
    .profile-img-container img{
        height: 100%;
    }

    .profile-img-container i {
        top: 45%;
        left: 45%;
        transform: translate(-23%, -27%);
        display: none;
    }

    .profile-img-container:hover img {
        opacity: 0.5;
    }

    .profile-img-container:hover i {
        display: block;
        z-index: 500;
    }
    .gallery img{

    }
    .company-logo-sec{ padding: 0; display: table}
    .company-logo-sec li{list-style: none; display: table-cell; width: 50px; height: 40px;padding-right:10px; position: relative;}
    .company-logo-sec li img{ width: 100%; height: 100%;}
    .company-icon{}

    .vl {
        border-left: 1px solid lightgrey;
        height: 46px;
        position: absolute;
        left: 18%;
        margin-left: -29px;
        top: 8px;
    }
    .user-profile{
        border-radius: 20px;
        width: 80%;
    }
    .social-summary-linked {
        padding-right: 15px;
        border-right: 1px solid #ccd0d9;
    }
    .list-unstyled.social-summary-linked li img {
        width: 35px;
    }
    .profile-name li a{
        /*margin-left: -30px;*/
    }

</style>
@endpush
@push('scripts')
    <script src="{{asset('')}}js/jscolor.js"> </script><!-- pignose.calendar.full.min -->
    <script type="text/javascript">
        $(document).ready(function () {
            $('.delete').on('click', function () {
                var id = $(this).data('id');

                $.ajax({
                    type: "DELETE",
                    // contentType: "application/json; charset=utf-8",
                    headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                    url: "delete/image/"+id,
                    dataType: "json",
                    success: function (data) {
                        $('#image'+id+'').hide();

                    },
                    failure: function (response) {
                        alert("No Match");
                    }
                });
            });

            $('.disconnet').on('click', function () {
                var uuid = $(this).data('id');
                console.log(uuid);
                $.ajax({
                    type: "POST",
                    // contentType: "application/json; charset=utf-8",
                    headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                    url: "disconnect/account",
                    data:{uuid:uuid},
                    dataType: "json",
                    success: function (data) {
                        $('#disconnect'+uuid+'').hide();
                        swal("Account Disconnected Successfully!");
                    },
                    failure: function (response) {
                        alert("No Match");
                    }
                });
            });
        });
        $(function() {
            // Multiple images preview in browser
            var imagesPreview = function(input, placeToInsertImagePreview) {

                if (input.files) {
                    var filesAmount = input.files.length;

                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();
                        var btns = '';
                        reader.onload = function(event) {
                            console.log(event.target.result);
                            btns = '<span id="imageD"'+i+' class="profile-img-container"><button type="button" class="delete" data-id="'+i+'">'+
                                '<i class="fa fa-remove"></i>'+
                                '</button>'+
                                '<img src="'+event.target.result+'" width="10%" height="50%"></span>';

                            // $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                            $('span.gallery').append(btns);
                        }

                        reader.readAsDataURL(input.files[i]);
                    }
                }

            };

            $('#imgInp').on('change', function() {
                if (parseInt($(this).get(0).files.length) > 3) {
                    alert("You can only upload a maximum of 3 images");
                } else {
                    imagesPreview(this, 'span.gallery');
                }

            });
        });
</script>

@endpush
