@extends('layouts.layout')
@section('title','Schedule')

@section('content')
    <!-- Content Start -->
    <div id="content" class="d-flex">
        <!-- =========================================
        Left Section
        ========================================== -->
        <section class="left-section sidebar">
            <div id="calendar"></div>
            <div class="side-accord">
                <div class="accordion" id="accordionExample1">
                    <div class="card">
                        <div class="card-header" id="headingOne" aria-expanded="false" >
                            {{__('message.Quantity_of_posts_Scheduled')}} : {{$count}}
                        </div>
                    </div>
{{--                    <div class="card">--}}
{{--                        <div class="card-header" >--}}
{{--                            <a href="{{url('previousPost')}}">{{__('message.Previous_Post')}}</a>--}}
{{--                            <span class="acc-arrow">--}}
{{--									<em class="fa fa-angle-right"></em>--}}
{{--								</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                </div>
            </div>
        </section>
        <!-- =========================================
        Content Section
        ========================================== -->
        <section class="content-section section-schedule flex-fill">

            @if(Session::has('success'))
                <p class="alert alert-success">{{ Session::get('success') }}</p>
            @endif
            @if(Session::has('error'))
                <p class="alert alert-danger">{{ Session::get('error') }}</p>
            @endif
                <div class="page-header">{{__('message.SCHEDULED_POST')}}</div>
            <div class="card mx-100">
                <div class="card-head">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active"data-toggle="tab" href="#summary" role="tab" aria-controls="summary" aria-selected="true">{{__('message.Summary')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#post" role="tab" aria-controls="post" aria-selected="false">{{__('message.Full_Post')}}</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body py-0">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="summary" role="tabpanel" aria-labelledby="summary-tab">
                            <div class="scroll-cust">
                                @if(count($finalDates) == 0)
                                    <div class="no-post-summ summary-block d-flex align-items-center justify-content-center mt-3">
                                        {{__('message.No_post_scheduled_yet')}}
                                    </div>
                                @else
                                @foreach($finalDates as $key => $date)

                                    <div class="day-block" id="sc-{{$key}}">
                                        <div class="dayname-head position-relative">

                                            @if($key == \Carbon\Carbon::today()->toDateString())
                                                {{__('message.Today')}}
                                            @else
                                                @php
                                                    $ndate = \Carbon\Carbon::parse($key)->locale('en')->formatLocalized('%A,%B,%e');
                                                    $ddate = explode(',',$ndate);

                                                    echo (__('message.'.$ddate[0]).', '. $ddate[2].' '. __('message.'.$ddate[1]) );
                                                @endphp
                                            @endif
                                            <a href="{{url('home/'.$key)}}" class="btn btn-secondary btn-web" data-date="{{$key}}">+ {{__('message.Add_Post')}}</a>
                                            <a href="{{url('home/'.$key)}}" class="btn-mobile btn-add-post" data-date="{{$key}}"><img src="{{asset('images/add.svg')}}"></a>
                                        </div>
                                        <div class="summary-group">
                                            @if(count($date) == 0)
                                                <div class="no-post-summ summary-block d-flex align-items-center justify-content-center">
                                                    {{__('message.No_post_scheduled_yet')}}
                                                </div>
                                            @else
                                                @foreach($date as $dk => $day_)
                                                    @foreach($day_ as $d_k => $day)
{{--                                                        @if($key == \Carbon\Carbon::today()->toDateString() && $day['time'] > \Carbon\Carbon::now())--}}
                                                            <div class="summary-block d-flex align-items-center justify-content-between" >
                                                            <div class="d-flex align-items-center mxw-50">
                                                                <span class="summary-image">
                                                                    <img src="{{$day['image']}}" onerror="this.onerror=null;this.src='{{asset('images/photo.svg')}}';" alt="">
                                                                </span>
                                                                @if(strlen($day['text']) > 50)
                                                                {{substr($day['text'], 0, 47)." ..."}}
                                                                @else
                                                                    {{$day['text']}}
                                                                @endif
                                                            </div>
                                                              @php
                                                                $start = $day['time'];
                                                                $now = \Carbon\Carbon::now();
                                                                $time = $now->format('H:i:s');
                                                                $date = $now->format('Y-m-d');
                                                            @endphp
                                                            <div class="d-flex align-items-center">
                                                                <ul class="list-unstyled social-summary d-flex align-items-center m-0">
                                                                    @if(in_array('facebook_page',$day['channel']))
                                                                        <li><a href="#"><img src="{{asset('')}}images/facebook.svg" alt=""></a></li>
                                                                    @else
                                                                        <!-- <li><a href="#"><img src="{{asset('')}}images/ic/grey-facebook.png" alt=""></a></li> -->
                                                                    @endif

                                                                    @if(in_array('linkedin_profile',$day['channel']))
                                                                        <li><a href="#"><img src="{{asset('')}}images/linkedin.svg" alt=""></a></li>
                                                                    @else
                                                                        <!-- <li><a href="#"><img src="{{asset('')}}images/ic/grey-twitter.png" alt=""></a></li> -->
                                                                    @endif

                                                                    @if(in_array('twitter',$day['channel']))
                                                                        <li><a href="#"><img src="{{asset('')}}images/twitter.svg" alt=""></a></li>
                                                                    @else
                                                                        <!-- <li><a href="#"><img src="{{asset('')}}images/ic/grey-linkedin.png" alt=""></a></li> -->
                                                                    @endif

                                                                    @if(in_array('instagram',$day['channel']))
                                                                        <li>
                                                                            <a href="#">
                                                                                <img src="{{asset('')}}images/instagram.svg" alt="">
                                                                            </a>
                                                                        </li>
                                                                    @else
                                                                        <!-- <li><a href="#"><img src="{{asset('')}}images/ic/grey-instagram.png" alt=""></a></li> -->
                                                                    @endif

                                                                </ul>
                                                                <ul class="list-unstyled social-summary social-summary-time d-flex align-items-center m-0">
                                                                    <li>{{ \Carbon\Carbon::parse($day['time'])->format('H:i') }}</li>
                                                                </ul>
                                                                <ul class="list-unstyled action-list d-flex align-items-center m-0">
                                                                    <li><a href="{{url('edit/post/'.$day['id'].'/'.$day['post_id'])}}"><img src="{{asset('')}}images/edit.svg" alt=""></a></li>
                                                                    <li><a href="{{url('delete/post/'.$day['id'].'/'.$day['post_id'])}}"><img src="{{asset('')}}images/delete.svg" alt=""></a></li>
                                                                    <li>
                                                                        @csrf
                                                                        @if (($time >= $start) && ($date == $day['date']))
                                                                              <a href="#" class="btn btn-outline-secondary">{{__('message.Reschedule')}}</a>
                                                                        @else
                                                                               <a href="#" class="btn btn-outline-secondary btn-web share_now" data-id="{{$day['post_id']}}">{{__('message.Share_Now')}}</a>
                                                                        @endif
                                                                        
                                                                        <a href="#" class="btn-mobile"><img src="{{asset('images/send.svg')}}"></a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
{{--                                                        @endif--}}
                                                    @endforeach
                                                @endforeach

                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="tab-pane fade" id="post" role="tabpanel" aria-labelledby="post-tab">
                            <div class="scroll-cust">

                                @foreach($posts as $pk => $post)

                                    <div class="full-day-block">
                                        <div class="dayname-head full-post position-relative">

                                            @if($pk == \Carbon\Carbon::today()->toDateString())
                                                {{__('message.Today')}}
                                            @else
                                                @php
                                                    $ndate = \Carbon\Carbon::parse($key)->locale('en')->formatLocalized('%A,%B,%e');
                                                    $ddate = explode(',',$ndate);

                                                    echo (__('message.'.$ddate[0]).', '. __('message.'.$ddate[1]) .' '. $ddate[2]);
                                                @endphp
                                            @endif
                                        </div>

                                        <div class="full-post-group">
                                                @foreach($post as $dk => $day_)
                                                    @foreach($day_ as $d_k => $day)

                                                        <div class="full-post-block d-md-flex align-items-center justify-content-between" >
                                                            <div class="d-flex align-items-center">
                                                                <span class="full-post-image"><img src="{{$day['image']}}" onerror="this.onerror=null;this.src='{{asset('images/photo.svg')}}';" alt=""></span>
                                                            </div>

                                                            <div class="full-post-content">
                                                                <div class="full-post-desc">
                                                                    @if(strlen($day['text']) > 300)
                                                                        {{substr($day['text'], 0, 297)." ..."}}
                                                                    @else
                                                                        {{$day['text']}}
                                                                    @endif
                                                                </div>


                                                                <div class="full-post-footer">
                                                                    <div class="btn-sec ">
                                                                        <div class=" text-left">
                                                                            <div class="schedule-sec">
                                                                                <!-- {{\Carbon\Carbon::parse($day['date'])->format('l, F d')." ". \Carbon\Carbon::parse($day['time'])->format('H:i')}} -->
                                                                                    @php
                                                                                        $n_date = \Carbon\Carbon::parse($day['date'])->format('F d');
                                                                                        $d_date = explode(' ',$n_date);

                                                                                        $n1_date = \Carbon\Carbon::parse($day['time'])->format('H:i');

                                                                                        echo (__('message.'.$d_date[0]).' '. $d_date[1] .' ' .__('message.at').' '. $n1_date);
                                                                                    @endphp
                                                                            </div>

                                                                        </div>
                                                                        @php
                                                                            $start = $day['time'];
                                                                            $now = \Carbon\Carbon::now();
                                                                            $time = $now->format('H:i:s');
                                                                            $date = $now->format('Y-m-d');
                                                                        @endphp

                                                                        <div class="full-post-soc-btns d-flex">
                                                                            <ul class="list-unstyled social-full-post d-flex align-items-center m-0 pt-7">
                                                                                @if(in_array('facebook_page',$day['channel']))
                                                                                    <li><a href="#"><img src="{{asset('')}}images/facebook.svg" alt=""></a></li>
                                                                                @else
                                                                                <!-- <li><a href="#"><img src="{{asset('')}}images/ic/grey-facebook.png" alt=""></a></li> -->
                                                                                @endif

                                                                                @if(in_array('linkedin_profile',$day['channel']))
                                                                                    <li><a href="#"><img src="{{asset('')}}images/linkedin.svg" alt=""></a></li>
                                                                                @else
                                                                                <!-- <li><a href="#"><img src="{{asset('')}}images/ic/grey-twitter.png" alt=""></a></li> -->
                                                                                @endif

                                                                                @if(in_array('twitter',$day['channel']))
                                                                                    <li><a href="#"><img src="{{asset('')}}images/twitter.svg" alt=""></a></li>
                                                                                @else
                                                                                <!-- <li><a href="#"><img src="{{asset('')}}images/ic/grey-linkedin.png" alt=""></a></li> -->
                                                                                @endif

                                                                                @if(in_array('instagram',$day['channel']))
                                                                                    <li><a href="#"><img src="{{asset('')}}images/instagram.svg" alt=""></a></li>
                                                                                @else
                                                                                <!-- <li><a href="#"><img src="{{asset('')}}images/ic/grey-instagram.png" alt=""></a></li> -->
                                                                                @endif
                                                                            </ul>
                                                                            <ul class="list-unstyled action-list d-flex m-0 right-fix">
                                                                                <li class="pt-7"><a href="{{url('edit/post/'.$day['id'].'/'.$day['post_id'])}}"><img src="{{asset('')}}images/edit.svg" alt=""></a></li>
                                                                                <li class="pt-7"><a href="{{url('delete/post',$day['id'])}}"><img src="{{asset('')}}images/delete.svg" alt=""></a></li>
                                                                                <li>
                                                                                    @if (($time >= $start) && ($date == $day['date']))
                                                                                        <a href="#" class="btn btn-outline-secondary">{{__('message.Reschedule')}}</a>
                                                                                    @else
                                                                                        <a href="#" class="btn btn-outline-secondary">{{__('message.Share_Now')}}</a>
                                                                                    @endif
                                                                                </li>
                                                                            </ul>
                                                                        </div>


                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================================
        Right Section
        ========================================== -->
        <section class="right-section sidebar scroll-cust">

            <div class="side-accord">
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            {{__('message.Need_Help_with_Schedulling_Posts')}}
                            <span class="acc-arrow">
									<em class="fa fa-angle-right"></em>
								</span>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                {{__('message.Need_Help_with_Schedulling_Posts_content')}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
{{--            <div class="start-chat d-flex align-items-center justify-content-between">--}}
{{--                <span>Start Chatting</span>--}}
{{--                <button type="button" class="btn btn-secondary">Click here</button>--}}
{{--            </div>--}}
            <div class="thumbnail-group ">
                <div class="thumbnail-block">
                    <div class="thumb">
                        <img src="{{asset('')}}images/ic/image-1.jpg" alt="">
                    </div>
                    <div class="caption">
                        <h4>{{__('message.How_to_schedule_post')}}</h4>
                        <p>{{__('message.How_to_schedule_post_content')}}</p>
                        <a href="https://bonconline.com/en/blog/online-marketingtrends.html?fbclid=IwAR0sryOaPf0rgbZXKVJIEbTY3YP0zRpPvh-Fc8_0fIHS1X8FkZdS_00aLic" target="_blank">{{__('message.Read_more')}}</a>
                    </div>
                </div>

            </div>
        </section>
    </div>

@endsection

@push('styles')
    <link href="{{asset('')}}css/pignose.calendar.min.css" rel="stylesheet"><!-- pignose.calendar css -->
@endpush
@push('scripts')
    <script src="{{asset('')}}js/pignose.calendar.full.min.js"> </script><!-- pignose.calendar.full.min -->
    <script>
    $(function () {
        var ocDates = @json($ocDate);
        var locale = '{{ config('app.locale') }}';
        var langs='';
        if(locale=='en'){

            var langs='en';

        }
        else{
            var langs='nl';
        }
        $('#calendar').pignoseCalendar({
            'enabledDates': ocDates,
            'lang': langs,
            'click': function(event, context) {
                // this is clicked button.
                var this_ = $(this);

                // event is general event object.
                var selDate = this_[0].dataset.date;
            }
        });
    });

    $(".alert").delay(3000).slideUp(800, function() {
        $(this).alert('close');
    });
    $(document).ready(function (){
        $("#click").click(function (){
            $('html, body').animate({
                scrollTop: $("#div1").offset().top
            }, 2000);
        });
    });
      $('.share_now_').on('click',function(){
         var post_id=$(this).data('id');
         
           $.ajax({
                    type: "POST",
                    // contentType: "application/json; charset=utf-8",
                    headers: {'X-CSRF-Token': $('input[name="_token"]').val()},
                    url: "share/now/"+post_id,
                    dataType: "json",
                    success: function (data) {
                        
                     console.log(data);   
                    },
                    failure: function (response) {
                        alert("No Match");
                    }
                });
      });
      

    </script>

@endpush
