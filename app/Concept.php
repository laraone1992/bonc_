<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concept extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'concept';
    protected $primaryKey = 'id';
}
