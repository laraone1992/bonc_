<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
    <meta name = "format-detection" content = "telephone=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title')</title>

    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('images/favicon.png')}}" type="image/x-icon">
    <!-- CSS Stylesheet -->
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet"><!-- font-awesome css -->
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet"><!-- bootstrap css -->
    <link href="{{asset('css/jquery.datetimepicker.css')}}" rel="stylesheet" type="text/css" />
{{--    <link href="{{asset('css/pignose.calendar.min.css')}}" rel="stylesheet"><!-- pignose.calendar css -->--}}
    <link href="{{asset('css/jquery.mCustomScrollbar.css')}}" rel="stylesheet"><!-- jquery.mCustomScrollbar css -->
    <link href="{{asset('css/css3.css')}}" rel="stylesheet"><!-- css3 css -->

{{--    <script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>--}}
    @stack('styles')
    <link href="{{asset('css/docs.css')}}" rel="stylesheet"><!-- docs css -->
    <link href="https://cdn.materialdesignicons.com/4.8.95/css/materialdesignicons.min.css" rel="stylesheet"><!-- docs css -->
    <style>

        #BoncModalText .ui-autocomplete.ui-widget {
            font-family: Verdana,Arial,sans-serif;
            font-size: 12px;
        }
        #BoncModalText .modal-dialog {

            width: 400px;


        }
        #BoncModalText .modal-body{
            height: 500px;
        }
        #BoncModalText .modal-header {

            background-color: #000000;

            padding:16px 16px;

            color:#FFF;

        }
        #notificationview .modal-header{
            background-color: #000000;

            padding:16px 16px;

            color:#FFF;
        }
        .notification-content{font-family: 'Titillium Web', sans-serif; !important; font-size: 12px;
            line-height: 15px;
            }
        .notification-image{
            /*width: 100%;*/
            height: 100%;
            max-height: 300px;
            padding-bottom: 8px;
        }
        .clickboard{
            top: 772px;
            left: 751px;
            width: 19px;
            height: 19px;
            opacity: 1;
        }
        .clipboardtext{
            top: 770px;
            left: 781px;
            width: 145px;
            height: 24px;
            text-align: left;
            font-family: 'Titillium Web', sans-serif; !important; font-size: 12px;
            letter-spacing: 0;
            color: #000000;
            opacity: 1;
        }
        .notification-title{
            top: 197px;
            left: 750px;
            text-align: left;
            font-family: 'Titillium Web', sans-serif; !important; font-size: 16px;
            letter-spacing: 0;
            color: #FFFFFF;
            opacity: 1;
        }
        .shareInsta{
            top: 830px;
            left: 850px;
            width: 194px;
            height: 39px;
            text-align: center;
            font-family: 'Titillium Web', sans-serif; !important; font-size: 13px;
            letter-spacing: 0;
            color: #FFFFFF;
            opacity: 1;
            border-top: none;
        }
    </style>
</head>

<body>
<div id="wrapper">

    <!-- =========================================
    Header Section
    ========================================== -->
    @include('layouts.header')

    @yield('content')


</div>
<div class="overlay">
    <div class="overlay__inner">
        <div class="overlay__content"><span class="spinner"></span></div>
    </div>
</div>
<!-- =========================================
java script
========================================== -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

{{--<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script><!-- jquery-3.3.1.min js-->--}}
<script src="{{asset('js/popper.min.js')}}"></script><!-- popper.min js-->
<script src="{{asset('js/bootstrap.min.js')}}"></script><!-- bootstrap.min js-->
<script src="{{asset('js/moment.min.js')}}"></script>
<script src="{{asset('js/modal-steps.min.js')}}"></script>
<script src="{{asset('js/jquery.datetimepicker.full.js')}}"></script>
{{--<script src="{{asset('js/pignose.calendar.min.js')}}"> </script><!-- pignose.calendar.min -->--}}
{{--<script src="{{asset('js/pignose.calendar.full.min.js')}}"> </script><!-- pignose.calendar.full.min -->--}}
<script src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script><!-- jquery.mCustomScrollbar.concat.min js-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="{{asset('js/cssua.js')}}"></script><!-- cssua js-->
<script src="{{asset('js/custom.js')}}"></script><!-- custom js-->
<script src="{{asset('js/comman.js')}}"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5e29ef45daaca76c6fcf9210/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
@stack('scripts')
</body>
</html>
