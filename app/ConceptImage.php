<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConceptImage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'concept_image';
    protected $primaryKey = 'id';
}
